<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH.'controllers/Base.php');

class Api extends Base {

	function __construct()
    {
		parent::__construct(); 
        $this->load->model('m_api'); 
	}
	
	function index()
	{
            $ret = array('status'=>'FAILED','message'=>'Invalid url path !');

            $this->output->set_status_header('400');
            header('Content-Type: application/json');
            echo json_encode($ret);  
	} 
	
        function getinfo(){
            $clientIp = $_SERVER['REMOTE_ADDR'];
            $this->ipAccess($clientIp, $allowed=false);
            
            if ( $this->input->post()){
                log_message('error', ' GET REQUEST : '.json_encode($_POST));
               
                $merchant_code  = $this->input->post('merchant_code');
                
                
                    if(empty($merchant_code)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Kode Merchant Tidak Boleh Kosong !!'); 
                    }else {

                        if ( !$this->m_api->cekMerchantId(strtoupper($merchant_code)) ){
                            log_message('error', '========= Kode Merchant Tidak Ada ========= : ');
                            $this->output->set_status_header('400');
                            $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Kode Merchant Tidak Ada !!');  
                        }else{
                                                            
                            $data = $this->m_api->getMerchantByCode($merchant_code);  
                            $data = json_decode($data);

                            unset ($data->rows->discount);
                            
                            $ret = array();
                            $ret['status']     = 'SUCCESS';
                            $ret['respcode']   = '00';
                            $ret['data']       = $data->rows;
                            $ret['total_row']  = $data->total;
                            $this->output->set_status_header('200');
                            log_message('error', '========= Get Info Success ========= : ');
                           
                        }  
                    }
                }else{
                    log_message('error', '========= GET NOT ALLOWED ========= : ');
                    $this->output->set_status_header('400');
                    $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Parameters required!');    
                }

            header('Content-Type: application/json');
            echo json_encode($ret); 
            log_message('error', 'SEND RESPONSE : '.json_encode($ret));
        }
                
        function inquiry(){
            $clientIp = $_SERVER['REMOTE_ADDR'];
            $this->ipAccess($clientIp, $allowed=false);
            
            if ( $this->input->post()){
                log_message('error', ' GET REQUEST : '.json_encode($_POST));
                $random     = rand(000000,999999);
                $str        = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuffled   = str_shuffle($str);
                $shuffled   = substr($shuffled,0,4);

                $requestid      = $this->input->post('requestid');
                $amount         = $this->input->post('amount');
                $app_code       = $this->input->post('app_code');
                $merchant_code  = $this->input->post('merchant_code');
                $msisdn         = $this->input->post('msisdn');
                $name           = $this->input->post('name');
                $time           = $this->input->post('time');
                $signature      = $this->input->post('signature');
                $trxId          = md5(strrev($shuffled).date('YmdHis').$shuffled.$random);
                
                $sign           = md5($merchant_code.$amount.$time);
                $amount         = str_replace('.', '', $amount);
                
                    if(empty($merchant_code)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Kode Merchant Tidak Boleh Kosong !!'); 
                    }else if(empty($requestid)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'RequestId Tidak Boleh Kosong !!'); 
                    }else if(empty($amount)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Amount Tidak Boleh Kosong !!'); 
                    }else if(empty($time)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Time Tidak Boleh Kosong !!'); 
                    }else if(empty($signature)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Signature Tidak Boleh Kosong !!'); 
                    }else if($signature != $sign){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Invalid Signature !!'); 
                    }else {

                        if ( !$this->m_api->cekMerchantId(strtoupper($merchant_code)) ){
                            log_message('error', '========= Kode Merchant Tidak Ada ========= : ');
                            $this->output->set_status_header('400');
                            $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Kode Merchant Tidak Ada !!');  
                        }else{
                            
                            $getId = $this->m_api->getIdDiscount($merchant_code); 
                            if($getId['is_active'] == 'INACTIVE'){
                                log_message('error', '========= Merchant Is INACTIVE ========= : ');
                                $this->output->set_status_header('400');
                                $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Merchant Is Not Activated !!');  
                            }else{
                                $discount = $this->getDiscount($amount,$app_code,$getId['discount'],$merchant_code,$msisdn);
                            
                                $dataInquiry = array( 'requestid'     => $requestid,  
                                                    'amount'          => $amount,
                                                    'app_code'        => $app_code,
                                                    'id_merchant'     => strtoupper($merchant_code),
                                                    'msisdn'          => $msisdn,
                                                    'name'            => $name,
                                                    'discount'        => $discount['discount'],
                                                    'fee'             => $discount['fee'],
                                                    'total_amount'    => $discount['total_amount'],
                                                    'status'          => '0',
                                                    'id_discount'     => $getId['discount'],
                                                    'trxid'           => $trxId
                                            );

                                if($this->m_api->addInquiry($dataInquiry)){

                                    $data = $this->m_api->getMerchantByCode($merchant_code);  
                                    $data = json_decode($data);
                                    $data->rows->trxid           = $trxId;
                                    $data->rows->amount          = $amount;
                                    $data->rows->fee             = $discount['fee'];
                                    $data->rows->discount        = $discount['discount'];
                                    $data->rows->total_amount    = $discount['total_amount'];

                                    unset($data->rows->id);  
                                    unset($data->rows->status);     


                                    $ret = array();
                                    $ret['status']          = 'SUCCESS';
                                    $ret['respcode']        = '00';
                                    $ret['requestid']       = $requestid;
                                    $ret['amount']          = $amount;
                                    $ret['data']            = $data->rows;
                                    //$ret['total_row']       = $data->total;
                                    $this->output->set_status_header('200');
                                    log_message('error', '========= Inquiry Success ========= : ');
                                }else{
                                    log_message('error', '========= Save Inquiry to Database Failed ========= : ');
                                    $this->output->set_status_header('400');
                                    $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Proses Inquiry GAGAL !!');  
                                }
                            }
                            
                        }  
                    }
                }else{
                    log_message('error', '========= GET NOT ALLOWED ========= : ');
                    $this->output->set_status_header('400');
                    $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Parameters required!');    
                }

            header('Content-Type: application/json');
            echo json_encode($ret); 
            log_message('error', 'SEND RESPONSE : '.json_encode($ret));
        }
        
        function payment(){
            $clientIp = $_SERVER['REMOTE_ADDR'];
            $this->ipAccess($clientIp, $allowed=false);
            
            if ( $this->input->post()){
                log_message('error', ' GET REQUEST : '.json_encode($_POST));
                $random     = rand(000000,999999);
                $str        = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $shuffled   = str_shuffle($str);
                $shuffled   = substr($shuffled,0,4);

                $requestid      = $this->input->post('requestid');
                $amount         = $this->input->post('amount');
                $app_code       = $this->input->post('app_code');
                $merchant_code  = $this->input->post('merchant_code');
                $msisdn         = $this->input->post('msisdn');
                $name           = $this->input->post('name');
                $trxId          = $this->input->post('trxid');
                $time           = $this->input->post('time');
                $signature      = $this->input->post('signature');
                $reffid         = md5(strrev($shuffled).date('YmdHis').$shuffled.$random);
                $SerialNumber   = $shuffled.'-'.$random;
                
                $sign           = md5($merchant_code.$amount.$time);
                $amount         = str_replace('.', '', $amount);
                
                    if(empty($trxId)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Trx Id Tidak Boleh Kosong !!'); 
                    }else if(empty($requestid)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'RequestId Tidak Boleh Kosong !!'); 
                    }else if(empty($amount)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Amount Tidak Boleh Kosong !!'); 
                    }else if(empty($time)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Time Tidak Boleh Kosong !!'); 
                    }else if(empty($signature)){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Signature Tidak Boleh Kosong !!'); 
                    }else if($signature != $sign){
                        $this->output->set_status_header('400');
                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Invalid Signature !!'); 
                    } else {

                       
                        
                        if ( !$this->m_api->cekInquiryId($trxId,strtoupper($merchant_code)) ){
                            log_message('error', '========= Trx Id Tidak ada  ========= : '.$trxId);
                            $this->output->set_status_header('400');
                            $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Trx Id Tidak ada !!');  
                        }else{
                            
                            $data = $this->m_api->getDataInquiry($trxId);
                            $value = $this->m_api->getDataDiscount(strtoupper($app_code),$data['id_discount']);
                            if (!$this->checkBudget($value) && $data['discount'] > 0) {
                                log_message('error', '========= Budget Habis  ========= : ');
                                $this->output->set_status_header('400');
                                $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Budget Diskon Habis');
                            }else{
                                //$amountTrx = $amount;
                                $amountTrx = $data['amount'];

                                $getMerchant = $this->m_api->getMerchantData($merchant_code);
                                $msisdnMerchant = isset($getMerchant['phone']) ? $getMerchant['phone'] : '0';
                                $balanceMerchant = isset($getMerchant['balance']) ? $getMerchant['balance'] : '0';
                                $mdrMerchant = isset($getMerchant['mdr']) ? $getMerchant['mdr'] : '0';

                                $balance = $balanceMerchant+$amount;
                                $fee     = ($balance*$mdrMerchant)/100;
                                $withdraw = $balance - $fee;

                                //$result = $this->apimerchant->transferMerchant($amountTrx,$msisdnMerchant);
                                $result = $this->apimerchant->paymentMerchant($amountTrx,$msisdnMerchant,$requestid);

                                $status = explode(';', $result);
                                $statusCode = isset($status[0]) ? $status[0] : '';
                                $description = isset($status[1]) ? $status[1] : '';

                                if(strpos($result, 'TRX ID:') !== false && strpos($result, 'Nama:') !== false){
                                    $explTrx = explode('TRX ID:', $description);
                                    $getTrx  = explode('Nama:', $explTrx[1]);
                                    $SN = isset($getTrx[0]) ? $getTrx[0] : '';
                                    $SerialNumber = trim($SN);
                                }

                                $merchant = $this->m_api->getMerchantByCode($merchant_code);
                                $merchant = json_decode($merchant);

                                $merchant->rows->amount         = $amount;
                                $merchant->rows->fee            = $data['fee'];
                                $merchant->rows->discount       = $data['discount'];
                                $merchant->rows->total_amount   = $data['total_amount'];

                                if($statusCode == '941'){

                                    $dataTrx = array( 'requestid'     => $requestid,  
                                                    'amount'          => $data['amount'],
                                                    'app_code'        => $app_code,
                                                    'id_merchant'     => strtoupper($merchant_code),
                                                    'msisdn'          => $data['msisdn'],
                                                    'name'            => $name,
                                                    'inquiryid'       => $trxId,
                                                    'trxid'           => $reffid,
                                                    'amount_trx'      => $amountTrx,
                                                    'serial_number'   => $SerialNumber,
                                                    'discount'        => $data['discount'],
                                                    'fee'             => $data['fee'],
                                                    'total_amount'    => $data['total_amount'],
                                                    'id_discount'     => $getMerchant['discount'],
                                                    'status'          => 'SUCCESS',
                                                    'description'     => $description
                                            );

                                    $updateMerchant = array( 'balance'     => $balance,  
                                                              'fee'        => $fee,
                                                              'withdraw'   => $withdraw,
                                            );

                                    $updateInquiry = array( 'status'     => '1');

                                    if($this->m_api->addTrx($dataTrx)){

                                        $ret['status']     = 'SUCCESS';
                                        $ret['respcode']   = '00';
                                        $ret['amount']     = $amount;
                                        //$ret['requestid']  = $requestid;
                                        $ret['requestid']  = $SerialNumber;
                                        $ret['reffid']     = $SerialNumber;
                                        $ret['message']    = 'Transaksi Pembayaran Sebesar Rp. '.$amount.' BERHASIL SN :'.$SerialNumber;
                                        $ret['data']       = $merchant->rows;
                                        $this->output->set_status_header('200');
                                        $this->m_api->updateMerchantBalance($updateMerchant,$merchant_code);
                                        $this->m_api->updateInquiry($updateInquiry,$trxId);

                                        log_message('error', '========= Transaksi SUCCESS  ========= : ');
                                    }else{
                                        log_message('error', '========= Save Trx Transfer to Database FAILED  ========= : ');
                                        $this->output->set_status_header('400');
                                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Transaksi Pembayaran Sebesar Rp. '.$amount.' GAGAL');  
                                    }

                                }else if($statusCode == '68'){
                                    /* PENDING */
                                    $dataTrx = array( 'requestid'     => $requestid,  
                                                    'amount'          => $amount,
                                                    'app_code'        => $app_code,
                                                    'id_merchant'     => $merchant_code,
                                                    'msisdn'          => $data['msisdn'],
                                                    'name'            => $name,
                                                    'inquiryid'       => $trxId,
                                                    'trxid'           => $reffid,
                                                    'amount_trx'      => $amountTrx,
                                                    'id_discount'     => $getMerchant['discount'],
                                                    'status'          => 'PENDING',
                                                    'description'     => $description
                                            );

                                    $updateMerchant = array( 'balance'     => $balance,  
                                                              'fee'        => $fee,
                                                              'withdraw'   => $withdraw,
                                            );

                                    $updateInquiry = array( 'status'     => '1');

                                    if($this->m_api->addTrx($dataTrx)){
                                        $data['amount']    = $amount;
                                        $ret['status']     = 'PENDING';
                                        $ret['respcode']   = '68';
                                        $ret['amount']     = $amount;
                                        $ret['requestid']  = $requestid;
                                        //$ret['reffid']     = $SerialNumber;
                                        $ret['message']    = 'Transaksi Sedang Di Proses';
                                        $ret['data']       = $merchant->rows;
                                        $this->output->set_status_header('200');
                                        $this->m_api->updateInquiry($updateInquiry,$trxId);

                                        log_message('error', '========= Transaksi PENDING  ========= : ');
                                    }else{
                                        log_message('error', '========= Save Trx Transfer to Database FAILED  ========= : ');
                                        $this->output->set_status_header('400');
                                        $ret = array('status'=>'PENDING','respcode'=>'40','message'=>'Transaksi Sedang Di Proses');  
                                    }
                                }else if(strpos($result, 'BERHASIL') !== false || strpos($result, 'Berhasil') !== false || strpos($result, 'berhasil') !== false || strpos($result, 'SUKSES') !== false || strpos($result, 'Sukses') !== false || strpos($result, 'sukses') !== false){

                                    $dataTrx = array( 'requestid'     => $requestid,  
                                                    'amount'          => $amount,
                                                    'app_code'        => $app_code,
                                                    'id_merchant'     => strtoupper($merchant_code),
                                                    'msisdn'          => $data['msisdn'],
                                                    'name'            => $name,
                                                    'inquiryid'       => $trxId,
                                                    'trxid'           => $reffid,
                                                    'amount_trx'      => $amountTrx,
                                                    'serial_number'   => $SerialNumber,
                                                    'id_discount'     => $getMerchant['discount'],
                                                    'status'          => 'SUCCESS',
                                                    'description'     => $description
                                            );

                                    $updateMerchant = array( 'balance'     => $balance,  
                                                              'fee'        => $fee,
                                                              'withdraw'   => $withdraw,
                                            );

                                    $updateInquiry = array( 'status'     => '1');

                                    if($this->m_api->addTrx($dataTrx)){
                                        $data['amount']    = $amount;
                                        $ret['status']     = 'SUCCESS';
                                        $ret['respcode']   = '00';
                                        $ret['amount']     = $amount;
                                        //$ret['requestid']  = $requestid;
                                        $ret['requestid']  = $SerialNumber;
                                        $ret['reffid']     = $SerialNumber;
                                        $ret['message']    = 'Transaksi Pembayaran Sebesar Rp. '.$amount.' BERHASIL SN :'.$SerialNumber;
                                        $ret['data']       = $merchant->rows;
                                        $this->output->set_status_header('200');
                                        $this->m_api->updateMerchantBalance($updateMerchant,$merchant_code);
                                        $this->m_api->updateInquiry($updateInquiry,$trxId);

                                        log_message('error', '========= Transaksi SUCCESS  ========= : ');
                                    }else{
                                        log_message('error', '========= Save Trx Transfer to Database FAILED  ========= : ');
                                        $this->output->set_status_header('400');
                                        $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Transaksi Pembayaran Sebesar Rp. '.$amount.' GAGAL');  
                                    }
                                }else{
                                    $dataTrx = array( 'requestid'     => $requestid,  
                                                    'amount'          => $amount,
                                                    'app_code'        => $app_code,
                                                    'id_merchant'     => $merchant_code,
                                                    'msisdn'          => $data['msisdn'],
                                                    'name'            => $name,
                                                    'inquiryid'       => $trxId,
                                                    'trxid'           => $reffid,
                                                    'amount_trx'      => $amountTrx,
                                                    'id_discount'     => $getMerchant['discount'],
                                                    'status'          => 'FAILED',
                                                    'description'     => $description
                                            );
                                    $this->m_api->addTrx($dataTrx);
                                    log_message('error', '========= Transfer to st24 FAILED  ========= : '.json_encode($dataTrx));
                                    $this->output->set_status_header('400');
                                    $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Transaksi Pembayaran GAGAL');  
                                }
                            }
                        }  
                    }
                }else{
                    log_message('error', '========= GET NOT ALLOWED ========= : ');
                    $this->output->set_status_header('400');
                    $ret = array('status'=>'FAILED','respcode'=>'40','message'=>'Parameters required!');    
                }

            header('Content-Type: application/json');
            echo json_encode($ret); 
            log_message('error', 'SEND RESPONSE : '.json_encode($ret));
        }
        
        private function ipAccess($clientIp,$allowed){
            $CI =& get_instance();
            
            if (array_key_exists($clientIp, $CI->config->item('IpWhiteList'))){
                $allowed = true;
                log_message('error', 'IP ACCESS FROM : '.$clientIp);
            }
            
            //$allowed = true; // REMOVE THIS LINE ON PRODUCTION
            
            if (!$allowed) {
                log_message('error', 'FORBIDDEN ACCESS for IP : '.$clientIp);
                //header(':', true, 403);
                echo 'FORBIDDEN ACCESS For IP : '.$clientIp ;
                die();
            }
        }
         
        private function getDiscount($total,$app_code,$Id,$merchant_code,$msisdn){
            $CI =& get_instance();
            
            $value = $this->m_api->getDataDiscount(strtoupper($app_code),$Id);
            
            $data = array();
            $data['fee']                = '0' ;
            $data['discount'] 		= '0' ;
            $data['total_amount'] 	= $total;
            
	
            if($value){
                if($value['status'] == 'EXPIRED'){
                    $data['fee']                = '0' ;
                    $data['discount'] 		= '0' ;
                    $data['total_amount'] 	= $total;
                }elseif(!$this->checkDate($value)){
                    $data['fee']                = '0' ;
                    $data['discount'] 		= '0' ;
                    $data['total_amount'] 	= $total;
                }elseif (!$this->checkBudget($value)) {
                    $data['fee']                = '0' ;
                    $data['discount'] 		= '0' ;
                    $data['total_amount'] 	= $total;
                }elseif (!$this->maxUser($merchant_code,$msisdn,$value)) {
                    $data['fee']                = '0' ;
                    $data['discount'] 		= '0' ;
                    $data['total_amount'] 	= $total;
                }else{
                    $data['total'] 		= $total + $value['fee'] ;
                    $data['fee']            = $value['fee'] ;
                    if ($value['nominal_get_discount'] <= $total && $value['type'] == 'Nominal') {

                        if($value['discount'] > $value['max_discount']){
                            $data['discount'] = $value['max_discount'];
                        }else{
                            $data['discount'] = $value['discount'];
                        }
                    }else if ($value['nominal_get_discount'] <= $total && $value['type'] == 'Percentage'){
                        $data['percentage'] = ($data['total'] * $value['discount']) / 100;
                        if($data['percentage'] > $value['max_discount']){
                            $data['discount'] = $value['max_discount'];
                        }else{
                            $data['discount'] = $data['percentage'];
                        }
                    }

                        $data['total_amount'] = ($data['total'] - $data['discount']) ;
                }
                
                
            }
            
            return $data;
        }
        
        private function maxUser($merchant_code,$msisdn,$discount ){
            
            $user = $this->m_api->checkMaxUser($merchant_code,$msisdn,$discount);
            log_message('error', 'Max User DB : '.json_encode($user));
            if($discount['limit_user'] <= $user['max']){
                return false;
            }else{
                return true;
            }
        }
        
        private function checkBudget($discount){
            log_message('error', 'Discount DB : '.json_encode($discount));
            $budget = $this->m_api->sumBudget($discount);
            log_message('error', 'Budget DB : '.json_encode($budget));
            
            if($discount['limit_balance'] <= $budget['budget']){
                return false;
            }else{
                return true;
            }
        }
        
        private function checkDate($discount ){
            $today  = date('Y-m-d');
            if($today < $discount['from_date']){
                return false;
            }elseif($today > $discount['to_date']){
                return false;
            }else{
                return true;
            }
        }
        
}

