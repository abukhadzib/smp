<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

include "PHPMailer/PHPMailerAutoload.php";

class Apiemail {

    
     protected $url            = "http://localhost/email";
	

     public function sendEmail($nama,$msisdn,$usaha,$type,$identitas,$alamat,$kodeMerchant,$bank,$rekening,$kodePos,$email,$statusType) {
         $CI =& get_instance();
         
         $mail             = new PHPMailer();

        $body             = "<p>&nbsp;Dear All,</p>

            <p>Berikut Data Registrasi ".$statusType." PayAccess, Mohon Agar dapat dilakukan Aktivasi :</p>

            <table border='1' cellpadding='1' cellspacing='1' style='width:500px'>
                    <tbody>
                            <tr>
                                    <td>Kode ".$statusType."</td>
                                    <td>: $kodeMerchant</td>
                            </tr>
                            <tr>
                                    <td>Nama ".$statusType."</td>
                                    <td>: $nama</td>
                            </tr>
                            <tr>
                                    <td>Handphone</td>
                                    <td>: $msisdn</td>
                            </tr>
                            <tr>
                                    <td>Jenis Usaha</td>
                                    <td>: $usaha</td>
                            </tr>
                            <tr>
                                    <td>Email</td>
                                    <td>: $email</td>
                            </tr>
                            <tr>
                                    <td>Alamat</td>
                                    <td>: $alamat</td>
                            </tr>
                            <tr>
                                    <td>Type Identitas</td>
                                    <td>: $type</td>
                            </tr>
                            <tr>
                                    <td>No Identitas</td>
                                    <td>: $identitas</td>
                            </tr>
                            <tr>
                                    <td>Bank</td>
                                    <td>: $bank</td>
                            </tr>
                            <tr>
                                    <td>No Rekening</td>
                                    <td>: $rekening</td>
                            </tr>
                            <tr>
                                    <td>Status Type</td>
                                    <td>: $statusType</td>
                            </tr>
                    </tbody>
            </table>

            <p>Terima Kasih,</p>";
        
                
        $mail->IsSMTP();                
        $mail->SMTPAuth   = true;                  
        $mail->SMTPSecure = "ssl";         
        $mail->Host       = "ssl://".$CI->config->item('mail_host');          
        $mail->Host       = $CI->config->item('mail_host');     
        $mail->Port       = $CI->config->item('mail_port');                     
        $mail->Username   = $CI->config->item('mail_username');      
        $mail->Password   = $CI->config->item('mail_password');                 
        $mail->SetFrom($CI->config->item('mail_from'), "Registrasi ".$statusType);
        $mail->AddAddress($CI->config->item('mail_to')); 
        $mail->AddAddress($CI->config->item('mail_to1'));
        $mail->AddAddress($CI->config->item('mail_to2'));
        $mail->AddCC($CI->config->item('mail_cc'));
        $mail->AddCC($CI->config->item('mail_cc1'));
        $mail->AddCC($CI->config->item('mail_cc2'));
        $mail->AddCC($CI->config->item('mail_cc3'));
        $mail->AddCC($CI->config->item('mail_cc4'));
        $mail->addBCC($CI->config->item('mail_bcc'));
        /*
        $mail->Host       = "ssl://smtp.gmail.com";           
        $mail->Host       = "smtp.gmail.com";      
        $mail->Port       = 465;                   
        $mail->Username   = "reportharian@gmail.com";  
        $mail->Password   = "reporting1234";            
        $mail->SetFrom('reportharian@gmail.com', 'Reporting');
        */
        

        $mail->Subject    = "Registrasi ".$statusType." Payaccess DEV";

        $mail->MsgHTML($body);

        //$mail->AddAttachment($path);      // attachment
          
        //$mail->AddCC("syamsul.anwar21@gmail.com");
        //$mail->addBCC("syamsul.anwar21@gmail.com", "Syamsul");

        ///$mail->Send();
		
        if(!$mail->Send()) {
          log_message('error', '========= ERROR SEND EMAIL ========== '. $mail->ErrorInfo);
		  //echo "Mailer Error: " . $mail->ErrorInfo;
          return false;
		} else {
          log_message('error', '========= SEND EMAIL SUCCESS ========== ');
		  //echo "Message sent!";
          return true;
		}
		
     }
	

}
