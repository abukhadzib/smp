<?php

//include('httpful-0.2.0.phar');
require_once 'httpclient.php';

class St24 {

    private $smscid = "IN%2DGPRS";
    private $host;

    function __construct() {
        $CI =& get_instance();
        $this->host     = $CI->config->item('Host'); 
        $this->xmlHost  = $CI->config->item('xmlHost'); 
    }

    public function xmlInquiry($request) {
        $request->kode  = 'TAG'.$request->kode;
        $resp           = $this->sendXml($request);
        
        if($resp['RESPONSECODE'] == 00){
            if (!isset($resp['AMOUNT'])) {
                if(isset($resp['TOTAL'])) {
                    $parameter['amount'] = $resp['TOTAL'];
                } else {
                    $parameter['amount'] = $resp['BILL'];
                }
            } else {
                $parameter['amount'] = $resp['AMOUNT'];
            }
           $result = array(
                "status"            => "OK",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID'],
                "bill_data"         => $resp['BILL_DATA'],
                "nama"              => $resp['NAMA'],
                "amount"            => $parameter['amount'],
                "custid"            => $request->custid
            );
        }else if($resp['RESPONSECODE'] == 68){
           $result = array(
                "status"            => "OK",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID'],
                "custid"            => $request->custid
            );
        }else{
            $result = array(
                "status"            => "ERROR",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID']
            );
        }
        
        return $result;
    }
    
    public function xmlPayment($request) {
        $request->kode  = 'PAY'.$request->kode;
        $resp           = $this->sendXml($request);
        
        if($resp['RESPONSECODE'] == 00 || $resp['RESPONSECODE'] == 68){
           $result = array(
                "status"            => "OK",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID'],
                "amount"            => $request->amount
            );
        }else{
            $result = array(
                "status"            => "ERROR",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID']
            );
        }
        
        return $result;
    }
    
    public function xmlpurchase($request) {
        $resp = $this->sendXml($request);
        
        if($resp['RESPONSECODE'] == 00){
            $a = explode('Rp ',$resp['MESSAGE']);
            $pattern='/\d+\.?\d*/';

            $success = preg_match($pattern, $a[1], $match);
            $amount = str_replace('.','',$match[0]);
            
           $result = array(
                "status"            => "OK",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID'],
                "amount"            => $amount
            );
        }else if($resp['RESPONSECODE'] == 68){
           $result = array(
                "status"            => "OK",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID']
            );
        }else{
            $result = array(
                "status"            => "ERROR",
                "responsecode"      => $resp['RESPONSECODE'],
                "kode"              => $request->kode,
                "requestid"         => $resp['REQUESTID'],
                "message"           => $resp['MESSAGE'],
                "transactionid"     => $resp['TRANSACTIONID']
            );
        }
        
        return $result;
    }
    
    public function purchase($request) {
        $text = $request->nominal . "." . $request->custid . "." . md5($request->webpass) . "." . md5($request->pin);
        return $this->sendToSt24($request, $text);
    }
    
    public function inquiry($request) {
        $text = 'TAG.' . $request->kode . '."' . $request->custid . '.' . $request->targetnumber . '".' . md5($request->webpass) . '.' . md5($request->pin);
        return $this->sendToSt24($request, $text);
    }

    public function payment($request) {
        $text = 'PAY.' . $request->kode . '."' . $request->custid . '.' . $request->targetnumber . '".' . md5($request->webpass) . '.' . md5($request->pin);
        return $this->sendToSt24($request, $text);
    }
    
    public function inquiryToBank($request) { 
	$request->keterangan = str_replace('.', ' ', $request->keterangan);
        $text = "TAG.". $request->kode. "." . $request->kode_bank . "." . $request->no_rekening . "." . $request->nohp_penerima . "|" . $request->nama_penerima . "|" . $request->kota_penerima . "|" . $request->keterangan . "." . md5($request->webpass) . "." . md5($request->pin);
        $result = $this->sendToSt24($request, $text);
        if(strrpos(strtoupper($result['message']  ), 'A/N') == TRUE){
            $result['status']          = "OK";
            $result['responsecode']    = '00';
        }else if(strrpos(strtoupper($result['message']  ), 'GAGAL') == TRUE){
            $result['status']          = "ERROR";
            $result['responsecode']    = '40';
        }
        return $result;
    }
    
    public function transferToBank($request) {
        $text = "PAY." . $request->kode. "." . $request->kode_bank . "." . $request->nama_pengirim . "." . "01" . "." . $request->no_id . "." . $request->nohp_penerima . "." . $request->no_rekening . "." . $request->amount . "." . $request->nohp_pengirim . "." . md5($request->webpass) . "." . md5($request->pin);
        $result = $this->sendToSt24($request, $text);
        if(strrpos(strtoupper($result['message']  ), 'BERHASIL') == TRUE){
            $result['status']          = "OK";
            $result['responsecode']    = '00';
        }else if(strrpos(strtoupper($result['message']  ), 'GAGAL') == TRUE){
            $result['status']          = "ERROR";
            $result['responsecode']    = '40';
        }
        return $result;
    }

    private function sendToSt24($request, $text) {
        $hair = time();
        $secure = $this->getSecureCode($request, $hair);

        $uri  = $this->host . "?"
                . "secure=" . $secure
                . "&Text=" . urlencode($text)
                . "&hair=" . $hair
                . "&msisdn=" . $request->msisdn
                . "&SMSCID=" . $this->smscid
                . "&PhoneNumber=" . $request->msisdn;

        try {
            log_message('error', 'SEND to ST24 API-IN: ' . $uri);
            
            $response = \Httpful\Request::get($uri)->parseWith(
                            function($body) {
                                return explode(";", $body);
                            })->send();
           
            
            $httpStatus = $response->code;
            if ($httpStatus == 200) {
                $statusCode = $response->body[0];
                $message    = $response->body[1];
                if($statusCode == '00'){
                    $status     = "OK";
                }else{
                    $status     = "ERROR";
                }
                log_message('error', 'RECV from ST24 API-IN: statusCode=' . $statusCode . ', message=' . $message);
            } else {
                $status     = "ERROR";
                $statusCode = 99;
                $message    = "Unable to connect to server.";
                log_message('error', 'COULD NOT Connect to ST24 API-IN ['.$uri.'] because HttpStatus is ' . $httpStatus);
            }
            
            $result = array(
                "status"        => $status,
                "responsecode"  => $statusCode,
                "message"       => $message
                
            );
            
        } catch (Exception $e) {
            log_message('error', 'COULD NOT Connect to ST24 API-IN ['.$uri.'] because: ' . $e->getMessage());
            $result = array(
                "status"        => "ERROR",
                "responsecode"  => 99,
                "message"       => "Transaksi Gagal, Unable to connect to server."
            );
        }
        
        return $result;
    }
    
    private function sendXml($request) {
        
        $params = array(
            "MSISDN" => $request->msisdn,
            "REQUESTID" => $request->requestid,
            "PIN" => $request->pin,
            "NOHP" => $request->custid,
            "NOM" => $request->kode
        );
        
        $xmlReq = xmlrpc_encode_request("topUpRequest", $params);
        unset($params['PIN']);
        log_message('error', 'Request to XMLIN : ' . json_encode($params));

        $response = \Httpful\Request::post($this->xmlHost)
                ->body($xmlReq)
                ->sendsXml()
                ->send();

        log_message('error', 'Response from XMLIN (XML): ' . $response);
        $arrResponse = xmlrpc_decode($response, "iso-8859-1");
        log_message('error', 'Response from XMLIN (JSON): ' . json_encode($arrResponse));
        
        return $arrResponse;
    }
    
    private function getSecureCode($request, $time) {
        //Secure=md5(pin(dibaca terbalik, dr kanan ke kiri)+webpin(dibaca terbalik, dr kanan ke kiri)+koesplus+time(long))
        $reveresedPin       = strrev($request->pin);
        $reveresedWebpin    = strrev($request->webpass);
        $s                  = $reveresedWebpin . $reveresedPin . "koesplus" . $time;
        return md5($s);
    }
    
}
