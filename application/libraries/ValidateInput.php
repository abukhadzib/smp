<?php

class ValidateInput {

    var $message = "";
    var $TYPE_TEXT = "TEXT";
    var $TYPE_EMAIL = "EMAIL";
    var $TYPE_URL = "URL";
    var $TYPE_NUMBER = "NUMBER";
    var $TYPE_DATE = "DATE";
    var $error = false;
    var $input_value = array("0");
    var $input_key = array("0");

    function ValidateInput() {
        
    }

    function getMessage() {
        return $this->message;
    }

    function setMessage($message) {
        $this->message .= "&raquo; " . $message . "<br>";
    }

    function isError() {
        return $this->error;
    }

    function setInputValue($key, $value) {
        array_push($this->input_value, $value);
        array_push($this->input_key, $key);
    }

    function getInputValue() {
        $r = $this->array_combine($this->input_key, $this->input_value);
        $this->resetInputValue();
        return $r;
    }

    function resetInputValue() {
        reset($this->input_key);
        reset($this->input_value);
    }

    function mustSame($val1, $name1, $val2, $name2) {
        if ($val1 <> $val2) {
            $this->error = true;
            $r = false;
            $this->setMessage("$name1 dan $name2 tidak sama!!");
        }
    }

    function validateNumber($input, $field_name, $max_value = 0, $null = false) {
        $r = "";

        $r = $this->validateText($input, $field_name, $this->TYPE_NUMBER, $null);
        if ($r <> false && $max_value <> 0 && $r > $max_value) {
            $this->error = true;
            $r = false;
            $this->setMessage("$field_name melebihi batas!!");
        }

        return $r;
    }

    function validateText($input, $field_name, $type = "TEXT", $null = false, $max_char = 0, $min_char = 0, $allow_quotes = false, $allow_html = false, $clean_html = false) {
        $r = "";

        if (!$null && $input == "") {
            $this->error = true;
            $r = false;
            $this->setMessage("$field_name harus diisi");
        } else {
            if ($max_char <> 0 && strlen($input) > $max_char) {
                $this->error = true;
                $r = false;
                $this->setMessage("Jumlah karakter $field_name melebihi batas!!");
            } else if ($min_char <> 0 && strlen($input) < $min_char) {
                $this->error = true;
                $r = false;
                $this->setMessage("Minimun karakter $field_name adalah $min_char!!");
            } else {

                if (!$allow_quotes && preg_match('/["|\']/', $input)) {
                    $this->error = true;
                    $r = false;
                    $this->setMessage("$field_name Cannot Contain Any Quotes!!");
                } else {
                    $input = preg_replace('/"/', '&quot;', $input);
                    $input = preg_replace('/\'/', '&#039;', $input);

                    if (preg_match('/<\/?[a-z][a-z0-9]*[^<>]*>/sim', $input)) {
                        if (!$allow_html) {
                            $this->error = true;
                            $r = false;
                            $this->setMessage("$field_name Cannot Contain HTML Tags Characters!!");
                        } else {
                            if (!$clean_html) {
                                $r = $input;
                            } else {
                                $r = htmlentities($input, ENT_QUOTES);
                            }
                        }
                    } else {
                        $r = $input;
                    }
                }
            }
        }

        if ($r <> false) {
            switch ($type) {
                case "TEXT":
                    $this->setInputValue($field_name, $r);
                    break;

                case "EMAIL":
                    if (!preg_match('/\\A\\b[A-Z0-9._%-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b\\z/i', $r)) {
                        $this->error = true;
                        $r = false;
                        $this->setMessage("$field_name has Invalid Email Address!!");
                    } else {
                        $this->setInputValue($field_name, $r);
                    }
                    break;

                case "URL":
                    if (!preg_match('/\\A(?:\\b(?P<protocol>https?|ftp):\/\/(?P<domain>[-A-Z0-9.]+)(?P<file>\/[-A-Z0-9+&@#\/%=~_|!:,.;]*)?(?P<parameters>\\?[-A-Z0-9+&@#\/%=~_|!:,.;]*)?)\\z/i', $r)) {
                        $this->error = true;
                        $r = false;
                        $this->setMessage("$field_name has Invalid URL Address!!");
                    } else {
                        $this->setInputValue($field_name, $r);
                    }
                    break;

                case "NUMBER":
                    if (!preg_match('/\\A(?:[-+]?(?:\\b[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+\\b)(?:[eE][-+]?[0-9]+\\b)?)\\z/', $r)) {
                        $this->error = true;
                        $r = false;
                        $this->setMessage("$field_name has Invalid Number Format!!");
                    } else {
                        $this->setInputValue($field_name, $r);
                    }
                    break;

                case "DATE":
                    if (!preg_match('/\\A(?:(19|20)[0-9]{2}[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01]))\\z/', $r)) {
                        $this->error = true;
                        $r = false;
                        $this->setMessage("$field_name has Invalid Date Format!!");
                    } else {
                        $this->setInputValue($field_name, $r);
                    }
                    break;

                default:
                    break;
            }
        }

        return $r;
    }

    function array_combine($a, $b) {
        $c = array();
        if (is_array($a) && is_array($b))
            while (list(, $va) = each($a))
                if (list(, $vb) = each($b))
                    $c[$va] = $vb;
                else
                    break 1;
        return $c;
    }

}

?>