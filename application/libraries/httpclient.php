<?php

include_once dirname(__FILE__) . '/http.php';

class HttpClient {

    private $err;

    function __construct() {
        
    }

    function doSyncHttpRequest($url, $params, $method = "POST", $headers = NULL) {

        $http = $this->getHttpConnection();
        $error = $http->GetRequestArguments($url, $arguments);
        $arguments["RequestMethod"] = $method;
        $arguments["PostValues"] = $params;

        if ($headers <> NULL) {
            foreach ($headers as $key => $value) {
                $arguments["Headers"][$key] = $value;
            }
        }

        flush();
        $error = $http->Open($arguments);

        $response = false;
        if ($error == "") {
            $error = $http->SendRequest($arguments);
            if ($error == "") {
                $headers = array();
                $error = $http->ReadReplyHeaders($headers);
                if ($error == "") {
                    for (;;) {
                        $error = $http->ReadReplyBody($body, 1000);
                        if ($error != "" || strlen($body) == 0)
                            break;
                        $response .= HtmlSpecialChars($body);
                    }
                    flush();
                }
            }
        }

        if ($error <> "") {
            $this->err = $error;
        }
        return $response;
    }

    function doAsyncHttpRequest($url, $params, $contentType = null, $headers = NULL) {
        $http = $this->getHttpConnection();

        $error = $http->GetRequestArguments($url, $arguments);
        $arguments["RequestMethod"] = "POST";
        $arguments["PostValues"] = $params;

        /* Set additional request headers */
        $arguments["Headers"]["Pragma"] = "nocache";
        $arguments["Headers"]["Connection"] = "Close";
        if ($contentType <> null) {
            $arguments["Headers"]["Content-type"] = $contentType;
        }
        if ($headers <> null) {
            foreach ($headers as $key => $value) {
                $arguments["Headers"][$key] = $value;
            }
        }

        flush();
        $http->
                $error = $http->Open($arguments);

        if ($error == "") {
            $error = $http->SendRequest($arguments);
            if ($error == "") {
                return true;
            }
        }

        return $error;
    }

    function doSyncRawHttpRequest($url, $requestBody, $headers = NULL) {

        $http = $this->getHttpConnection();
        $error = $http->GetRequestArguments($url, $arguments);
        $arguments["RequestMethod"] = "POST";
        //$arguments["PostValues"] = $params;
        $arguments["Body"] = $requestBody;

        if ($headers <> null) {
            foreach ($headers as $key => $value) {
                $arguments["Headers"][$key] = $value;
            }
        }

        flush();
        $error = $http->Open($arguments);

        $response = false;
        if ($error == "") {
            $error = $http->SendRequest($arguments);
            if ($error == "") {
                $headers = array();
                $error = $http->ReadReplyHeaders($headers);
                if ($error == "") {
                    for (;;) {
                        $error = $http->ReadReplyBody($body, 1000);
                        if ($error != "" || strlen($body) == 0)
                            break;
                        $response .= $body;
                    }
                    flush();
                }
            }
        }

        if ($error <> "") {
            $this->err = $error;
        }
        return $response;
    }

    function getError() {
        return $this->err;
    }

    function getHttpConnection() {
        set_time_limit(0);
        $http = new http_class;
        $http->timeout = 0;
        $http->data_timeout = 0;
        $http->debug = 0;
        $http->html_debug = 0;
        $http->prefer_curl = 1;

        return $http;
    }

}

?>
