<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_cta extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getProduct($limit=10,$page=0,$filter=array(),$excel=false)
    {        
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * FROM product ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
        
    }
    
    function getProductById($id)
    {
    	$sql = "SELECT * from product where id=$id ";
    	$data  = $this->db->query($sql);
    	return $data->row();
    }
    
     function addProduct($data=array()){
        return $this->db->insert('product', $data); 
    }
    
    function cekCodeProduct($code=''){
        
        $sql = "select code from product WHERE code='$code' ";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    } 
    
    function cekCodeProductnId($code='',$id=''){
        
        $sql = "select code from product WHERE code='$code' AND id <> '$id'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    } 
    
    function updateProduct($data,$id)
    {
    	return $this->db->update('product', $data, array('id' => $id));
    }
    
    
    function deleteProduct($id)
    {
        return $this->db->delete('product', array('id'=>$id));         
    }
    
    function getCtaInquiry($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT * FROM cta_inquiry ORDER BY id DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
    function getCtaTransaksi($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT * FROM cta_transaksi ORDER BY id DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
    public function exportCtaInquiry($startDate,$endDate,$filter=array(),$excel=false){
        
        $sql = "SELECT * FROM cta_inquiry WHERE created_at BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }  
    }
    
    public function exportCtaTransaksi($startDate,$endDate,$filter=array(),$excel=false){
        
        $sql = "SELECT * FROM cta_transaksi WHERE created_at BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        //log_message('error', 'Query : ' .$sql);
        
        if ($excel){
            return $data;
        } else {        
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }  
    }
}