<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_merchant extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function getMerchant($limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * FROM merchant ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 	

    function getListDiscount($limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT m.id_merchant,m.name,m.jenis_usaha,m.phone,d.name as discount_name,m.discount
                FROM merchant m 
                LEFT JOIN
                discount d ON m.discount = d.id
                WHERE m.discount IS NOT NULL AND m.discount <> ''";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 	
    
    function getDiscount($limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        $sql = "SELECT * FROM discount ORDER BY id DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 
    
    function searchMerchant($merchantCode='',$msisdn='',$limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        if($merchantCode == ''){
            $where  = " where phone like '%".$msisdn."%'";
        }else if($msisdn == ''){
            $where  = " where id_merchant like '%".$merchantCode."%'";
        }else if($msisdn !== '' && $merchantCode !== ''){
            $where  = " where (phone like '%".$msisdn."%' or id_merchant like '%".$merchantCode."%')";
        }else{
            $where = '';
        }
        $sql = "SELECT * from merchant $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 	
    
    function getMerchantById($id)
    {
    	$sql = "SELECT * from merchant where id=$id";
    	$data  = $this->db->query($sql);
    	return $data->row();
    }
    
    function cekIdMerchant($code=''){
        
        $sql = "select id_merchant from merchant WHERE id_merchant='$code'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    } 
    
    function getDiscountById($id)
    {
    	$sql = "SELECT * from discount where id=$id";
    	$data  = $this->db->query($sql);
    	return $data->row();
    }
    
    function cekDiscount($app_code=''){
        
        $sql = "select discount from discount WHERE app_code='$app_code'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
    
    function cekStatusDiscountAllMerchant(){
        
        $sql = "select id from discount WHERE status ='ALL MERCHANT'";
        $query = $this->db->query($sql);
        if ($query->num_rows()>0){
            return true;
        } else {
            return false;
        }
    }
    
    function getDiscountAllMerchant(){
        
        $sql = "select * from discount WHERE status ='ALL MERCHANT'";
        $data = $this->db->query($sql);
        return $data->row_array();
    }
    
    function addDiscount($data=array()){
        $this->db->insert('discount', $data); 
        $insert_id = $this->db->insert_id();
        log_message('error', 'Insert ID : '.$insert_id);
        return  $insert_id;
    }
    
    function injectDiscount($id)
    {
    	$sql = "UPDATE merchant SET discount = '$id' WHERE status = 'MERCHANT' AND discount IS NULL OR discount = '' ";
        
        return $this->db->query($sql);
    }
    
    function changeInjectDiscount($id)
    {
    	$sql = "UPDATE merchant SET discount = NULL WHERE discount = '$id' ";
        
        return $this->db->query($sql);
    }
    
    function updateDiscount($data,$id)
    {
    	return $this->db->update('discount', $data, array('id' => $id));
    }
    
    function deleteDiscount($id)
    {
        return $this->db->delete('discount', array('id'=>$id));         
    }
    
    function getBank(){
        
        $sql = "SELECT * from bank";
        $data  = $this->db->query($sql);
        return $data->result();  
        
    } 
    
    function discountList(){
        
        $sql = "SELECT * from discount";
        $data  = $this->db->query($sql);
        return $data->result();  
        
    }
    
    function addMerchant($data=array()){
        return $this->db->insert('merchant', $data); 
    }
    
    function updateMerchant($data,$id)
    {
    	return $this->db->update('merchant', $data, array('id' => $id));
    }
    
    function deleteMerchant($id)
    {
        return $this->db->delete('merchant', array('id'=>$id));         
    }
    
    function getInquiry($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT a.trxid,a.requestid,a.amount,a.app_code,a.id_merchant,m.name AS 'merchant',a.msisdn,a.name,a.date,a.id,
                a.fee,a.discount,a.total_amount
                FROM inquiry a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                ORDER BY a.date DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
    function searchInquiry($merchantCode='',$msisdn='',$startDate,$endDate,$limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        if($merchantCode == '' && $msisdn !== ''){
            $where  = " where a.msisdn like '%".$msisdn."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($msisdn == '' && $merchantCode !== ''){
            $where  = " where a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($msisdn !== '' && $merchantCode !== ''){
            $where  = " where (a.msisdn like '%".$msisdn."%' or a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59')";
        }else{
            $where = "where a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }
        $sql = "SELECT a.trxid,a.requestid,a.amount,a.app_code,a.id_merchant,m.name AS 'merchant',a.msisdn,a.name,a.date,a.id,
                a.fee,a.discount,a.total_amount
                FROM inquiry a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                $where
                ORDER BY a.date DESC";
        
        //$sql = "SELECT * from inquiry $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
    } 	
    
    function getTransaksi($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT a.serial_number,a.trxid,a.requestid,a.amount,a.app_code,a.id_merchant,m.name AS 'merchant',a.msisdn,a.name,a.date,a.id,a.status,a.description,
                a.fee,a.discount,a.total_amount
                FROM transaksi a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                ORDER BY a.date DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
    function searchTransaksi($merchantCode='',$status='',$startDate,$endDate,$limit=15,$page=0,$excel=false)
    {    
        $start = $page>0 ? $limit*$page : 0; 
        
        if($merchantCode == '' && $status != ''){
            $where  = " where a.status = '".$status."' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($status == '' && $merchantCode !== ''){
            $where  = " where a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($status !== '' && $merchantCode !== ''){
            $where  = " where (a.status = '".$status."' AND a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59')";
        }else{
            $where = "where a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }
        $sql = "SELECT a.trxid,a.requestid,a.amount,a.app_code,a.id_merchant,m.name AS 'merchant',a.msisdn,a.name,a.serial_number,a.date,a.id,a.status,a.description,
                a.fee,a.discount,a.total_amount
                FROM transaksi a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                $where
                ORDER BY a.date DESC";
        //$sql = "SELECT * from transaksi $where order by id desc";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
        
        
        if ($excel){
            return $data;
        } else {        
            $sql  .= " LIMIT ".$start.",".$limit;    
            $data  = $this->db->query($sql);             
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }   
        
    } 	
    
    function getHistory($limit=15,$page=0,$excel=false)
    {
    	$start = $page>0 ? $limit*$page : 0;
    
    	$sql = "SELECT * FROM transaksi WHERE user_id=$this->userid ORDER BY date DESC";
    	$data  = $this->db->query($sql);
    	$total = $data->num_rows();
    	 
    	if ($excel){
    		return $data;
    	} else {
    		$sql  .= " LIMIT ".$start.",".$limit;
    		$data  = $this->db->query($sql);
    		return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
    	}
    }
    
    public function exportMerchant($type,$startDate,$endDate,$filter=array(),$excel=false){
        
        $isi    = "'";
        $concat = '"'.$isi.'"';
        //$sql = "SELECT * FROM merchant ORDER BY id ASC";
        $sql = "SELECT id_merchant AS Kode,name,CONCAT($concat,phone) AS Handphone,jenis_usaha,alamat,zipcode,type_identitas,
                CONCAT($concat,no_identitas) AS no_identitas,email,id_bank AS kode_bank,bank,CONCAT($concat,rekening) AS Rekening,nama_rekening,hubungan ,mdr,status,tgl_daftar,sales,pic,cluster,is_active
                FROM merchant WHERE status = '$type' AND tgl_daftar BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'
                ORDER BY id ASC";
        
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
       
        if ($excel){
            return $data;
        } else {        
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }  
    }
    
    public function exportInquiry($merchantCode='',$msisdn='',$startDate,$endDate,$filter=array(),$excel=false){
        
        if($merchantCode == '' && $msisdn !== ''){
            $where  = " where a.msisdn like '%".$msisdn."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($msisdn == '' && $merchantCode !== ''){
            $where  = " where a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }else if($msisdn !== '' && $merchantCode !== ''){
            $where  = " where (a.msisdn like '%".$msisdn."%' or a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59')";
        }else{
            $where = "where a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59'";
        }
        $sql = "SELECT a.date,a.trxid,a.requestid,a.amount,a.id_merchant,m.name AS 'merchant',m.alamat,m.jenis_usaha,a.msisdn,a.name,a.app_code
                FROM inquiry a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                $where
                ORDER BY a.date DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        if ($excel){
            return $data;
        } else {        
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }  
    }
    
    public function exportTransaksi($merchantCode='',$status='',$startDate,$endDate,$filter=array(),$excel=false){
        
        if($merchantCode == '' && $status != ''){
            $where  = " where a.status = '".$status."' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
        }else if($status == '' && $merchantCode !== ''){
            $where  = " where a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
        }else if($status !== '' && $merchantCode !== ''){
            $where  = " where (a.status = '".$status."' AND a.id_merchant like '%".$merchantCode."%' AND a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59') ";
        }else{
            $where = "where a.date BETWEEN '$startDate 00:00:00' AND '$endDate 23:59:59' ";
        }
        
        $sql = "SELECT a.date,a.trxid,a.requestid,a.amount,a.discount,a.total_amount,d.name as discount_name,a.id_merchant AS Kode_Merchant,m.name AS 'merchant',m.alamat,m.jenis_usaha,a.msisdn,a.name,a.app_code,a.status,a.description
                FROM transaksi a
                LEFT JOIN
                merchant m
                ON a.id_merchant=m.id_merchant
                LEFT JOIN
                discount d
                ON a.id_discount = d.id
                $where
                ORDER BY a.date DESC";
        $data  = $this->db->query($sql);
        $total = $data->num_rows();
         
        //log_message('error', 'Query : ' .$sql);
        
        if ($excel){
            return $data;
        } else {        
            return '{"total":"'.$total.'","rows":'.json_encode($data->result()).'}';
        }  
    }
    
}