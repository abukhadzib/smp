<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_trx extends CI_Model {
    
    function __construct() {
		parent::__construct();
	}
 
    function addInquiryCta($data=array()){
        return $this->db->insert('cta_inquiry', $data); 
    }
    
    function addTrxCta($data=array()){
        return $this->db->insert('cta_transaksi', $data); 
    }
    
    function updateCtaInquiry($data,$trxid)
    {
        return $this->db->update('cta_inquiry', $data, array('trxId' => $trxid));
    }
    
    function validateTrxId($body){
        
        $trxId = $body['trxId'];
        
        $sql = "select * from cta_inquiry WHERE trxId = '$trxId' AND is_use = '0' ";
        $query = $this->db->query($sql);
        return $query->row_array();
        
    } 
    
    function validateCode($body){
        
        $kode = strtoupper($body['kode']);
        
        $sql = "select * from product WHERE code = '$kode' ";
        $query = $this->db->query($sql);
        return $query->row_array();
        
    } 
    
}