<!DOCTYPE HTML>
<html>
  <body>
  <a href="<?=site_url('home/video')?>">Back</a>
    <canvas id="myCanvas" width="500" height="200"></canvas>
    <script>
      var canvas = document.getElementById('myCanvas');
      var context = canvas.getContext('2d');
<!--  www  .ja v a 2  s.c om-->
      // draw cloud
      context.beginPath();
      context.moveTo(170, 80);
      context.bezierCurveTo(130, 100, 130, 150, 230, 150);
      context.bezierCurveTo(250, 180, 320, 180, 340, 150);
      context.bezierCurveTo(420, 150, 420, 120, 390, 100);
      context.bezierCurveTo(430, 40, 370, 30, 340, 50);
      context.bezierCurveTo(320, 5, 250, 20, 250, 50);
      context.bezierCurveTo(200, 5, 150, 20, 170, 80);
      context.closePath();
      context.lineWidth = 5;
      context.fillStyle = 'red';
      context.fill();
      context.strokeStyle = '#0000ff';
      context.stroke();

      // save canvas image as data url (png format by default)
      var dataURL = canvas.toDataURL();
      console.log(dataURL);
      
      // load image from data url
      var imageObj = new Image();
      imageObj.onload = function() {
          // scale y component
          context.scale(1, 0.5);
          context.drawImage(this, 0, 0);
      };

      imageObj.src = dataURL;      
    </script>
  </body>
</html> 