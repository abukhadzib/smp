
<section class="content">
<div class="row">
    <div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw  fa-qrcode"></i> Your Qrcode <span> > Code </span></h3>
	</div>
    <div class="col-xs-12">
        <div id="image"  style="width: auto;">
            <img src="<?=$qrcode.$code.'.png'?>" />
        </div>
    </div>
    
</div>  
</section>