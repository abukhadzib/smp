$(document).ready(function()
{    
    $(document).ajaxStart(function(){
		$("#loading-data").show();
	}).ajaxStop(function(){
		$("#loading-data").fadeOut("fast");
        $(window).resize();        
	}); 
		
    // Center Modal
    $(window).on('resize', function() {
        resize_login();
        $('.modal:visible').each(reposition);
    });    
    $('.modal').on('shown.bs.modal', reposition);
    $('.modal').on('loaded.bs.modal', reposition);
    // End Center Modal
    
    // Login
    $("#login-form").submit(function(){
        if ($("#username").val()==''){
            $("#username").focus();
            return false;
        }
        if ($("#password").val()==''){
            $("#password").focus();
            return false;
        }
    });
    resize_login();
    if ( $('.alert').text().trim().length > 0 ){
        $('.login-panel').shake();
    }
    // End Login
    
    // Menu Calender
    $("#calendar").datepicker({
		format: 'dd-mm-yyyy'
	}); 
    $('#calendar').datepicker().on('changeDate', function(e){
        var date = $(this).datepicker('getDate');
        $('#date1').datepicker('setDate',date);
        $('#date2').datepicker('setDate',date);
        $('#form-search').submit();
    }); 
    
    // Home Profile
    $('#user-profile').click(function() {
        var url = '<?=site_url('user_management/profile')?>';
        $('#labelDataProfile').text('');
        $('#dataProfileModal .modal-body').html('');
        $('#dataProfileModal .modal-body').load(url);
        $('#dataProfileModalURL').text(url);
    });
    $('#simpanProfileModal').click(function(){
        if ( $('#name_profile').val()=='' ){
            $('#name_profile').focus();
            return false;
        }
        if ( $('#no_identitas_profile').val()=='' ){
            $('#no_identitas_profile').focus();
            return false;
        }
        if ( !$.isNumeric( $('#no_identitas_profile').val() ) ){
            showNotifikasi('notifikasiProfileModal','No Identitas harus angka !');
            $('#no_identitas_profile').focus();
            return false;
        }
        if ( $('#phone_profile').val()=='' ){
            $('#phone_profile').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone_profile').val() ) ){
            showNotifikasi('notifikasiProfileModal','No Telepon harus angka !');
            $('#phone_profile').focus();
            return false;
        }
        if ( $('#email_profile').val()=='' ){
            $('#email_profile').focus();
            return false;
        }
        if ( !isValidEmailAddress($('#email_profile').val()) ){
            showNotifikasi('notifikasiProfileModal','Alamat Email tidak valid !');
            $('#email_profile').focus();
            return false;
        }
        if ( $('#username_profile').val()=='' ){
            $('#username_profile').focus();
            return false;
        }
        var url = $('#dataProfileModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-profile').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataProfileModal').modal('hide');  
            showNotifikasi('notifikasiProfileModal',result.Msg); 
            $('#statusNotifikasiProfile').val(result.success);
        });   
    });
    $("#buttonNotifikasiProfileOK").click(function() {
        if ( $('#statusNotifikasiProfile').val() ) {
            window.location.href = '<?=site_url('logout')?>';
        } else {
            $('#simpanProfileModal').button('reset'); 
        }
	});        
    // End Home Profile
    
    // Home Password
    $('#user-password').click(function() {
        var url = '<?=site_url('user_management/password')?>';
        $('#labelDataPassword').text('');
        $('#dataPasswordModal .modal-body').html('');
        $('#dataPasswordModal .modal-body').load(url);
        $('#dataPasswordModalURL').text(url);
    });
    $('#simpanPasswordModal').click(function(){        
        if ( $('#password_old').val()=='' ){
            $('#password_old').focus();
            return false;
        }
        if ( $('#password_new').val()=='' ){
            $('#password_new').focus();
            return false;
        } 
        if ( $('#password_konfirm').val()=='' ){
            $('#password_konfirm').focus();
            return false;
        }
        if ( $('#password_konfirm').val()!=$('#password_new').val() ){
            showNotifikasi('notifikasiPasswordModal','Password dan Konfirmasi Password tidak sama !');
            $('#password_konfirm').focus();
            return false;
        }
        var url = $('#dataPasswordModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-password').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataPasswordModal').modal('hide');  
            showNotifikasi('notifikasiPasswordModal',result.Msg); 
            $('#statusNotifikasiPassword').val(result.success);
        });   
    });
    $("#buttonNotifikasiPasswordOK").click(function() {
        if ( $('#statusNotifikasiPassword').val() ) {
            window.location.href = '<?=site_url('logout')?>';
        } else {
            $('#simpanPasswordModal').button('reset'); 
        }
	});        
    // End Home Password    
    
    // Role
    $('#tambahRole').click(function() {
        var url = '<?=site_url('user_management/role_add')?>';
        $('#labelDataRole').text('Tambah');  
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    $('.editRole').click(function() {
        var id = $(this).attr('data-role-id');
		var url = '<?=site_url('user_management/role_edit')?>'+'/'+id;
        $('#labelDataRole').text('Rubah');
        $('#dataRoleModal .modal-body').html('');
        $('#dataRoleModal .modal-body').load(url);
        $('#dataRoleModalURL').text(url);
    });
    $(".deleteRole").click(function() {
        var name = $(this).attr('data-role-name');
        var id = $(this).attr('data-role-id');
		$('#deleteRoleModalYes').attr('data-role-id',id);
        $('#namaRoleHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteRoleModalYes').click(function() {
        var id  = $(this).attr('data-role-id');
        var url = '<?=site_url('user_management/role_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });        
	});
    $('body').on('click', '.checkRead', function(){
        if($(this).prop('checked')){
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').prop('checked', false);
        } 
    });
    $('body').on('click', '.checkAll', function(){
        if ($(this).prop('checked')) {
            $(this).parents('tr').find('.checkRole').removeAttr('disabled');
            $(this).parents('tr').find('.checkRole').prop('checked', true);
        }else{
            $(this).parents('tr').find('.checkRole').prop('checked',false);
            $(this).parents('tr').find('.checkRole').not('.readCheck').not('.createCheck').attr('disabled', 'disabled');
        }
    });
    $('#simpanRoleModal').click(function(){
        if ( $('#role_name').val()=='' ){
            $('#role_name').focus();
            return false;
        }
        var url = $('#dataRoleModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-role').serialize(),
        function(result){
            var result = eval('('+result+')');
            $('#dataRoleModal').modal('hide');
            showNotifikasi('notifikasiRoleModal',result.Msg);
            $('#statusNotifikasiRole').val(result.success);
        });   
    });
    $("#buttonNotifikasiRoleOK").click(function() {
        if ( $('#statusNotifikasiRole').val() ) {
            window.location.reload();
        } else {
            $('#simpanRoleModal').button('reset'); 
        }
	});        
    // End Role
    
    // User
    $('#tambahUser').click(function() {
        var url = '<?=site_url('user_management/user_add')?>';
        $('#labelDataUser').text('Tambah');  
        $('#dataUserModal .modal-body').html('');
        $('#dataUserModal .modal-body').load(url);
        $('#dataUserModalURL').text(url);
    });
    $('.editUser').click(function() {
        var id = $(this).attr('data-user-id');
		var url = '<?=site_url('user_management/user_edit')?>'+'/'+id;
        $('#labelDataUser').text('Rubah');
        $('#dataUserModal .modal-body').html('');
        $('#dataUserModal .modal-body').load(url);
        $('#dataUserModalURL').text(url);
    });
    $(".deleteUser").click(function() {
        var name = $(this).attr('data-user-name');
        var id = $(this).attr('data-user-id');
		$('#deleteUserModalYes').attr('data-user-id',id);
        $('#namaUserHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteUserModalYes').click(function() {
        var id  = $(this).attr('data-user-id');
        var url = '<?=site_url('user_management/user_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteUserModal').modal('hide');
            showNotifikasi('notifikasiUserModal',result.Msg);
            $('#statusNotifikasiUser').val(result.success);
        });        
	});
    $('#simpanUserModal').click(function(){
        if ( $('#role').val()=='' || $('#role').val()===null ){
            $('#role').focus();
            return false;
        }
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#no_identitas').val()=='' ){
            $('#no_identitas').focus();
            return false;
        }
        if ( !$.isNumeric( $('#no_identitas').val() ) ){
            showNotifikasi('notifikasiUserModal','No Identitas harus angka !');
            $('#no_identitas').focus();
            return false;
        }
        if ( $('#phone').val()=='' ){
            $('#phone').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone').val() ) ){
            showNotifikasi('notifikasiUserModal','No Telepon harus angka !');
            $('#phone').focus();
            return false;
        }
        if ( $('#email').val()=='' ){
            $('#email').focus();
            return false;
        }
        if ( !isValidEmailAddress($('#email').val()) ){
            showNotifikasi('notifikasiUserModal','Alamat Email tidak valid !');
            $('#email').focus();
            return false;
        }
        if ( $('#username').val()=='' ){
            $('#username').focus();
            return false;
        }
        if ( $('#password').val()=='' ){
            $('#password').focus();
            return false;
        }
        if ( $('#passwordConfirm').val()=='' ){
            $('#passwordConfirm').focus();
            return false;
        }
        if ( $('#passwordConfirm').val()!=$('#password').val() ){
            showNotifikasi('notifikasiUserModal','Password dan Konfirmasi Password tidak sama !');
            $('#passwordConfirm').focus();
            return false;
        }
        var url = $('#dataUserModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-user').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataUserModal').modal('hide');  
            showNotifikasi('notifikasiUserModal',result.Msg); 
            $('#statusNotifikasiUser').val(result.success);
        });   
    });
    $("#buttonNotifikasiUserOK").click(function() {
        if ( $('#statusNotifikasiUser').val() ) {
            window.location.reload();
        } else {
            $('#simpanUserModal').button('reset'); 
        }
	});        
    // End User
    
    // Partner
    $('#tambahPartner').click(function() {
        var url = '<?=site_url('data_master/partner_add')?>';
        $('#labelDataPartner').text('Tambah');  
        $('#dataPartnerModal .modal-body').html('');
        $('#dataPartnerModal .modal-body').load(url);
        $('#dataPartnerModalURL').text(url);
    });
    $('.editPartner').click(function() {
        var id = $(this).attr('data-Partner-id');
		var url = '<?=site_url('data_master/partner_edit')?>'+'/'+id;
        $('#labelDataPartner').text('Rubah');
        $('#dataPartnerModal .modal-body').html('');
        $('#dataPartnerModal .modal-body').load(url);
        $('#dataPartnerModalURL').text(url);
    });
    $(".deletePartner").click(function() {
        var name = $(this).attr('data-Partner-name');
        var id = $(this).attr('data-Partner-id');
		$('#deletePartnerModalYes').attr('data-Partner-id',id);
        $('#namaPartnerHapus').html('<b>'+name+'</b>');
	});    
    $('#deletePartnerModalYes').click(function() {
        var id  = $(this).attr('data-Partner-id');
        var url = '<?=site_url('data_master/partner_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deletePartnerModal').modal('hide');
            showNotifikasi('notifikasiPartnerModal',result.Msg);
            $('#statusNotifikasiPartner').val(result.success);
        });        
	});
    $('#simpanPartnerModal').click(function(){
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#npwp').val()=='' ){
            $('#npwp').focus();
            return false;
        }
        if ( !$.isNumeric( $('#npwp').val() ) ){
            showNotifikasi('notifikasiPartnerModal','NPWP harus angka !');
            $('#npwp').focus();
            return false;
        }
        if ( $('#phone').val()=='' ){
            $('#phone').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone').val() ) ){
            showNotifikasi('notifikasiPartnerModal','No Telepon harus angka !');
            $('#phone').focus();
            return false;
        }
        if ( $('#zipcode').val()!='' && !$.isNumeric( $('#zipcode').val() ) ){
            showNotifikasi('notifikasiPartnerModal','Kode Pos harus angka !');
            $('#zipcode').focus();
            return false;
        }
        if ( $('#sms_response').val()!='' && $('#sms_response').val().length > 160 ){
            showNotifikasi('notifikasiPartnerModal','Jumlah karakter Response lebih dari 160 karakter !.');
            $('#sms_response').focus();
            return false;
        }
        var url = $('#dataPartnerModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-partner').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataPartnerModal').modal('hide');  
            showNotifikasi('notifikasiPartnerModal',result.Msg); 
            $('#statusNotifikasiPartner').val(result.success);
        });   
    });
    $("#buttonNotifikasiPartnerOK").click(function() {
        if ( $('#statusNotifikasiPartner').val() ) {
            window.location.reload();
        } else {
            $('#simpanPartnerModal').button('reset'); 
        }
	});        
    // End Partner
    
    // Program
    $('#tambahProgram').click(function() {
        var url = '<?=site_url('data_master/program_add')?>';
        $('#labelDataProgram').text('Tambah');  
        $('#dataProgramModal .modal-body').html('');
        $('#dataProgramModal .modal-body').load(url);
        $('#dataProgramModalURL').text(url);
    });
    $('.editProgram').click(function() {
        var id = $(this).attr('data-program-id');
		var url = '<?=site_url('data_master/program_edit')?>'+'/'+id;
        $('#labelDataProgram').text('Rubah');
        $('#dataProgramModal .modal-body').html('');
        $('#dataProgramModal .modal-body').load(url);
        $('#dataProgramModalURL').text(url);
    });
    $(".deleteProgram").click(function() {
        var name = $(this).attr('data-program-name');
        var id = $(this).attr('data-Program-id');
		$('#deleteProgramModalYes').attr('data-Program-id',id);
        $('#namaProgramHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteProgramModalYes').click(function() {
        var id  = $(this).attr('data-program-id');
        var url = '<?=site_url('data_master/program_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteProgramModal').modal('hide');
            showNotifikasi('notifikasiProgramModal',result.Msg);
            $('#statusNotifikasiProgram').val(result.success);
        });        
	});
    $('#simpanProgramModal').click(function(){
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#description').val()=='' ){
            $('#description').focus();
            return false;
        }
        if ( $('#keyword').val()=='' ){
            $('#keyword').focus();
            return false;
        }
        if ( $('#partner').val()=='' || $('#partner').val()===null ){
            $('#partner').focus();
            return false;
        }
        if ( $('#sms_response').val()!='' && $('#sms_response').val().length > 160 ){
            showNotifikasi('notifikasiProgramModal','Jumlah karakter Response lebih dari 160 karakter !.');
            $('#sms_response').focus();
            return false;
        }
        var url = $('#dataProgramModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-program').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataProgramModal').modal('hide');  
            showNotifikasi('notifikasiProgramModal',result.Msg); 
            $('#statusNotifikasiProgram').val(result.success);
        });   
    });
    $("#buttonNotifikasiProgramOK").click(function() {
        if ( $('#statusNotifikasiProgram').val() ) {
            window.location.reload();
        } else {
            $('#simpanProgramModal').button('reset'); 
        }
	});
    $('#clear-search-program-view').click(function(){
        window.location.href='<?=site_url('data_master/program_clear_search')?>';
    });
    // End Program
    
    // SubProgram
    $('#tambahSubProgram').click(function() {
        var url = '<?=site_url('data_master/subprogram_add')?>';
        $('#labelDataSubProgram').text('Tambah');  
        $('#dataSubProgramModal .modal-body').html('');
        $('#dataSubProgramModal .modal-body').load(url);
        $('#dataSubProgramModalURL').text(url);
    });
    $('.editSubProgram').click(function() {
        var id = $(this).attr('data-subprogram-id');
		var url = '<?=site_url('data_master/subprogram_edit')?>'+'/'+id;
        $('#labelDataSubProgram').text('Rubah');
        $('#dataSubProgramModal .modal-body').html('');
        $('#dataSubProgramModal .modal-body').load(url);
        $('#dataSubProgramModalURL').text(url);
    });
    $(".deleteSubProgram").click(function() {
        var name = $(this).attr('data-subprogram-name');
        var id = $(this).attr('data-SubProgram-id');
		$('#deleteSubProgramModalYes').attr('data-SubProgram-id',id);
        $('#namaSubProgramHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteSubProgramModalYes').click(function() {
        var id  = $(this).attr('data-subprogram-id');
        var url = '<?=site_url('data_master/subprogram_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteSubProgramModal').modal('hide');
            showNotifikasi('notifikasiSubProgramModal',result.Msg);
            $('#statusNotifikasiSubProgram').val(result.success);
        });        
	});
    $('#simpanSubProgramModal').click(function(){
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#description').val()=='' ){
            $('#description').focus();
            return false;
        }
        if ( $('#keyword').val()=='' ){
            $('#keyword').focus();
            return false;
        }
        if ( $('#partner').val()=='' || $('#partner').val()===null ){
            $('#partner').focus();
            return false;
        }
        if ( $('#program').val()=='' || $('#program').val()===null ){
            $('#program').focus();
            return false;
        }
        if ( $('#sms_response').val()!='' && $('#sms_response').val().length > 160 ){
            showNotifikasi('notifikasiSubProgramModal','Jumlah karakter Response lebih dari 160 karakter !.');
            $('#sms_response').focus();
            return false;
        }
        var url = $('#dataSubProgramModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-subprogram').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataSubProgramModal').modal('hide');  
            showNotifikasi('notifikasiSubProgramModal',result.Msg); 
            $('#statusNotifikasiSubProgram').val(result.success);
        });   
    });
    $("#buttonNotifikasiSubProgramOK").click(function() {
        if ( $('#statusNotifikasiSubProgram').val() ) {
            window.location.reload();
        } else {
            $('#simpanSubProgramModal').button('reset'); 
        }
	});
    $('body').on('change', '#partner', function(){
        $('#program').find('option:not(:first)').remove();
		$('#subprogram').find('option:not(:first)').remove();
		if ($(this).val()!='' || $(this).val()!='0'){
			$.get("<?=site_url('data_master/getProgramPartnerData')?>"+"/"+$(this).val(),function(data){
                var result = eval('('+data+')');
                $.each( result.rows, function( key, value ) {
					$('#program').append("<option value=\""+value.id+"\">"+value.name+"</option>");
				});
			});  
		}		
    });
    $('#search_partner').change(function(){
        $('#search_program').find('option:not(:first)').remove();
		$('#search_subprogram').find('option:not(:first)').remove();
		if ($(this).val()!='' || $(this).val()!='0'){
			$.get("<?=site_url('data_master/getProgramPartnerData')?>"+"/"+$(this).val(),function(data){
                var result = eval('('+data+')');
                $.each( result.rows, function( key, value ) {
					$('#search_program').append("<option value=\""+value.id+"\">"+value.name+"</option>");
				});
			});  
		}	
    });
    $('#clear-search-subprogram-view').click(function(){
        window.location.href='<?=site_url('data_master/subprogram_clear_search')?>';
    });
    // End SubProgram
    
    // Filter
    $('#tambahFilter').click(function() {
        var url = '<?=site_url('data_master/filter_add')?>';
        $('#labelDataFilter').text('Tambah');  
        $('#dataFilterModal .modal-body').html('');
        $('#dataFilterModal .modal-body').load(url);
        $('#dataFilterModalURL').text(url);
    });
    $('.editFilter').click(function() {
        var id = $(this).attr('data-filter-id');
		var url = '<?=site_url('data_master/filter_edit')?>'+'/'+id;
        $('#labelDataFilter').text('Rubah');
        $('#dataFilterModal .modal-body').html('');
        $('#dataFilterModal .modal-body').load(url);
        $('#dataFilterModalURL').text(url);
    });
    $(".deleteFilter").click(function() {
        var name = $(this).attr('data-filter-name');
        var id = $(this).attr('data-filter-id');
		$('#deleteFilterModalYes').attr('data-filter-id',id);
        $('#namaFilterHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteFilterModalYes').click(function() {
        var id  = $(this).attr('data-filter-id');
        var url = '<?=site_url('data_master/filter_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteFilterModal').modal('hide');
            showNotifikasi('notifikasiFilterModal',result.Msg);
            $('#statusNotifikasiFilter').val(result.success);
        });        
	});
    $('#simpanFilterModal').click(function(){
        if ( $('#word').val()=='' ){
            $('#word').focus();
            return false;
        }
        var url = $('#dataFilterModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-filter').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataFilterModal').modal('hide');  
            showNotifikasi('notifikasiFilterModal',result.Msg); 
            $('#statusNotifikasiFilter').val(result.success);
        });   
    });
    $("#buttonNotifikasiFilterOK").click(function() {
        if ( $('#statusNotifikasiFilter').val() ) {
            window.location.reload();
        } else {
            $('#simpanFilterModal').button('reset'); 
        }
	});        
    $('#resetSearchFilter,#resetSearchPartner').click(function() {
        $('#search').val('');
        $('#form-search').submit();
	});        
    // End Filter
    
    // Pesan
    $('#tambahPesan').click(function() {
        var url = '<?=site_url('pesan/pesan_add')?>';
        $('#labelDataPesan').text('Tambah');  
        $('#dataPesanModal .modal-body').html('');
        $('#dataPesanModal .modal-body').load(url);
        $('#dataPesanModalURL').text(url);
    });
    $('.editPesan').click(function() {
        var id = $(this).attr('data-pesan-id');
		var url = '<?=site_url('pesan/pesan_edit')?>'+'/'+id;
        $('#labelDataPesan').text('Rubah');
        $('#dataPesanModal .modal-body').html('');
        $('#dataPesanModal .modal-body').load(url);
        $('#dataPesanModalURL').text(url);
    });
    $(".deletePesan").click(function() {
        var name = $(this).attr('data-pesan-name');
        var id = $(this).attr('data-pesan-id');
		$('#deletePesanModalYes').attr('data-pesan-id',id);
        $('#namaPesanHapus').html('<b>'+name+'</b>');
	});    
    $('#deletePesanModalYes').click(function() {
        var id  = $(this).attr('data-pesan-id');
        var url = '<?=site_url('pesan/pesan_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deletePesanModal').modal('hide');
            showNotifikasi('notifikasiPesanModal',result.Msg);
            $('#statusNotifikasiPesan').val(result.success);
        });        
	});
    $('#simpanPesanModal').click(function(){
        if ( $('#word').val()=='' ){
            $('#word').focus();
            return false;
        }
        var url = $('#dataPesanModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-pesan').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataPesanModal').modal('hide');  
            showNotifikasi('notifikasiPesanModal',result.Msg); 
            $('#statusNotifikasiPesan').val(result.success);
        });   
    });
    $("#buttonNotifikasiPesanOK").click(function() {
        if ( $('#statusNotifikasiPesan').val() ) {
            window.location.reload();
        } else {
            $('#simpanPesanModal').button('reset'); 
        }
	});        
    $('#resetSearchPesan').click(function() {
        $('#search').val('');
        $('#form-search').submit();
	});        
    $('#pesan-excel').click(function() {
        window.location.href='<?=site_url('pesan/export_excel')?>';
	});        
    // End Pesan
    
    // Pesan view
    $('#date1,#date2').datepicker({
		format: 'dd-mm-yyyy',
        autoclose: true
	});    
    $('#date2').datepicker('setStartDate',$('#date1').datepicker('getDate'));
    $('#date1').datepicker().on('changeDate', function(e){
        var date1 =  $('#date1').datepicker('getDate');
        var date2 =  $('#date2').datepicker('getDate');
        if ( $('#date2').val()=='' ||  date1>date2 ){
            $('#date2').datepicker('setDate',date1);
        }
        $('#date2').datepicker('setStartDate',date1);
    });     
    $('#date2').datepicker().on('changeDate', function(e){
        var date1 =  $('#date1').datepicker('getDate');
        var date2 =  $('#date2').datepicker('getDate');
        if ( $('#date1').val()=='' ){
            $('#date1').datepicker('setDate',date2);
            $('#date2').datepicker('setStartDate',date2);
        }        
    }); 
    $('body').on('change', '#program', function(){
        $('#subprogram').find('option:not(:first)').remove();
		if ($(this).val()!='' || $(this).val()!='0'){
			$.get("<?=site_url('data_master/getSubProgramPartnerData')?>"+"/"+$('#partner').val()+"/"+$(this).val(),function(data){
                var result = eval('('+data+')');
                $.each( result.rows, function( key, value ) {
					$('#subprogram').append("<option value=\""+value.id+"\">"+value.name+"</option>");
				});
			});  
		}		
    });
    $('#clear-search-pesan-view').click(function(){
        window.location.href='<?=site_url('pesan/clear_search')?>';
    });
    // End Pesan view
    
    // sms response count
    $('body').on('keyup', '#sms_response,#pesan,#pesan_filter', function(){
        var rj = $(this).val().length;
        if ( rj > 160 ){
            $('#sms_response_count').css('color','red');
        } else {
            $('#sms_response_count').removeAttr('style');
        }
		$('#sms_response_count').text(rj);
    });
    $('body').on('change', '#sms_response,#pesan,#pesan_filter', function(){
        var rj = $(this).val().length;
        if ( rj > 160 ){
            $('#sms_response_count').css('color','red');
        } else {
            $('#sms_response_count').removeAttr('style');
        }
		$('#sms_response_count').text(rj);
    });    
    // End sms response count
    
    // Function
    function resize_login(){ 
        $('.login-panel').css({
            position:'absolute',
            bottom:'',
            right:'',
            left: ($(window).width() - $('.login-panel').outerWidth())/2,
            top: ($(window).height() - $('.login-panel').outerHeight())/2
        });    
    };
    function reposition() {
        var modal  = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 2));
    }
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
    }
          
});

function isValidEmailAddress(emailAddress){
    var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
    return pattern.test(emailAddress);
}