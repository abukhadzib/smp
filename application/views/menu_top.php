
<header class="main-header">
    <!-- Logo -->
    <a href="<?=site_url('home')?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>SMP</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>System</b>MerchantPortal</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- Notifications: style can be found in dropdown.less -->
          <li>
            <a href="<?=site_url('home')?>">
              <i class="fa fa-home"></i>
            </a>
          </li>
          <li>
            <a href="<?=site_url('user_management/profile')?>">
              <i class="fa fa-user"></i>
            </a>
          </li>
          <li>
            <a href="<?=site_url('user_management/password')?>">
              <i class="fa fa-lock"></i>
            </a>
          </li>
         <!--
          <li class="dropdown notifications-menu">
            <a href="<?=site_url('home/video')?>" >
              <i class="fa fa-exchange"></i>
            </a>
          </li>
          
          <li class="dropdown notifications-menu">
            <a href="<?=site_url('home/canvas')?>">
              <i class="fa fa-th"></i>
            </a>
          </li>
          -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?=$img?>avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs">Hi, <?=$name; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?=$img?>avatar5.png" class="img-circle" alt="User Image">
                <p>
                  <?=$name; ?>
                  <small><?=$email; ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <li class="user-body">
                
                <div class="col-xs-12 text-center">
                  <a href="<?=site_url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
                
              </li>
              
              <!--
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" id="user-profile" data-toggle="modal" class="btn btn-default btn-flat" data-target="#dataProfileModal">Profile</a>
                </div>
                 
                <div class="pull-right">
                  <a href="<?//=site_url('logout')?>" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
              -->
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
          -->
        </ul>
      </div>
    </nav>
  </header>
            