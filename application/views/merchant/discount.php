<section class="content">
<div class="row">
    <div class="col-xs-12">
         <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Discount Merchant <span> > List</span></h3>
    </div> 
    
        <div class="box-body">
            <div  style="margin-bottom: 1px" align="right"> 
                <button type="button" class="btn btn-danger" id="tambahDiscount" title="add Merchant">Tambah <i class="fa fa-plus"></i> </button>
            </div>
        </div> 
        <div class="col-xs-12" id="tb">
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title"><b>Data Discount </b> <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <div class="box-body">
            <section id="no-more-tables">
            <div class="table-responsive">												
                    <table id="tbdiscount" class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff; overflow: auto;">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>From Date</th>
                                    <th>To Date</th>
                                    <th>Discount Name</th>
                                    <th>Nominal Get Discount</th>
                                    <th>Discount Type</th>
                                    <th>Discount</th>
                                    <th>Max Discount</th>
                                    <th>Max Per User</th>
                                    <th>Budget</th>
                                    <th>Status</th>
                                    <th>Apps</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1; 
                            if ( count($content) > 0 ) {
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td data-title="#"><?=($page*$perpage)+$no;?>&nbsp;</td>
                                <td data-title="from"><?=$row->from_date;?>&nbsp;</td>
                                <td data-title="To"><?=$row->to_date;?>&nbsp;</td>
                                <td data-title="Name"><?=$row->name;?>&nbsp;</td>
                                <td data-title="Nom Get Discount"><?=$row->nominal_get_discount;?>&nbsp;</td>
                                <td data-title="Type"><?=$row->type;?>&nbsp;</td>
                                <td data-title="Discount"><?=$row->discount;?>&nbsp;</td>
                                <td data-title="Maximum Discount"><?=$row->max_discount;?>&nbsp;</td>
                                <td data-title="Maximum per User"><?=$row->limit_user;?>&nbsp;</td>
                                <td data-title="Budget"><?=$row->limit_balance;?>&nbsp;</td>
                                <td data-title="Status"><?=$row->status;?>&nbsp;</td>
                                <td data-title="Apps"><?=$row->app_code;?>&nbsp;</td>
                                <td data-title="Action">
                                <a class="editDiscount" href="#" title="Ubah" data-discount-name="<?=$row->app_code;?>" data-discount-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataDiscountModal"><i class="fa fa-edit"></i></a>&nbsp;
                                <a class="deleteDiscount" href=#" title="Hapus" data-discount-name="<?=$row->app_code;?>" data-discount-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteDiscountModal"><i class="fa fa-trash-o" style="color: red;"></i></a>
                               </td>
                            </tr>                         
                        <?php 
                            $no++; 
                            } } else {
                        ?>
                            <tr>
                                <td colspan="15"style="text-align: center;">No Data</td>
                            </tr>
                        <?php } ?>
                            </tbody>
                    </table>        	
            </div> 
            </section>
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>  
        </div>
        </div>    
    </div>
    <div class="col-xs-12" id="adddiscount" style="display: none;">
            <div class="box box-danger">
                <div class="box-header">
                  <h3 class="box-title">Input Data Discount  <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/Preloader_3.gif')?>" /></span></h3>
                  
                </div>
                <form enctype="multipart/form-data" id="form-discount" class="form-horizontal" method="post" >
                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount Name * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="name" id="name" class="form-control" placeholder="Discount Name" >
                          </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Nominal Get Discount * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-key"></i>
                            </div>
                              <input type="text" name="get_discount" id="get_discount" class="form-control" placeholder="Nominal Get Discount" >
                          </div>
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount Type  * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="type" id="type" class="form-control select2" style="width: 100%;">
                            <option value="">--- Discount Type ---</option>
                            <option value="Percentage"> Percentage (%) </option>
                            <option value="Nominal"> Nominal </option>
                          </select>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-phone"></i>
                            </div>
                              <input type="text" name="discount" id="discount" class="form-control" placeholder="Discount" onkeyup="myFunction()">
                          </div>
                        </div>
                   </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Maximum Discount * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-envelope-o"></i>
                            </div>
                              <input type="text" name="max_discount" id="max_discount" class="form-control" placeholder="Maximum Discount">
                          </div>
                        </div>
                   </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Fee * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="fee" id="fee" class="form-control" placeholder="Fee">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Apps ( App Code Taps ) *:</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-home"></i>
                            </div>
                              <input type="text" name="app_code" id="app_code" class="form-control" placeholder="Apps">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>From Date * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar-o"></i>
                            </div>
                            <input class="form-control" placeholder="From Date" style="width: 100%;" name="from_date" id="from_date" required>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>To Date * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-calendar-o"></i>
                            </div>
                            <input class="form-control" placeholder="To Date" style="width: 100%;" name="to_date" id="to_date" required>
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Max Per User :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                              <input type="text" name="maxuser" id="maxuser" class="form-control" placeholder="max user">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Budget Balance :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-user"></i>
                            </div>
                              <input type="text" name="budget" id="budget" class="form-control" placeholder="Budget Balance">
                          </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                          <label>Discount status  * :</label>
                          <div class="input-group">
                            <div class="input-group-addon">
                              <i class="fa fa-credit-card"></i>
                            </div>
                          <select name="status" id="status" class="form-control select2" style="width: 100%;">
                            <option value="COSTUM"> Costum </option>
                            <option value="ALL MERCHANT">All Merchant</option>
                            <option value="EXPIRED"> Expired </option>
                          </select>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                <em>* Wajib diisi.</em>
                    <div align="center">
                        <button type="reset" class="btn btn-danger" id="reset"><i class="fa fa-close"></i> Batal</button>
                        <button type="button" class="btn btn-danger" id="simpan"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>
            </div>
        </div>
</div>                       
</section>


<!-- Modal Hapus -->
<div class="modal fade" id="deleteDiscountModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteMerchant">Hapus Data Discount</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Discount <span id="namaDiscountHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteDiscountModalYes" data-Discount-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiDiscountModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiDiscount">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiDiscount">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiDiscount" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiDiscountOK">OK</button>
            </div>
        </div>
    </div>
</div>

<!--
<script type="text/javascript" src="<?=$js;?>filereader.js"></script>
<script type="text/javascript" src="<?=$js;?>qrcodelib.js"></script>
<script type="text/javascript" src="<?=$js;?>json-min.js"></script>
<script type="text/javascript" src="<?=$js;?>jquery.qrcode.min.js"></script>
-->

<script>
//Date range picker with time picker
//$('#datetime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});

$("#get_discount,#discount,#max_discount,#fee,#budget,#maxuser").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) ||
         // Allow: Ctrl+C
        (e.keyCode == 67 && e.ctrlKey === true) ||
         // Allow: Ctrl+X
        (e.keyCode == 88 && e.ctrlKey === true) ||
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
    
$(function () {
    $("#tbdiscount").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
  
$('#from_date, #to_date').datepicker({format: 'yyyy-mm-dd'});

$("#tambahDiscount").click(function() {  
    $("#tb").hide();
    $("#adddiscount").show();
});

$(document).ready(function(){
    $("#type").change(function() {  
        if ( $('#type').val()=='Nominal' ){
           $("#discount").attr('maxlength', 7);
        }
        if ( $('#type').val()=='Percentage' ){
           $("#discount").attr('maxlength', 2);
        }
    });
});

$("#reset").click(function() {  
    $("#tb").show();
    $("#adddiscount").hide();
});

$('#simpan').click(function(){
    
    if ( $('#name').val()=='' ){
        $('#name').focus();
        return false;
    }
    
    if ( $('#get_discount').val()=='' || $('#get_discount').val()===null ){
        $('#get_discount').focus();
        return false;
    }
    if ( $('#type').val()=='' ){
        $('#type').focus();
        return false;
    }
    if ( $('#discount').val()=='' ){
        $('#discount').focus();
        return false;
    }
    
    if ( $('#max_discount').val()=='' ){
        $('#max_discount').focus();
        return false;
    }
    
    if ( $('#fee').val()=='' ){
        $('#fee').focus();
        return false;
    }
    
    if ( $('#app_code').val()=='' ){
        $('#app_code').focus();
        return false;
    }
    
    if ( $('#rekening').val()=='' ){
        $('#rekening').focus();
        return false;
    }
    
    if ( $('#daterange').val()=='' ){
        $('#daterange').focus();
        return false;
    }
    
    if ( $('#maxuser').val()=='' ){
        $('#maxuser').focus();
        return false;
    }
    
    var url = '<?=site_url('merchant/discount_add')?>';       
    //$(this).attr('data-loading-text','Proses Simpan...').button('loading');         
    $.post(url,$('#form-discount').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
        //alert(result.Msg);
       showNotifikasi('notifikasiDiscountModal',result.Msg); 
       $('#statusNotifikasiDiscount').val(result.success);
    });   
});

$('.editDiscount').click(function() {
    var id = $(this).attr('data-discount-id');
    var url = '<?=site_url('merchant/discount_edit')?>'+'/'+id;
    window.location = url;
});

$(".deleteDiscount").click(function() {
    var name = $(this).attr('data-discount-name');
    var id = $(this).attr('data-discount-id');
	$('#deleteDiscountModalYes').attr('data-discount-id',id);
    $('#namaDiscountHapus').html('<b>'+name+'</b>');
});    

$('#deleteDiscountModalYes').click(function() {
    var id  = $(this).attr('data-discount-id');
    var url = '<?=site_url('merchant/discount_delete')?>'+'/'+id; 
    $(this).attr('data-loading-text','Proses Delete...').button('loading');        
    $.post(url,{action:'delete',id:id},
    function(result){
        var result = eval('('+result+')');
        $('#deleteCodeModal').modal('hide');
        showNotifikasi('notifikasiDiscountModal',result.Msg);
        $('#statusNotifikasiDiscount').val(result.success);
    });        
});

$("#buttonNotifikasiDiscountOK").click(function() {
    if ( $('#statusNotifikasiDiscount').val() ) {
        window.location.reload();
        //window.location = "<?=site_url('merchant/discount')?>";
    } 
});

function showNotifikasi(idModal,pesan){
    $('#'+idModal+' .modal-body').html(pesan);
    $('#'+idModal).modal('show');
    $('body .modal-backdrop').hide();

}

</script>