<section class="content">
    <section class="content-header">
        <h1>
          <i class="fa fa-barcode"></i> Generate Kode Merchant
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-barcode"></i> Merchant</a></li>
          <li><a href="#">Generate </a></li>
          <li class="active">Code</li>
        </ol>
      </section>

    
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Silahkan Masukan Kode Merchant</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <!--<form action="<?//=site_url('cashier/addProduct')?>" method="post" id="form-qrcode">-->
            <form id="form-qrcode">
            <div class="box-body">
            <div class="col-xs-12">
                <div class="form-group">
                  <label for="code" class="col-xs-4 control-label">Kode Merchant</label>
                    <div class="col-xs-8">
                    <input type="text" class="form-control"  id="code" name="code" placeholder="Kode Merchant" maxlength="6" required>
                    <span style="color:red;">* Maximum Code 6 Digit</span>
                    </div>
                </div>
            </div>
            </div><!-- /.box-body -->

            <div align="center" class="box-footer">
                <button type="reset" class="btn btn-danger" id="reset"><i class="fa fa-close"></i> Reset</button>
                <button type="submit" class="btn btn-danger" id="saveProduct"><i class="fa fa-qrcode"></i> Generate</button>
            </div>
          </form>
        </div><!-- /.box -->
      </div>
    </div>
</section>
</section>
   
	
<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiMerchantModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiMerchant">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiMerchant" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiMerchantOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script>

$('#saveProduct').click(function(){
    
    if ( $('#code').val()=='' ){
        $('#code').focus();
        return false;
    }
   
    var url = '<?=site_url('merchant/qrcode')?>';       
    //$(this).attr('data-loading-text','Generate...').button('loading');         
    $.post(url,$('#form-qrcode').serialize(),
    function(result){
        var result = eval('('+result+')');
        if ( result.success );  
       showNotifikasi('notifikasiMerchantModal',result.Msg); 
       $('#statusNotifikasiMerchant').val(result.success);
    });  
    return false;
});

$("#buttonNotifikasiMerchantOK").click(function() {
    if ( $('#statusNotifikasiMerchant').val() ) {
        window.location="<?=site_url('merchant/showCode/');?>/"+$('#code').val();
    } 
});

     function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    
    </script>