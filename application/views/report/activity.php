<section class="content">
<div class="row">
    <div class="col-xs-12">
            <h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> Laporan <span> > Activity</span></h3>
    </div> 
    <div class="box-body">
        <div class="col-xs-12">
			<section id="no-more-tables">
				<div class="table-responsive" style="margin-bottom:40px">												
						<table id="tbactivity" class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff">
								<thead>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>User</th>
										<th>Action</th>
										<th>Modul</th>
                                                                                <!--<th>Data</th>-->
										<th>IP</th>
										<th>User Agent</th>
									</tr>
								</thead>
								<tbody>
										<?php 
								$no = 1; 
								if ( count($content) > 0 ) {
								foreach ($content as $row){ 
							?>
								<tr>
									<td data-title="#"><?=($page*$perpage)+$no;?></td>
									<td data-title="Date"><?=date("d-m-Y H:i:s", strtotime($row->created_date));?></td>
									<td data-title="Name"><?=$row->user_name;?></td>
									<td data-title="Action"><?=$row->action;?></td>
									<td data-title="Modul"><?=$row->modul;?></td>
                                                                        <!--<td data-title="Data"><?//=$row->data;?></td>-->
									<td data-title="IP"><?=$row->ip;?></td>
									<td data-title="UA"><?=$row->ua;?></td>
								</tr>                         
							<?php 
								$no++; 
								} } else {
							?>
								<tr>
									<td colspan="7"style="text-align: center;">No Data</td>
								</tr>
							<?php } ?>
								</tbody>
								<!--
								<tfoot>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>User</th>
										<th>Action</th>
										<th>Modul</th>
										<th>IP</th>
										<th>User Agent</th>
									</tr>                    
								</tfoot>
								-->
						</table>        	
				</div>    
			</section>
            <div align="right" style=" margin-top: -25px;">
                <?//=$pagination;?>
            </div>           
        </div>    
    </div>
</div>                       
</section>
<script>
  $(function () {
    $("#tbactivity").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<!-- Modal Hapus -->
<div class="modal fade" id="deletePesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDeletePesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeletePesan">Hapus Data Pesan</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Pesan <span id="namaPesanHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deletePesanModalYes" data-Pesan-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelDataPesan">
    <div class="modal-dialog" role="document" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataPesan"></span> Data Pesan <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataPesanModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanPesanModal" data-Pesan-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiPesanModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiPesan">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiPesan">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiPesan" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiPesanOK">OK</button>
            </div>
        </div>
    </div>
</div>
