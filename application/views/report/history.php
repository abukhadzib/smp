<section class="content">
<div class="right_col" role="main">
	<div class="">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-barcode"></i> Report <span> > Laporan Transaksi </span></h3>
	</div>
    <div class="col-xs-12">
                      
    </div>    
   <div class="col-xs-12">                     
            <div class="table-responsive" style="overflow: auto">												
                    <table class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff">
                            <thead>
                            <tr>
                            <th>#</th>
                            <th>date</th>
                            <th>Trx Code</th>
                            <th>Kode </th>
                            <th>Nama </th>
                            <th>Harga</th>
                            <th>Jumlah</th>
                            <th>Total</th>
                            </thead>
                            <tbody>
                                    <?php 
                            $no = 1; 
                            foreach ($content as $row){ 
                        ?>
                            <tr>
                                <td><?=($page*$perpage)+$no;?></td>
                                <td><?=$row->date;?></td>
                                <td><?=$row->trxcode;?></td>
                                <td><?=$row->kode;?></td>
                                <td><?=$row->nama;?></td>
                                <td><?=$row->harga;?></td>
                                <td><?=$row->jml;?></td>
                                <td><?=($row->jml*$row->harga);?></td>

                            </tr>                         
                        <?php 
                            $no++; 
                            }
                        ?>
                            </tbody>
                    </table>        	
            </div>       
            <div align="right" style=" margin-top: -25px;">
                <?=$pagination;?>
            </div>           
        </div>      
	</div>                       
</div>
</div>
</section>
  
<!-- Modal Hapus -->
<div class="modal fade" id="deleteCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteCode">Hapus Data Product</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data Product <span id="namaCodeHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteCodeModalYes" data-code-id="">Ya</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiCodeModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiCode">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiCode">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiCode" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiCodeOK">OK</button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
   // data
    
    $(".deleteCode").click(function() {
        var name = $(this).attr('data-code-name');
        var id = $(this).attr('data-code-id');
		$('#deleteCodeModalYes').attr('data-code-id',id);
        $('#namaCodeHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteCodeModalYes').click(function() {
        var id  = $(this).attr('data-code-id');
        var url = '<?=site_url('cashier/code_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteCodeModal').modal('hide');
            showNotifikasi('notifikasiCodeModal',result.Msg);
            $('#statusNotifikasiCode').val(result.success);
        });        
	});
       
    $("#buttonNotifikasiCodeOK").click(function() {
        if ( $('#statusNotifikasiCode').val() ) {
            window.location.reload();
        } 
	});        
        
        
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    // End data
</script>