<br />
<div class="row">
	<div class="col-lg-8">
        <form enctype="multipart/form-data" id="form-menu" class="form-horizontal" method="post">
            <input type="hidden" name="id" value="<?=$mainmenu->id?>">
            <div class="form-group">
                <label for="name" class="col-xs-4 control-label">Nama *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" required="required" id="name" name="name" placeholder="Nama" value="<?=$mainmenu->name?>">
                  
                </div>
            </div>
            <div class="form-group">
                <label for="Tipe" class="col-xs-4 control-label">Tipe *</label>
                <div class="col-xs-7">
                    <select name="tipe" required="required" class="form-control" id="tipe">
                        <option value="" selected=""> --Tipe--</option>
                        <option value="H" <?php if($mainmenu->tipe=="H") echo 'selected="selected"'; ?>>Menu</option>
                        <option value="S" <?php if($mainmenu->tipe=="S") echo 'selected="selected"'; ?>>Submenu</option>
                        <option value="SS" <?php if($mainmenu->tipe=="SS") echo 'selected="selected"'; ?>>Sub Submenu</option>
                        </select>
                </div>
            </div> 
            <!--
            <div class="form-group">
                <label for="Tipe" class="col-xs-4 control-label">Tipe *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" required="required" id="tipe" name="tipe" placeholder="Tipe">
                </div>
            </div> 
            
            <div class="form-group">
                <label for="address1" class="col-xs-4 control-label">Header</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="header" name="header" placeholder="Header">
                </div>
            </div>-->
            <div class="form-group">
                <label for="Header" class="col-xs-4 control-label">Header *</label>
                <div class="col-xs-7">
                    <select name="header" required="required" class="form-control" id="header">
                        <option value="0" selected=""> --Header--</option>
                        <? if ($menu) { foreach($menu as $row_menu) { ?>
                        <option value="<?=$row_menu->id?>" <?=$row_menu->id==$mainmenu->header?' selected="selected"':''?>><?=$row_menu->name?></option>
                        <? } } ?>
                        </select>
                </div>
            </div> 
            <div class="form-group">
                <label for="Url" class="col-xs-4 control-label">Url *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" id="url" name="url" placeholder="Url" value="<?=$mainmenu->url?>">
                   
                </div>
            </div> 
            <div class="form-group">               
                <label for="Icon" class="col-xs-4 control-label">Icon *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control has-feedback-left" name="icon" id="icon" placeholder="Icon" value="<?=$mainmenu->icon?>">
                   
                </div>
            </div> 
            <div class="form-group">
                <label for="Sort" class="col-xs-4 control-label">Sort *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="sort" name="sort" placeholder="Sort" value="<?=$mainmenu->sort?>">
                </div>
            </div>
            
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
