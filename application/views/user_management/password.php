<section class="content">
    <section class="content-header">
        <h1>
          <i class="fa fa-lock"></i> Ganti Password
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-lock"></i> Ganti</a></li>
          <li><a href="#">Password</a></li>
        </ol>
      </section>

    
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <!-- left column -->
      <div class="col-md-6">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Silahkan Masukan Password Anda</h3>
          </div><!-- /.box-header -->
          <!-- form start -->
          <div class="box-body">
            <div class="col-lg-10">
                <form enctype="multipart/form-data" id="form-password" class="form-horizontal" method="post">
                <div class="form-group">                
                    <label for="password" class="col-xs-4 control-label">Password Lama *</label>
                    <div class="col-xs-8">
                        <input type="password" class="form-control" id="password_old" name="password_old" placeholder="Password Lama" autofocus>
                    </div>
                </div>  
                <div class="form-group">                
                    <label for="password" class="col-xs-4 control-label">Password Baru *</label>
                    <div class="col-xs-8">
                        <input type="password" class="form-control" id="password_new" name="password_new" placeholder="Password Baru">
                    </div>
                </div>  
                <div class="form-group">                
                    <label for="password" class="col-xs-4 control-label">Konfirmasi Password *</label>
                    <div class="col-xs-8">
                        <input type="password" class="form-control" id="password_konfirm" name="password_konfirm" placeholder="Konfirmasi Password">
                    </div>
                </div>  
                    <em>*) Wajib diisi.</em></br>
                <em>*Anda akan otomatis logout setelah penggantian password Berhasil.</em>
		</form>
            </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <span id="dataPasswordModalURL" style="display: none;"></span>
                <button type ="reset" class="btn btn-default" onclick="location.href='<?php echo site_url('home')?>'">Back</button>
                <button type="button" class="btn btn-success" id="simpanPasswordModal" data-profile-id=""><i class="fa fa-floppy-o"></i> Simpan</button>
            </div>
        </div><!-- /.box -->
      </div>
    </div>
</section>
</section>
