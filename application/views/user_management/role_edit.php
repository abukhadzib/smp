<br />
<div class="row">
	<div class="col-lg-12">		 
		<form enctype="multipart/form-data" id="form-role" class="form-horizontal" method="post">
            <input type="hidden" name="role_id" placeholder="Role" value="<?=$role->id;?>">
            <div class="form-group">
                <label for="name" class="col-xs-2 control-label">Role</label>
                <div class="col-xs-6">
                    <input type="text" class="form-control" id="role_name" name="role_name" placeholder="Nama Role" value="<?=$role->name;?>" autofocus>
                </div>
                <div class="col-xs-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="is_admin"<?=$role->is_admin=='Y'?' checked="checked"':'';?>> Is Admin
                        </label>
                    </div>
                </div>
            </div> 
              
            <div class="row">												
            	<table class="table table-bordered table-hover">
            		<thead>
                        <tr>
            				<th style="text-align:center;">Menu</th>
            				<!--<th style="text-align:center; width: 10%;">#</th>-->
                            <th style="text-align:center;">Create</th>
                            <th style="text-align:center;">Read</th>
                            <th style="text-align:center;">Update</th>
                            <th style="text-align:center;">Delete</th>
            			</tr>
            		</thead>
            		<tbody>
            			<?php 
						foreach ($menu as $row)
						{ 
							$create_role = '0';
							$read_role   = '0';
							$update_role = '0';
							$delete_role = '0';
							
							foreach ($role_menu as $row2){
								if ( $row2->menu_id == $row->id ) {
									$create_role = $row2->create_;
									$read_role   = $row2->read_;
									$update_role = $row2->update_;
									$delete_role = $row2->delete_;
								}
							}
						?>
						<tr<?=$row->tipe=='H' ? ' style="background-color:#f0f0f0;"' : ''?>>
                            <td><?=($row->tipe=='H' ? '' : '&nbsp;&nbsp;&nbsp;&nbsp;') . $row->name;?></td>
                            <!--<td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" class="checkAll"<?=$create_role=='1'||$read_role=='1'||$update_role=='1'||$delete_role=='1'?' checked="checked"':''?>>
                                    </label>
                                </div>
                                <? } ?>
                            </td>-->
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="create[]" class="checkRole checkCreate createCheck" value="<?=$row->id;?>"<?=$create_role=='1'?' checked="checked"':''?>>
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="read[]" class="checkRole checkRead readCheck" value="<?=$row->id;?>"<?=$read_role=='1'?' checked="checked"':''?>>
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                               <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="update[]" class="checkRole checkUpdate" value="<?=$row->id;?>"<?=$update_role=='1'?' checked="checked"':''?><?=$read_role=='1'?'':' disabled="disabled"'?>>
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                            <td style="text-align:center;">    
                                <? if($row->tipe!='H') { ?>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="delete[]" class="checkRole checkDelete" value="<?=$row->id;?>"<?=$delete_role=='1'?' checked="checked"':''?><?=$read_role=='1'?'':' disabled="disabled"'?>>
                                    </label>
                                </div>
                                <? } ?>
                            </td>
                        </tr>                         
                        <?php } ?>
            		</tbody>
            	</table>        	
            </div>
		</form>      
	</div>  
</div>
