<section class="content">
<div class="right_col" role="main">
	<div class="">

<div class="row">
	<div class="col-xs-12">
		<h3 class="page-header txt-color-blueDark"><i class="fa fa-lg fa-fw fa-user"></i> User Management <span> > User </span></h3>
	</div>
    <div class="col-xs-12">
        <div class="pull-right" style="margin-bottom: 14px;">
            <button type="button" class="btn btn-primary" id="tambahUser" data-toggle="modal" data-target="#dataUserModal"><i class="fa fa-plus"></i> Tambah Data User</button>
        </div>                
    </div>    
    <div class="col-xs-12">                     
        <div class="table-responsive" style="overflow: auto">												
        	<table class="table table-bordered table-hover table-striped" style="margin-bottom: auto; background: #fff">
        		<thead>
                        <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th style="width: 5%;">Action</th>
        			</tr>
        		</thead>
        		<tbody>
        			<?php 
                        $no = 1; 
                        foreach ($content as $row){ 
                    ?>
                        <tr>
                            <td><?=($page*$perpage)+$no;?></td>
                            <td><?=$row->name;?></td>
                            <td><?=$row->username;?></td>
                            <td><?=$row->email;?></td>
                            <td><?=$row->role_name;?></td>
                            <td>
                            <a class="editUser" href="#" title="Edit" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>" data-toggle="modal" data-target="#dataUserModal"><i class="fa fa-edit"></i></a>&nbsp;
                            <a class="deleteUser" href=#" title="Remove" data-user-name="<?=$row->name;?>" data-user-id="<?=$row->id;?>" data-toggle="modal" data-target="#deleteUserModal"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>                         
                    <?php 
                        $no++; 
                        }
                    ?>
        		</tbody>
        	</table>        	
        </div>       
        <div align="right" style=" margin-top: -25px;">
            <?=$pagination;?>
        </div>           
    </div>    
	</div>                       
</div>
</div>
    </section>
    
<!-- Modal Hapus -->
<div class="modal fade" id="deleteUserModal" tabindex="-1" role="dialog" aria-labelledby="labelDeleteUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title" id="labelDeleteUser">Hapus Data User</h4>
            </div>
            <div class="modal-body">  
                <i class="fa fa-lg fa-fw fa-warning"></i>Yakin mau hapus data User <span id="namaUserHapus"></span> ?                  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="deleteUserModalYes" data-user-id="">Ya</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Data Tambah dan Edit -->
<div class="modal fade" id="dataUserModal" tabindex="-1" role="dialog" aria-labelledby="labelDataUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <!--button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
                <h4 class="modal-title"><span id="labelDataUser"></span> Data User <span id="loading-data" style="display:none;margin-left: 10px;"><img src="<?=base_url('assets/img/loading.gif')?>" /></span></h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="dataUserModalURL" style="display: none;"></span>
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" id="simpanUserModal" data-user-id="">Simpan</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Notifikasi -->
<div class="modal fade" id="notifikasiUserModal" tabindex="-1" role="dialog" aria-labelledby="labelNotifikasiUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="labelNotifikasiUser">Notifikasi</h4>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <span id="statusNotifikasiUser" style="display: none;"></span>
                <button type="button" class="btn btn-success" data-dismiss="modal" id="buttonNotifikasiUserOK">OK</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    // User
    $('#tambahUser').click(function() {
        var url = '<?=site_url('user_management/user_add')?>';
        $('#labelDataUser').text('Tambah');  
        $('#dataUserModal .modal-body').html('');
        $('#dataUserModal .modal-body').load(url);
        $('#dataUserModalURL').text(url);
    });
    $('.editUser').click(function() {
        var id = $(this).attr('data-user-id');
		var url = '<?=site_url('user_management/user_edit')?>'+'/'+id;
        $('#labelDataUser').text('Rubah');
        $('#dataUserModal .modal-body').html('');
        $('#dataUserModal .modal-body').load(url);
        $('#dataUserModalURL').text(url);
    });
    $(".deleteUser").click(function() {
        var name = $(this).attr('data-user-name');
        var id = $(this).attr('data-user-id');
		$('#deleteUserModalYes').attr('data-user-id',id);
        $('#namaUserHapus').html('<b>'+name+'</b>');
	});    
    $('#deleteUserModalYes').click(function() {
        var id  = $(this).attr('data-user-id');
        var url = '<?=site_url('user_management/user_delete')?>'+'/'+id;        
        $.post(url,{action:'delete',id:id},
        function(result){
            var result = eval('('+result+')');
            $('#deleteUserModal').modal('hide');
            showNotifikasi('notifikasiUserModal',result.Msg);
            $('#statusNotifikasiUser').val(result.success);
        });        
	});
        
    function isValidEmailAddress(emailAddress){
        var pattern = new RegExp(/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/);
        return pattern.test(emailAddress);
    }
    
    $('#simpanUserModal').click(function(){
        if ( $('#role').val()=='' || $('#role').val()===null ){
            $('#role').focus();
            return false;
        }
        if ( $('#name').val()=='' ){
            $('#name').focus();
            return false;
        }
        if ( $('#no_identitas').val()=='' ){
            $('#no_identitas').focus();
            return false;
        }
        if ( !$.isNumeric( $('#no_identitas').val() ) ){
            showNotifikasi('notifikasiUserModal','No Identitas harus angka !');
            $('#no_identitas').focus();
            return false;
        }
        if ( $('#phone').val()=='' ){
            $('#phone').focus();
            return false;
        }
        if ( !$.isNumeric( $('#phone').val() ) ){
            showNotifikasi('notifikasiUserModal','No Telepon harus angka !');
            $('#phone').focus();
            return false;
        }
        if ( $('#email').val()=='' ){
            $('#email').focus();
            return false;
        }
        if ( !isValidEmailAddress($('#email').val()) ){
            showNotifikasi('notifikasiUserModal','Alamat Email tidak valid !');
            $('#email').focus();
            return false;
        }
        if ( $('#username').val()=='' ){
            $('#username').focus();
            return false;
        }
        
        if ( $('#passwordAdd').val()=='' ){
            $('#passwordAdd').focus();
            return false;
        }
        if ( $('#passwordConfirmAdd').val()=='' ){
            $('#passwordConfirmAdd').focus();
            return false;
        }
        
        if ( $('#passwordConfirmAdd').val()!=$('#passwordAdd').val() ){
            showNotifikasi('notifikasiUserModal','Password dan Konfirmasi Password tidak sama !');
            $('#passwordConfirmAdd').focus();
            return false;
        }
        
        if ( $('#passwordConfirm').val()!=$('#password').val() ){
            showNotifikasi('notifikasiUserModal','Password dan Konfirmasi Password tidak sama !');
            $('#passwordConfirm').focus();
            return false;
        }
        var url = $('#dataUserModalURL').text();        
        $(this).attr('data-loading-text','Proses Simpan...').button('loading');         
        $.post(url,$('#form-user').serialize(),
        function(result){
            var result = eval('('+result+')');
            if ( result.success ) $('#dataUserModal').modal('hide');  
            //alert(result.Msg);
           showNotifikasi('notifikasiUserModal',result.Msg); 
           $('#statusNotifikasiUser').val(result.success);
        });   
    });
    $("#buttonNotifikasiUserOK").click(function() {
        if ( $('#statusNotifikasiUser').val() ) {
            window.location.reload();
        } else {
            $('#simpanUserModal').button('reset'); 
        }
	});        
        
        
    function showNotifikasi(idModal,pesan){
        $('#'+idModal+' .modal-body').html(pesan);
        $('#'+idModal).modal('show');
        $('body .modal-backdrop').hide();
    }
    // End User
</script>