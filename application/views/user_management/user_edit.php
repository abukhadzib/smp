<br />
<div class="row">
	<div class="col-lg-8">
        <form enctype="multipart/form-data" id="form-user" class="form-horizontal" method="post">
            <input type="hidden" name="user_id" value="<?=$user->id?>">
			<div class="form-group">
                <label for="name" class="col-xs-4 control-label">Role *</label>
                <div class="col-xs-7">
                    <select name="role" class="form-control" id="role">
						<option value="" selected="" disabled="" > --Role--</option>
                        <? if ($role) { foreach($role as $row_role) { ?>
						<option value="<?=$row_role->id?>"<?=$row_role->id==$user->role_id?' selected="selected"':''?>><?=$row_role->name?></option>
                        <? } } ?>
					</select>
                </div>
            </div> 
            <div class="form-group">
                <label for="name" class="col-xs-4 control-label">Nama *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="name" name="name" placeholder="Nama" value="<?=$user->name?>" autofocus>
                </div>
            </div>
            <div class="form-group">
                <label for="no_identitas" class="col-xs-4 control-label">No Identitas *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="no_identitas" name="no_identitas" placeholder="No Identitas" value="<?=$user->no_identitas?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="address1" class="col-xs-4 control-label">Alamat</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="address1" name="address1" placeholder="Alamat" value="<?=$user->address1?>">
                </div>
            </div>
            <div class="form-group">
                <label for="phone" class="col-xs-4 control-label">No Telpon *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="No Telpon" value="<?=$user->phone?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="email" class="col-xs-4 control-label">Email *</label>
                <div class="col-xs-7">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?=$user->email?>">
                </div>
            </div> 
            <div class="form-group">
                <label for="username" class="col-xs-4 control-label">Username *</label>
                <div class="col-xs-7">
                    <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?=$user->username?>">
                </div>
            </div> 
            <div class="form-group" style="display: none;">
                <label for="pass" class="col-xs-4 control-label">Pass *</label>
                <div class="col-xs-7">
                    <input type="password" class="form-control" id="pass" name="pass" placeholder="Pass">
                </div>
            </div> 
            <div class="form-group">
                <label for="password" class="col-xs-4 control-label">Password *</label>
                <div class="col-xs-7">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
            </div> 
            <div class="form-group">
                <label for="passwordConfirm" class="col-xs-4 control-label">Konfirmasi *</label>
                <div class="col-xs-7">
                    <input type="password" class="form-control" id="passwordConfirm" name="passwordConfirm" placeholder="Konfirmasi Password">
                </div>
            </div>  
            <em>*) Wajib diisi.</em>
		</form>
	</div>  
</div>
