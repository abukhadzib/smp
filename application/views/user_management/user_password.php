<style type="text/css">
	form {
		margin-bottom: 50px;
		padding-bottom: 60px;
	}
	.fLeft, .fRight {
		width: 460px;
	}
</style>

<div id="pageHeader">
	<h1 class="pageTitle"><span class="fa fa-lg fa-fw fa-key"></span>Ubah Password</h1>
</div>

<?php if (isset($error_password) && !empty($error_password)): ?>
	<div class="alert alert-warning fade in">
		<button class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="alert-title"><span class="fa-fw fa fa-warning"></span>Warning</div>
		<div class="alert-content"><?=$error_password?></div>
	</div>
<?php endif; ?>
<?php if (isset($sukses_password) && !empty($sukses_password)): ?> 
	<div class="alert alert-success fade in">
		<button class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="alert-title"><span class="fa-fw fa fa-thumbs-o-up"></span>Succeed</div>
		<div class="alert-content"><?=$sukses_password?></div>
	</div>
<?php endif; ?>

<form enctype="multipart/form-data" id="form-signup" class="smart-form client-form" method="post">
	<fieldset>
		<input type="hidden" name="user_id" value="<?=$user->id?>">
		<div class="fLeft">
            <section>
				<label for="name">Password *</label>
				<input type="password" id="password" name="password" placeholder="Password">
			</section>
			<section>
				<label for="name">Password Baru *</label>
				<input type="password" id="password_baru" name="password_baru" placeholder="Password Baru">
			</section>
			<section>
				<label for="name">Konfirmasi Password Baru *</label>
				<input type="password" id="password_baru2" name="password_baru2" placeholder="Konfirmasi Password Baru">
			</section>
		</div>
		<div class="clear"></div>
	</fieldset>
	<div class="divider"></div>
	<fieldset>
		<div class="note">*) Wajib diisi.</div><br />
		<div class="fLeft"><button type="submit" class="btnBlue">Simpan</button></div>
		<div class="fRight"><a href="<?=site_url('')?>" class="button btnGreen">Batal</a></div>
	</fieldset>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		// Validation
		$("#form-signup").validate({
			// Rules for form validation
			rules : {
				password : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				password_baru : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				password_baru2 : {
					required : true,
					minlength : 3,
					maxlength : 20,
					equalTo : "#password_baru"
				}
			},

			// Messages for form validation
			messages : {
				password : {
					required : 'Masukkan Password'
				},
				password_baru : {
					required : 'Masukkan Password Baru'
				},
				password_baru2 : {
					required : 'Masukkan Konfirmasi Password',
					equalTo : 'Password dan Konrifmasi Password harus sama'
				}
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		}); 
	});
</script>