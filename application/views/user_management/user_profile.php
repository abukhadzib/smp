<style type="text/css">
	form {
		margin-bottom: 50px;
		padding-bottom: 60px;
	}
	.fLeft, .fRight {
		width: 460px;
	}
</style>

<div id="pageHeader">
	<h1 class="pageTitle"><span class="glyphicon glyphicon-user"></span>Pengaturan Akun</h1>
</div>

<?php if (isset($error_profile) && !empty($error_profile)): ?>
	<div class="alert alert-warning fade in">
		<button class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="alert-title"><span class="fa-fw fa fa-warning"></span>Warning</div>
		<div class="alert-content"><?=$error_profile?></div>
	</div>
<?php endif; ?>
<?php if (isset($sukses_profile) && !empty($sukses_profile)): ?> 
	<div class="alert alert-success fade in">
		<button class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>
		<div class="alert-title"><span class="fa-fw fa fa-thumbs-o-up"></span>Succeed</div>
		<div class="alert-content"><?=$sukses_profile?></div>
	</div>
<?php endif; ?>

<form enctype="multipart/form-data" id="form-signup" class="smart-form client-form" method="post">
	<fieldset>
		<input type="hidden" name="user_id" value="<?=$user->id?>">
		<div class="fLeft">
            <section>
				<label for="name">Nama *</label>
				<input type="text" id="name" name="name" placeholder="nama anda" value="<?=$user->name?>">
			</section>
			<section>
				<label for="address1">Alamat</label>
				<input type="text" id="address1" name="address1" placeholder="alamat anda" value="<?=$user->address1?>">
			</section>
			<section>
				<label for="zipcode">Kode Pos</label>
				<input type="text" id="zipcode" name="zipcode" placeholder="kode pos" value="<?=$user->zip?>">
			</section>
			<section>
				<label for="city">Kota</label>
				<input type="text" id="city" name="city" placeholder="kota" value="<?=$user->city?>">
			</section>
		</div>
		<div class="fRight">
            <section>
				<label for="address2">Propinsi</label>
				<input type="text" id="address2" name="address2" placeholder="propinsi" value="<?=$user->address2?>"></label>
			</section>
			<section>
				<label for="country">Negara</label>
				<input type="text" id="country" name="country" placeholder="negara" value="<?=$user->country?>">
			</section>
            <section>
				<label for="no_identitas">No Identitas *</label>
				<input type="text" id="no_identitas" name="no_identitas" placeholder="no identitas anda" value="<?=$user->no_identitas?>">
			</section>
			<section>
				<label for="phone">Telepon *</label>
				<input type="text" id="phone" name="phone" placeholder="nomor telepon anda" value="<?=$user->phone?>">
			</section>
		</div>
		<div class="clear"></div>
	</fieldset>
	<div class="divider"></div>
	<fieldset id="field_publisher" style="display: <?=$user->tipe=='Publisher'?'block':'none'?>;">
		<div class="fLeft">
            <section>
    			<label>Perusahaan</label>
    			<input type="text" name="company" placeholder="Perusahaan" value="<?=$user->company?>">
    		</section>
    		<section>
    			<label>Category</label>
    			<select name="category">
    				<option value="0" disabled="">&#151; Category &#151;</option>
    				<option value="News"<?=$user->category=='News'?' selected="selected"':''?>>News</option>
    				<option value="Business"<?=$user->category=='Business'?' selected="selected"':''?>>Business</option>
    				<option value="Technology"<?=$user->category=='Technology'?' selected="selected"':''?>>Technology</option>
    				<option value="Women"<?=$user->category=='Women'?' selected="selected"':''?>>Women</option>
    				<option value="Lifestyle"<?=$user->category=='Lifestyle'?' selected="selected"':''?>>Lifestyle</option>
    				<option value="Sport"<?=$user->category=='Sport'?' selected="selected"':''?>>Sport</option>
    				<option value="Youth"<?=$user->category=='Youth'?' selected="selected"':''?>>Youth</option>
    				<option value="Parenting"<?=$user->category=='Parenting'?' selected="selected"':''?>>Parenting</option>
    				<option value="Automotive"<?=$user->category=='Automotive'?' selected="selected"':''?>>Automotive</option>
    				<option value="Travel"<?=$user->category=='Travel'?' selected="selected"':''?>>Travel</option>
    			</select>
    		</section>
            <section>
    			<label>Web URL</label>
    			<input type="text" name="web_url" placeholder="Web URL" value="<?=$user->web_url?>">
    		</section>
    		<section>
    			<label>Mobile Site</label>
    			<input type="text" name="mobile_site" placeholder="Mobile Site" value="<?=$user->mobile_url?>">
    		</section>
    		<section>
    			<label>Nama Bank</label>
    			<input type="text" name="bank_account" placeholder="Nama Bank" value="<?=$user->bank?>">
    		</section>
        </div>
        <div class="fRight">
    		<section>
    			<label>Nama Pemilik Akun</label>
    			<input type="text" name="pemilik_akun" placeholder="Nama Pemilik Akun" value="<?=$user->pemilik_akun?>">
    		</section>
    		<section>
    			<label>No Rekening Bank</label>
    			<input type="text" name="bank_account_no" placeholder="No Rekening Bank" value="<?=$user->bank_acc_no?>">
    		</section>	
    		<section>
    			<label>NPWP</label>
    			<input type="text" name="npwp" placeholder="NPWP" value="<?=$user->npwp?>">
    		</section>
    		<section>
    			<label>Last Month Pageview</label>
    			<input type="text" name="pageview" placeholder="Last Month Pageview" value="<?=$user->pageview?>">
    		</section>
    		<section>
    			<label>Screenshot</label>
    			<input id="screnshoot" type="file" name="screnshoot" onchange="$('#label_screenshot').val(this.value)">
    		</section>
    		<section>
    			<label>Google Analytics Screenshot</label>
    			<input type="text" id="label_screenshot" placeholder="Google Analytics Screenshot" readonly="">
    		</section>
        </div>
		<div class="clear"></div><br />
		<div class="divider"></div>
	</fieldset>
	<fieldset>
		<div class="fLeft">
			<section>
				<label>Nama User *</label>
				<input type="text" name="username" placeholder="Nama User" value="<?=$user->username?>"></label>
			</section>
            <!--
			<section>
				<label>Password *</label>
				<input type="password" name="password" placeholder="Password" id="password"></label>
			</section>
            -->
		</div>
		<div class="fRight">
            <section>
    			<label>Email*</label>
    			<input type="email" name="email" placeholder="Email address" value="<?=$user->email?>">
    		</section>
			<!--
			<section>
				<label>Konfirmasi Password *</label>
				<input type="password" name="passwordConfirm" placeholder="Konfirmasi Password"></label>
			</section>
            -->
		</div>
		<div class="clear"></div>
	</fieldset>
	<div class="divider"></div>
	<fieldset>
		<div class="note">*) Wajib diisi.</div><br />
		<div class="fLeft"><button type="submit" class="btnBlue">Simpan</button></div>
		<div class="fRight"><a href="<?=site_url('')?>" class="button btnGreen">Batal</a></div>
	</fieldset>
</form>

<script type="text/javascript">
	$(document).ready(function() {
		// Validation
		$("#form-signup").validate({
			// Rules for form validation
			rules : {
				name : {
					required : true
				},
				phone : {
					required : true,
					number : true
				},
				no_identitas : {
					required : true,
					number : true
				},
				zipcode : {
					number : true
				},
				username : {
					required : true
				},
				email : {
					required : true,
					email : true
				}
                /*
                ,
				password : {
					required : true,
					minlength : 3,
					maxlength : 20
				},
				passwordConfirm : {
					required : true,
					minlength : 3,
					maxlength : 20,
					equalTo : "#password"
				}
                */
			},

			// Messages for form validation
			messages : {
				name : {
					required : 'Masukkan Nama'
				},
				phone : {
					required : 'Masukkan No Telepon',
					number : 'Masukan No Telepon dengan angka'
				},
				no_identitas : {
					required : 'Masukkan No Identitas',
					number : 'Masukkkan No Identitas dengan angka'
				},
				zipcode : {
					number : 'Masukkan Kode Pos dengan angka'
				},
				username : {
					required : 'Masukkan Nama User'
				},
				email : {
					required : 'Masukkan Alamat Email',
					email : 'Masukkan Alamat Email dengan benar'
				}
                /*
                ,
				password : {
					required : 'Masukkan Password'
				},
				passwordConfirm : {
					required : 'Masukkan Konfirmasi Password',
					equalTo : 'Password dan Konrifmasi Password harus sama'
				}
                */
			},

			// Do not change code below
			errorPlacement : function(error, element) {
				error.insertAfter(element);
			}
		}); 
	});
</script>