if (typeof WL !== 'object') {
	WL = {};
}
(function () {
    'use strict';
    
    var PACKAGE_NAME = 'redeem.event';
    
    var rx_one = /^[\],:{}\s]*$/,
    rx_two = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
    rx_three = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
    rx_four = /(?:^|:|,)(?:\s*\[)+/g,
    rx_escapable = /[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
    rx_dangerous = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
    meta = {'\b' :'\\b','\t' :'\\t','\n':'\\n','\f':'\\f','\r':'\\r','"':'\\"','\\':'\\\\'};

	function f(n) {
	    return n < 10 ? '0' + n : n;
	}	
	function this_value() {
	    return this.valueOf();
	}	
	function quote(string) {
	    rx_escapable.lastIndex = 0;
	    return rx_escapable.test(string) ? '"' + string.replace(rx_escapable, function (a) {
	        var c = meta[a];
	        return typeof c === 'string' ? c : '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
	    }) + '"' : '"' + string + '"';
	}	
	function str(key, holder, gap, mind,indent,replacer) {
	    var i, k, v, length, mind = gap, partial, value = holder[key],rep=replacer;
	    if (value && typeof value === 'object' && typeof value.toJSON === 'function') {
	        value = value.toJSON(key);
	    }
	    if (typeof rep === 'function') {
	        value = rep.call(holder, key, value);
	    }
	    switch (typeof value) {
	    case 'string':
	        return quote(value);
	
	    case 'number':
	        return isFinite(value) ? String(value) : 'null';
			
	    case 'boolean':
	    case 'null':
	        return String(value);
	
	    case 'object':
	        if (!value) {
	            return 'null';
	        }
	        gap += indent;
	        partial = [];
	
	        if (Object.prototype.toString.apply(value) === '[object Array]') {
	            length = value.length;
	            for (i = 0; i < length; i += 1) {
	                partial[i] = str(i, value) || 'null';
	            }
	            v = partial.length === 0 ? '[]' : gap ? '[\n' + gap + partial.join(',\n' + gap) + '\n' + mind + ']' : '[' + partial.join(',') + ']';
	            gap = mind;
	            return v;
	        }
	        if (rep && typeof rep === 'object') {
	            length = rep.length;
	            for (i = 0; i < length; i += 1) {
	                if (typeof rep[i] === 'string') {
	                    k = rep[i];
	                    v = str(k, value);
	                    if (v) {
	                        partial.push(quote(k) + (gap ? ': ' : ':') + v);
	                    }
	                }
	            }
	        } else {
	            for (k in value) {
	                if (Object.prototype.hasOwnProperty.call(value, k)) {
	                    v = str(k, value);
	                    if (v) {
	                        partial.push(quote(k) + (
	                            gap ? ': ' : ':') + v);
	                    }
	                }
	            }
	        }
	        v = partial.length === 0 ? '{}' : gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' + mind + '}' : '{' + partial.join(',') + '}';
	        gap = mind;
	        return v;
	    }
	}
    
	
	
	/*
	 * Mengubah Object XML menjadi Object JSON
	 * 
	 * WL.xml2json(xml, tab)
	 * 
	 */
	if (typeof WL.xml2json !== 'function') {
		WL.xml2json = function(xml, tab) {
			var X = {
				toObj : function(xml) {
					var o = {};
					if (xml.nodeType == 1) {
						if (xml.attributes.length)
							for ( var i = 0; i < xml.attributes.length; i++) {
								o["@" + xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue || "").toString();
							}
						if (xml.firstChild) {
							var textChild = 0, cdataChild = 0, hasElementChild = false;
							for ( var n = xml.firstChild; n; n = n.nextSibling) {
								if (n.nodeType == 1) {
									hasElementChild = true;
								} else if (n.nodeType == 3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) {
									textChild++;
								} else if (n.nodeType == 4) {
									cdataChild++;
								}
							}
							if (hasElementChild) {
								if (textChild < 2 && cdataChild < 2) {
									X.removeWhite(xml);
									for ( var n = xml.firstChild; n; n = n.nextSibling) {
										if (n.nodeType == 3) { // text node
											o["#text"] = X.escape(n.nodeValue);
										} else if (n.nodeType == 4) {
											o["#cdata"] = X.escape(n.nodeValue);
										} else if (o[n.nodeName]) {
											if (o[n.nodeName] instanceof Array) {
												o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
											} else {
												o[n.nodeName] = [o[n.nodeName], X.toObj(n) ];
											}
										} else {
											o[n.nodeName] = X.toObj(n);
										}
									}
								} else { // mixed content
									if (!xml.attributes.length) {
										o = X.escape(X.innerXml(xml));
									} else {
										o["#text"] = X.escape(X.innerXml(xml));
									}
								}
							} else if (textChild) { // pure text
								if (!xml.attributes.length) {
									o = X.escape(X.innerXml(xml));
								} else {
									o["#text"] = X.escape(X.innerXml(xml));
								}
							} else if (cdataChild) { // cdata
								if (cdataChild > 1) {
									o = X.escape(X.innerXml(xml));
								} else {
									for ( var n = xml.firstChild; n; n = n.nextSibling) {
										o["#cdata"] = X.escape(n.nodeValue);
									}
								}
							}
						}
						if (!xml.attributes.length && !xml.firstChild) {
							o = null;
						}
					} else if (xml.nodeType == 9) { // document.node
						o = X.toObj(xml.documentElement);
					} else {
						//alert("unhandled node type: " + xml.nodeType);
						throw new Error('unhandled node type: ' + xml.nodeType);
					}
					return o;
				},
				toJson : function(o, name, ind) {
					var json = name ? ("\"" + name + "\"") : "";
					if (o instanceof Array) {
						for ( var i = 0, n = o.length; i < n; i++) {
							o[i] = X.toJson(o[i], "", ind + "\t");
						}
						json += (name ? ":[" : "[") + (o.length > 1 ? ("\n" + ind + "\t" + o.join(",\n" + ind + "\t") + "\n" + ind) : o.join("")) + "]";
					} else if (o == null) {
						json += (name && ":") + "null";
					} else if (typeof (o) == "object") {
						var arr = [];
						for (var m in o) {
							arr[arr.length] = X.toJson(o[m], m, ind + "\t");
						}
						json += (name ? ":{" : "{") + (arr.length > 1 ? ("\n" + ind + "\t" + arr.join(",\n" + ind + "\t") + "\n" + ind) : arr.join("")) + "}";
					} else if (typeof (o) == "string") {
						json += (name && ":") + "\"" + o.toString() + "\"";
					} else {
						json += (name && ":") + o.toString();
					}
					return json;
				},
				innerXml : function(node) {
					var s = ""
					if ("innerHTML" in node) {
						s = node.innerHTML;
					} else {
						var asXml = function(n) {
							var s = "";
							if (n.nodeType == 1) {
								s += "<" + n.nodeName;
								for ( var i = 0; i < n.attributes.length; i++) {
									s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue || "").toString() + "\"";
								}
								if (n.firstChild) {
									s += ">";
									for ( var c = n.firstChild; c; c = c.nextSibling) {
										s += asXml(c);
									}
									s += "</" + n.nodeName + ">";
								} else {
									s += "/>";
								}
							} else if (n.nodeType == 3) {
								s += n.nodeValue;
							} else if (n.nodeType == 4) {
								s += "<![CDATA[" + n.nodeValue + "]]>";
							}
							return s;
						};
						for ( var c = node.firstChild; c; c = c.nextSibling) {
							s += asXml(c);
						}
					}
					return s;
				},
				escape : function(txt) {
					return txt.replace(/[\\]/g, "\\\\").replace(/[\"]/g, '\\"').replace(/[\n]/g, '\\n').replace(/[\r]/g, '\\r');
				},
				removeWhite : function(e) {
					e.normalize();
					for ( var n = e.firstChild; n;) {
						if (n.nodeType == 3) { // text node
							if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) {
								var nxt = n.nextSibling;
								e.removeChild(n);
								n = nxt;
							} else {
								n = n.nextSibling;
							}
						} else if (n.nodeType == 1) { // element node
							X.removeWhite(n);
							n = n.nextSibling;
						} else {
							n = n.nextSibling;
						}
					}
					return e;
				}
			};
			if (xml.nodeType == 9) {
				xml = xml.documentElement;
			}
			var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
			return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g,"")) + "\n}";
		};
	}
	

	/*
	 * Mengubah Object JSON menjadi  Object XML
	 * 
	 * WL.json2xml(v, name, ind)
	 * 
	 */
	if (typeof WL.json2xml !== 'function') {
		WL.json2xml = function(o, tab) {
			var toXml = function(v, name, ind) {
				var xml = "";
				if (v instanceof Array) {
					for ( var i = 0, n = v.length; i < n; i++) {
						xml += ind + toXml(v[i], name, ind + "\t") + "\n";
					}
				} else if (typeof (v) == "object") {
					var hasChild = false;
					xml += ind + "<" + name;
					for (var m in v) {
						if (m.charAt(0) == "@") {
							xml += " " + m.substr(1) + "=\"" + v[m].toString() + "\"";
						} else {
							hasChild = true;
						}
					}
					xml += hasChild ? ">" : "/>";
					if (hasChild) {
						for ( var m in v) {
							if (m == "#text") {
								xml += v[m];
							} else if (m == "#cdata") {
								xml += "<![CDATA[" + v[m] + "]]>";
							} else if (m.charAt(0) != "@") {
								xml += toXml(v[m], m, ind + "\t");
							}
						}
						xml += (xml.charAt(xml.length - 1) == "\n" ? ind : "") + "</" + name + ">";
					}
				} else {
					xml += ind + "<" + name + ">" + v.toString() + "</" + name + ">";
				}
				return xml;
			}, xml = "";
			for ( var m in o) {
				xml += toXml(o[m], m, "");
			}
			return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
		};
	}
	

	/*
	 * Mengubah String menjadi object XML
	 * 
	 * WL.XMLparse(string)
	 * 
	 */
	if (typeof WL.XMLparse !== 'function') {
		WL.XMLparse = function(xml) {
			var dom = null;
			if (window.DOMParser) {
				try {
					dom = (new DOMParser()).parseFromString(xml, "text/xml");
				} catch (e) {
					dom = null;
				}
			} else if (window.ActiveXObject) {
				try {
					dom = new ActiveXObject('Microsoft.XMLDOM');
					dom.async = false;
					if (!dom.loadXML(xml)) { // parse error ..
						// window.alert(dom.parseError.reason +
						// dom.parseError.srcText);
						dom = null;
					}
				} catch (e) {
					dom = null;
				}
			}
			return dom;
		};
	}
	

	/*
	 * Mengubah String menjadi object JSON
	 * 
	 * WL.JSONparse(text, reviver)
	 * 
	 */
	if (typeof WL.JSONparse !== 'function') {
		WL.JSONparse = function(text, reviver) {
			var j;
			function walk(holder, key) {
				var k, v, value = holder[key];
				if (value && typeof value === 'object') {
					for (k in value) {
						if (Object.prototype.hasOwnProperty.call(value, k)) {
							v = walk(value, k);
							if (v !== undefined) {
								value[k] = v;
							} else {
								delete value[k];
							}
						}
					}
				}
				return reviver.call(holder, key, value);
			}
			text = String(text);
			rx_dangerous.lastIndex = 0;
			if (rx_dangerous.test(text)) {
				text = text.replace(rx_dangerous,
						function(a) {
							return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
						});
			}
			if (rx_one.test(text.replace(rx_two, '@').replace(rx_three, ']').replace(rx_four, ''))) {
				j = eval('(' + text + ')');
				return typeof reviver === 'function' ? walk({'' : j}, '') : j;
			}
			throw new SyntaxError('WL.JSONparse');
		};
	}
	

	/*
	 * Mengubah Object JSON menjadi String
	 * 
	 * WL.JSONstringify(value, replacer, space)
	 * 
	 */
	if (typeof WL.JSONstringify !== 'function') {
		WL.JSONstringify = function(value, replacer, space) {
			var i, gap = '', indent = ' ', mind = '';
			if (typeof space === 'number') {
				for (i = 0; i < space; i += 1) {
					indent += ' ';
				}
			} else if (typeof space === 'string') {
				indent = space;
			}
			if (replacer && typeof replacer !== 'function' && (typeof replacer !== 'object' || typeof replacer.length !== 'number')) {
				throw new Error('WL.JSONstringify');
			}
			return str('', {'' : value}, gap, mind, indent, replacer);
		};
	}
    
	
	/*
	 * Membuat format Date ke dalam String
	 * 
	 * WL.formatDate(date, format)
	 * 
	 */
	 if (typeof WL.formatDate !== 'function') {
		 WL.formatDate = function(date,format){
	    	var ds={
			    MONTH_NAMES:new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'),
			    DAY_NAMES:new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat'),
			    LZ:function(x){return(x<0||x>9?"":"0")+x;}
			};
	    	var f=format||'EE, dd MMM yyyy  HH:mm:ss',dt=date,res='',i_f=0,c='',token='';
	    	var y=dt.getYear()+'',M=dt.getMonth()+1,d=dt.getDate(),E=dt.getDay(),H=dt.getHours(),m=dt.getMinutes(),s=dt.getSeconds();
	    	var v=new Object();
	    	if(y.length < 4){
	    		y=''+(y-0+1900);
	    	}
	    	v['y']=''+y;
	    	v['yyyy']=y;
	    	v['yy']=y.substring(2,4);
	    	v['M']=M;
	    	v['MM']=ds.LZ(M);
	    	v['MMM']=ds.MONTH_NAMES[M-1];
	    	v['NNN']=ds.MONTH_NAMES[M+11];
	    	v['d']=d;
	    	v['dd']=ds.LZ(d);
	    	v['E']=ds.DAY_NAMES[E+7];
	    	v['EE']=ds.DAY_NAMES[E];
	    	v['H']=H;
	    	v['HH']=ds.LZ(H);
	    	if(H==0){
	    		v['h']=12;
	    	}else if(H>12){
	    		v['h']=H-12;
	    	}else{
	    		v['h']=H;
	    	}
	    	v['hh']=ds.LZ(v['h']);
	    	if(H>11){
	    		v['K']=H-12;
	    	}else{
	    		v['K']=H;
	    	}
	    	v['k']=H+1;
	    	v['KK']=ds.LZ(v['K']);
	    	v['kk']=ds.LZ(v['k']);
	    	if(H > 11){
	    		v['a']='PM';
	    	}else{
	    		v['a']='AM';
	    	}
	    	v['m']=m;
	    	v['mm']=ds.LZ(m);
	    	v['s']=s;
	    	v['ss']=ds.LZ(s);
	    	while(i_f < f.length){
	    		c=f.charAt(i_f);
	    		token='';
	    		while((f.charAt(i_f)==c) &&(i_f < f.length)){
	    			token += f.charAt(i_f++);
	    		}
	    		if(v[token] != null){
	    			res=res + v[token];
	    		}else{
	    			res=res + token;
	    		}
	    	}
	    	return res;
	    };
	}
	
	
	 /*
	  * Membuat format Date millisecond (Epoch) ke dalam String
	  * 
	  * WL.formatEpoch(dataInMillis, format)
	  * 
	  */
	if (typeof WL.formatEpoch !== 'function') {
		WL.formatEpoch = function(e,f){
	    	f = f || 'EE, dd MMM yyyy HH:mm:ss';
	    	var m = parseInt(e);
	    	if(m < 10000000000){
	    		m *= 1000;
	    	}
	    	var d = new Date();
	    	d.setTime(m);
	    	return WL.formatDate(d,f);
	    };
	}
    
    
    /*
	 * Membuat nomor unik untuk panjang digit tertentu
	 * 
	 * WL.uniqueNumber(panjangDigit)
	 * 
	 */
	if (typeof WL.uniqueNumber !== 'function') {
		WL.uniqueNumber = function(len){
	    	var div = Math.floor(Math.pow(10, len));
			var number = (new Date()).getTime(); 
			number = Math.floor((number / len) % div);
			return WL.padLeft(number +'', len, '0');
	    };
	}
    	
	
	/*
	 * Membuat String rata kiri menggunakan karakter tertentu 
	 * 
	 * WL.padLeft(string, panjang, karakter)
	 * 
	 */
	if (typeof WL.padLeft !== 'function') {
		WL.padLeft = function(str, len, chr) {
	    	var s = str;
	    	if(s.length > len) {
	    		s = s.substring(s.length-len, s.length);
	    	} else {
	    		while (s.length < length) {
	    	        s = chr + s;
	    	    }
	    	}
	    	return s;
	    };
	}
    
	
	/*
	 * Membuat String rata kanan menggunakan karakter tertentu 
	 * 
	 * WL.padRight(string, panjang, karakter)
	 * 
	 */
	if (typeof WL.padRight !== 'function') {		
		WL.padRight = function(str, len, chr){
	    	var s = str;
	    	if(s.length > len) {
	    		s = s.substring(0, len);
	    	} else {
	    		while(s.length < length) {
	    	        s = s + chr;
	    	    }
	    	}
	    	return s;
	    };
	}
    
	
	/*
	 * Mengubah angka ke dalam format uang 
	 * 
	 * WL.numberToMoney(angka, jumlahdesimal, prefix) --> prefix bisa diisi dengan mata uang spt: Rp, $, dll
	 * 
	 */
	if (typeof WL.numberToMoney !== 'function') {
		WL.numberToMoney = function(value, dec, prefix){
	    	var v=value,p=prefix||'';
	    	v=WL.roundDecimal(v, dec)+'';
			if(isNaN(v)){return p+value;};
	    	var rgx = /(\d+)(\d{3})/ ,x,x1,x2;
	    	x=v.split('.');
	    	x1=x[0];
	    	x2=',';
	    	if(!x[1]){
	    	  x2+='00';
	    	}else{
	    	  x2+=x[1].length==1?x[1]+'0':x[1];
	    	}
	    	while(rgx.test(x1)){
	    	  x1=x1.replace(rgx,'$1'+'.'+'$2');
	    	}
	    	//return p+x1+x2;
	    	return p+x1;
	    };
	}
    
	/*
	 * Membuat angka desimal dengan jumlah tertentu 
	 * 
	 * WL.roundDecimal(angka, jumlahdesimal)
	 * 
	 */
	if (typeof WL.roundDecimal !== 'function') {
		WL.roundDecimal = function(value, dec) {
	    	var d = dec || 2, v = value;
	    	if(!v){ return v; }
	    	return Math.round(Math.round(v*Math.pow(10,d+1))/Math.pow(10,1))/Math.pow(10,d);        	
	    };
	}
    
	
	/*
	 * Mendapatkan Query string dari URL yang sedang aktif 
	 * 
	 * WL.urlQueryString()
	 * 
	 */
	if (typeof WL.urlQueryString !== 'function') {
	    WL.urlQueryString = function(){
	    	var s = window.location.search;
	    	if(s == ''){
	    		return s;
	    	}
	    	return s.substring(1);
	    };
	}
    
	/*
	 * Mendapatkan parameter dari URL yang sedang aktif 
	 * 
	 * WL.urlParameters()
	 * 
	 */
	if (typeof WL.urlParameters !== 'function') {
		WL.urlParameters = function() {
	    	var s = WL.urlQueryString(), p = {};
	    	if(s == ''){
	    		return p;
	    	}
	    	var n = s.split('&'),c;
	    	for (var i = 0; i < n.length; i++){
	    		c = n[i].split('=');
	    		if (c.length == 2){
	    			p[c[0]] = c[1];
	    		}
	    	}
	    	return p;
	    };
	}
	
	
	/*
	 * Eksekusi fungsi asinkronus 
	 * 
	 * WL.asyncExecutor(fungsi, delay)
	 * 
	 */
	var mapFxQueue = {};
	if (typeof WL.asyncExecutor !== 'function') {		
		WL.asyncExecutor = function(fx, wait) {
			if (typeof(fx) != 'function') {
				return;
			}
			var id = (new Date()).getTime() + '';
			var th = window.setInterval(function() {
				fx();
				var o = mapFxQueue[id];
				window.clearInterval(o);
				delete mapFxQueue[id];
			}, wait);
			mapFxQueue[id] = th;
	    };
	}
    
	/*
	 * Untuk terhubung dengan WebLauncher android 
	 * 
	 */
	var WL_SUCCESS_CALLBACK = {}, WL_ERROR_CALLBACK = {};
	if (typeof WL.mobile !== 'object') {
		WL.mobile = {};
	}
	if (typeof WL.mobile.request !== 'function') {
		WL.mobile.request = function(v) {
			v = v || {};
    		var bundle = v.bundle || PACKAGE_NAME;
    		if(bundle == '') {
    			throw new Error('WL.mobile.request -> bundle is required');
    		}
    		var prefix = bundle.replace(/\./g,'_');
			var wl = window[prefix];
			if(!(wl && typeof(wl.request) == 'function')){
				throw new Error('WL.mobile.request -> ' + 'window.' + prefix + '.request() is undefined');
			}    			
			var mode = v.mode ? v.mode.trim() : '';
			if(mode == '') {
				throw new Error('WL.mobile.request -> mode is required');
			}
			var id = (new Date).getTime() + '';
			if (typeof(v.success) === 'function') {
				WL_SUCCESS_CALLBACK[id] = v.success;
			}
			if (typeof(v.error) === 'function') {
				WL_ERROR_CALLBACK[id] = v.error;
			}
			var p = {};
			p['id'] = id;
			p['mode'] = mode;
			p['success'] = 'WL.mobile.success';
			p['error'] = 'WL.mobile.error';
			p['parameter'] = v.parameter;
			if (typeof(v.modal) !== 'undefined') {
				p['modal'] = v.modal;
			}
			wl.request(WL.JSONstringify(p));
		};
	}
	
	if (typeof WL.mobile.success !== 'function') {
		WL.mobile.success = function(v) {
			var f = WL_SUCCESS_CALLBACK[v.id];
			if(!f){
				return;
			}
			if(typeof(f) === 'function'){
				f(v);
			}
			delete WL_SUCCESS_CALLBACK[v.id];
		};
	}
	
	if (typeof WL.mobile.error !== 'function') {
		WL.mobile.error = function(v) {
			var f = WL_ERROR_CALLBACK[v.id];
			if(!f){
				return;
			}
			if(typeof(f) === 'function'){
				f(v);
			}
			delete WL_ERROR_CALLBACK[v.id];
		};
	}
    
}());