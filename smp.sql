/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50527
Source Host           : localhost:3306
Source Database       : smp

Target Server Type    : MYSQL
Target Server Version : 50527
File Encoding         : 65001

Date: 2017-02-03 10:55:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bank`
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `nama` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kode` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES ('1', '014', 'Bank BCA');
INSERT INTO `bank` VALUES ('2', '009', 'Bank BNI');
INSERT INTO `bank` VALUES ('3', '002', 'Bank BRI');
INSERT INTO `bank` VALUES ('4', '008', 'Bank Mandiri');
INSERT INTO `bank` VALUES ('5', '011', 'Bank Danamon');
INSERT INTO `bank` VALUES ('6', '013', 'Permata Bank');
INSERT INTO `bank` VALUES ('7', '016', 'Bank BII');
INSERT INTO `bank` VALUES ('8', '019', 'Bank Panin');
INSERT INTO `bank` VALUES ('9', '022', 'Bank Niaga');

-- ----------------------------
-- Table structure for `bank_`
-- ----------------------------
DROP TABLE IF EXISTS `bank_`;
CREATE TABLE `bank_` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(10) DEFAULT NULL,
  `nama` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bank_
-- ----------------------------
INSERT INTO `bank_` VALUES ('1', '014', 'Bank BCA');
INSERT INTO `bank_` VALUES ('2', '009', 'Bank BNI');
INSERT INTO `bank_` VALUES ('3', '002', 'Bank BRI');
INSERT INTO `bank_` VALUES ('4', '008', 'Bank Mandiri');
INSERT INTO `bank_` VALUES ('5', '003', 'Bank Ekspor Indonesia');
INSERT INTO `bank_` VALUES ('6', '011', 'Bank Danamon');
INSERT INTO `bank_` VALUES ('7', '013', 'Permata Bank');
INSERT INTO `bank_` VALUES ('8', '016', 'Bank BII');
INSERT INTO `bank_` VALUES ('9', '019', 'Bank Panin');
INSERT INTO `bank_` VALUES ('10', '020', 'Bank Arta Niaga Kencana');
INSERT INTO `bank_` VALUES ('11', '022', 'Bank Niaga');
INSERT INTO `bank_` VALUES ('12', '023', 'Bank Buana Ind');
INSERT INTO `bank_` VALUES ('13', '026', 'Bank Lippo');
INSERT INTO `bank_` VALUES ('14', '028', 'Bank Nisp');
INSERT INTO `bank_` VALUES ('15', '030', 'American Express Bank Ltd');
INSERT INTO `bank_` VALUES ('16', '031', 'Citibank N A');
INSERT INTO `bank_` VALUES ('17', '032', 'Jp Morgan Chase Bank');
INSERT INTO `bank_` VALUES ('18', '033', 'Bank Of America');
INSERT INTO `bank_` VALUES ('19', '034', 'Ing Indonesia Bank');
INSERT INTO `bank_` VALUES ('20', '036', 'Bank Multicor Tbk');
INSERT INTO `bank_` VALUES ('21', '037', 'Bank Artha Graha');
INSERT INTO `bank_` VALUES ('22', '039', 'Bank Credit Agricole Indosuez');
INSERT INTO `bank_` VALUES ('23', '040', 'The Bangkok Bank Comp Ltd');
INSERT INTO `bank_` VALUES ('24', '041', 'The Hongkong & Shanghai BC');
INSERT INTO `bank_` VALUES ('25', '042', 'The Bank Of Tokyo Mitsubishi Ufj Ltd');
INSERT INTO `bank_` VALUES ('26', '045', 'Bank Sumitomo Mitsui Indonesia');
INSERT INTO `bank_` VALUES ('27', '046', 'Bank Dbs Indonesia');
INSERT INTO `bank_` VALUES ('28', '047', 'Bank Resona Perdania');
INSERT INTO `bank_` VALUES ('29', '048', 'Bank Mizuho Indonesia');
INSERT INTO `bank_` VALUES ('30', '050', 'Standard Chartered Bank');
INSERT INTO `bank_` VALUES ('31', '052', 'Bank Abn Amro');
INSERT INTO `bank_` VALUES ('32', '053', 'Bank Keppel Tatlee Buana');
INSERT INTO `bank_` VALUES ('33', '054', 'Bank Capital Indonesia Tbk');
INSERT INTO `bank_` VALUES ('34', '057', 'Bank Bnp Paribas Indonesia');
INSERT INTO `bank_` VALUES ('35', '058', 'Bank Uob Indonesia');
INSERT INTO `bank_` VALUES ('36', '059', 'Korea Exchange Bank Danamon');
INSERT INTO `bank_` VALUES ('37', '060', 'Rabobank Internasional Indonesia');
INSERT INTO `bank_` VALUES ('38', '061', 'Anz Panin Bank');
INSERT INTO `bank_` VALUES ('39', '067', 'Deutsche Bank Ag');
INSERT INTO `bank_` VALUES ('40', '068', 'Bank Woori Indonesia');
INSERT INTO `bank_` VALUES ('41', '069', 'Bank Of China Limited');
INSERT INTO `bank_` VALUES ('42', '076', 'Bank Bumi Arta');
INSERT INTO `bank_` VALUES ('43', '087', 'Bank Ekonomi');
INSERT INTO `bank_` VALUES ('44', '088', 'Bank Antardaerah');
INSERT INTO `bank_` VALUES ('45', '089', 'Bank Haga');
INSERT INTO `bank_` VALUES ('46', '093', 'Bank Ifi');
INSERT INTO `bank_` VALUES ('47', '095', 'Bank Century Tbk');
INSERT INTO `bank_` VALUES ('48', '097', 'Bank Mayapada');
INSERT INTO `bank_` VALUES ('49', '110', 'Bank Jabar');
INSERT INTO `bank_` VALUES ('50', '111', 'Bank Dki');
INSERT INTO `bank_` VALUES ('51', '112', 'Bpd Diy');
INSERT INTO `bank_` VALUES ('52', '113', 'Bank Jateng');
INSERT INTO `bank_` VALUES ('53', '114', 'Bank Jatim');
INSERT INTO `bank_` VALUES ('54', '115', 'Bpd Jambi');
INSERT INTO `bank_` VALUES ('55', '116', 'Bpd Aceh');
INSERT INTO `bank_` VALUES ('56', '117', 'Bank Sumut');
INSERT INTO `bank_` VALUES ('57', '118', 'Bank Nagari');
INSERT INTO `bank_` VALUES ('58', '119', 'Bank Riau');
INSERT INTO `bank_` VALUES ('59', '120', 'Bank Sumsel');
INSERT INTO `bank_` VALUES ('60', '121', 'Bank Lampung');
INSERT INTO `bank_` VALUES ('61', '122', 'Bpd Kalsel');
INSERT INTO `bank_` VALUES ('62', '123', 'Bpd Kalimantan Barat');
INSERT INTO `bank_` VALUES ('63', '124', 'Bpd Kaltim');
INSERT INTO `bank_` VALUES ('64', '125', 'Bpd Kalteng');
INSERT INTO `bank_` VALUES ('65', '126', 'Bpd Sulsel');
INSERT INTO `bank_` VALUES ('66', '127', 'Bank Sulut');
INSERT INTO `bank_` VALUES ('67', '128', 'Bpd Ntb');
INSERT INTO `bank_` VALUES ('68', '129', 'Bpd Bali');
INSERT INTO `bank_` VALUES ('69', '130', 'Bank Ntt');
INSERT INTO `bank_` VALUES ('70', '131', 'Bank Maluku');
INSERT INTO `bank_` VALUES ('71', '132', 'Bpd Papua');
INSERT INTO `bank_` VALUES ('72', '133', 'Bank Bengkulu');
INSERT INTO `bank_` VALUES ('73', '134', 'Bpd Sulawesi Tengah');
INSERT INTO `bank_` VALUES ('74', '135', 'Bank Sultra');
INSERT INTO `bank_` VALUES ('75', '145', 'Bank Nusantara Parahyangan');
INSERT INTO `bank_` VALUES ('76', '146', 'Bank Swadesi');
INSERT INTO `bank_` VALUES ('77', '147', 'Bank Muamalat');
INSERT INTO `bank_` VALUES ('78', '151', 'Bank Mestika');
INSERT INTO `bank_` VALUES ('79', '152', 'Bank Metro Express');
INSERT INTO `bank_` VALUES ('80', '153', 'Bank Shinta Indonesia');
INSERT INTO `bank_` VALUES ('81', '157', 'Bank Maspion');
INSERT INTO `bank_` VALUES ('82', '159', 'Bank Hagakita');
INSERT INTO `bank_` VALUES ('83', '161', 'Bank Ganesha');
INSERT INTO `bank_` VALUES ('84', '162', 'Bank Windu Kentjana');
INSERT INTO `bank_` VALUES ('85', '164', 'Halim Indonesia Bank');
INSERT INTO `bank_` VALUES ('86', '166', 'Bank Harmoni International');
INSERT INTO `bank_` VALUES ('87', '167', 'Bank Kesawan');
INSERT INTO `bank_` VALUES ('88', '200', 'Bank Tabungan Negara (Persero)');
INSERT INTO `bank_` VALUES ('89', '212', 'Bank Himpunan Saudara 1906 Tbk');
INSERT INTO `bank_` VALUES ('90', '213', 'Bank Tabungan Pensiunan Nasional');
INSERT INTO `bank_` VALUES ('91', '405', 'Bank Swaguna');
INSERT INTO `bank_` VALUES ('92', '422', 'Bank Jasa Arta');
INSERT INTO `bank_` VALUES ('93', '426', 'Bank Mega');
INSERT INTO `bank_` VALUES ('94', '427', 'Bank Jasa Jakarta');
INSERT INTO `bank_` VALUES ('95', '441', 'Bank Bukopin');
INSERT INTO `bank_` VALUES ('96', '451', 'Bank Syariah Mandiri');
INSERT INTO `bank_` VALUES ('97', '459', 'Bank Bisnis Internasional');
INSERT INTO `bank_` VALUES ('98', '466', 'Bank Sri Partha');
INSERT INTO `bank_` VALUES ('99', '472', 'Bank Jasa Jakarta');
INSERT INTO `bank_` VALUES ('100', '484', 'Bank Bintang Manunggal');
INSERT INTO `bank_` VALUES ('101', '485', 'Bank Bumiputera');
INSERT INTO `bank_` VALUES ('102', '490', 'Bank Yudha Bhakti');
INSERT INTO `bank_` VALUES ('103', '491', 'Bank Mitraniaga');
INSERT INTO `bank_` VALUES ('104', '494', 'Bank Agro Niaga');
INSERT INTO `bank_` VALUES ('105', '498', 'Bank Indomonex');
INSERT INTO `bank_` VALUES ('106', '501', 'Bank Royal Indonesia');
INSERT INTO `bank_` VALUES ('107', '503', 'Bank Alfindo');
INSERT INTO `bank_` VALUES ('108', '506', 'Bank Syariah Mega');
INSERT INTO `bank_` VALUES ('109', '513', 'Bank Ina Perdana');
INSERT INTO `bank_` VALUES ('110', '517', 'Bank Harfa');
INSERT INTO `bank_` VALUES ('111', '520', 'Prima Master Bank');
INSERT INTO `bank_` VALUES ('112', '521', 'Bank Persyarikatan Indonesia');
INSERT INTO `bank_` VALUES ('113', '525', 'Bank Akita');
INSERT INTO `bank_` VALUES ('114', '526', 'Liman International Bank');
INSERT INTO `bank_` VALUES ('115', '531', 'Anglomas Internasional Bank');
INSERT INTO `bank_` VALUES ('116', '523', 'Bank Dipo International');
INSERT INTO `bank_` VALUES ('117', '535', 'Bank Kesejahteraan Ekonomi');
INSERT INTO `bank_` VALUES ('118', '536', 'Bank Uib');
INSERT INTO `bank_` VALUES ('119', '542', 'Bank Artos Ind');
INSERT INTO `bank_` VALUES ('120', '547', 'Bank Purba Danarta');
INSERT INTO `bank_` VALUES ('121', '548', 'Bank Multi Arta Sentosa');
INSERT INTO `bank_` VALUES ('122', '553', 'Bank Mayora');
INSERT INTO `bank_` VALUES ('123', '555', 'Bank Index Selindo');
INSERT INTO `bank_` VALUES ('124', '566', 'Bank Victoria International');
INSERT INTO `bank_` VALUES ('125', '558', 'Bank Eksekutif');
INSERT INTO `bank_` VALUES ('126', '559', 'Centratama Nasional Bank');
INSERT INTO `bank_` VALUES ('127', '562', 'Bank Fama Internasional');
INSERT INTO `bank_` VALUES ('128', '564', 'Bank Sinar Harapan Bali');
INSERT INTO `bank_` VALUES ('129', '567', 'Bank Harda');
INSERT INTO `bank_` VALUES ('130', '945', 'Bank Finconesia');
INSERT INTO `bank_` VALUES ('131', '946', 'Bank Merincorp');
INSERT INTO `bank_` VALUES ('132', '947', 'Bank Maybank Indocorp');
INSERT INTO `bank_` VALUES ('133', '948', 'Bank Ocbc Indonesia');
INSERT INTO `bank_` VALUES ('134', '949', 'Bank China Trust Indonesia');
INSERT INTO `bank_` VALUES ('135', '950', 'Bank Commonwealth');

-- ----------------------------
-- Table structure for `inquiry`
-- ----------------------------
DROP TABLE IF EXISTS `inquiry`;
CREATE TABLE `inquiry` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trxid` varchar(250) NOT NULL,
  `requestid` varchar(250) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `app_code` varchar(200) DEFAULT NULL,
  `id_merchant` varchar(250) DEFAULT NULL,
  `msisdn` varchar(14) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=152 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of inquiry
-- ----------------------------
INSERT INTO `inquiry` VALUES ('1', 'EVGL20161215162004LGVE932617', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-15 16:20:04', null);
INSERT INTO `inquiry` VALUES ('2', 'VONU20161215162633UNOV18493', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-15 16:26:33', null);
INSERT INTO `inquiry` VALUES ('3', 'AJST20161215162726TSJA701782', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-15 16:27:26', null);
INSERT INTO `inquiry` VALUES ('4', 'CMGD20161215165647DGMC43091', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-15 16:56:47', null);
INSERT INTO `inquiry` VALUES ('5', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-16 09:05:21', null);
INSERT INTO `inquiry` VALUES ('6', 'ZOTF20161216094337FTOZ699001', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', '2016-12-16 09:43:37', null);
INSERT INTO `inquiry` VALUES ('7', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', '2016-12-16 13:45:12', null);
INSERT INTO `inquiry` VALUES ('8', 'CPOD20161216151830DOPC773487', '', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', '2016-12-16 15:18:30', null);
INSERT INTO `inquiry` VALUES ('9', 'WGPT20161216153310TPGW744572', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', '2016-12-16 15:33:10', null);
INSERT INTO `inquiry` VALUES ('10', 'IJZY20161216155305YZJI589186', 'AKKJDJS8888', '1500', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 15:53:05', null);
INSERT INTO `inquiry` VALUES ('11', 'ZJUQ20161216155525QUJZ92265', 'AKKJDJS8888', '100', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 15:55:25', null);
INSERT INTO `inquiry` VALUES ('12', 'WOJQ20161216160307QJOW144632', 'AKKJDJS8888', '100', 'AREMA', 'MCTS', '08577544678', 'Anwar', '2016-12-16 16:03:07', null);
INSERT INTO `inquiry` VALUES ('13', 'FRBZ20161216161354ZBRF542995', 'AKKJDJS8888', '100', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:13:54', null);
INSERT INTO `inquiry` VALUES ('14', 'ZADP20161216161527PDAZ949894', 'AKKJDJS8888', '500', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:15:27', null);
INSERT INTO `inquiry` VALUES ('15', 'ZIOD20161216161753DOIZ795537', 'AKKJDJS8888', '600', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:17:53', null);
INSERT INTO `inquiry` VALUES ('16', 'FEAH20161216162025HAEF406224', 'AKKJDJS8888', '1400', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:20:25', null);
INSERT INTO `inquiry` VALUES ('17', 'dc852bc1aa3d57940dcfa3065b7ef536', 'AKKJDJS8888', '1400', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:22:09', null);
INSERT INTO `inquiry` VALUES ('18', '5419b55714c7d25d5759aac2e4ac6b1c', 'AKKJDJS8888', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-16 16:32:40', null);
INSERT INTO `inquiry` VALUES ('19', '5f9b16dd1d291b14580f38112e6ff351', '47cph9aim5q8', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:01:03', null);
INSERT INTO `inquiry` VALUES ('20', '3c48122414d9cc8383a5cf0bc0a4c3c6', '47cpifh0rps0', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:02:34', null);
INSERT INTO `inquiry` VALUES ('21', '7f6510f6437dd2e9119fe420ba954b11', '47cpin2n8ha8', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:02:51', null);
INSERT INTO `inquiry` VALUES ('22', 'a1723adfe29601aa4333138dbfcf1d9c', '47cpiwje236s', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:03:11', null);
INSERT INTO `inquiry` VALUES ('23', 'c89821c4a0c81912360d690fdfe0df44', '47cpomw9ywow', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:10:40', null);
INSERT INTO `inquiry` VALUES ('24', 'b7270d1e39aba82811a9827b1a9c68a6', '47cqe5h9qkis', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:44:00', null);
INSERT INTO `inquiry` VALUES ('25', '7a7833a5c6a4fea647546baeaa8623c8', '47cqfh60k944', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:45:44', null);
INSERT INTO `inquiry` VALUES ('26', 'b7ffcc5fb4ba4bfe25f1c57316354b07', '47cqg2e947qc', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:46:30', null);
INSERT INTO `inquiry` VALUES ('27', '661952b0ee7d114f507c3aa4dc4c5b7f', '47cqiubld56o', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 16:50:07', null);
INSERT INTO `inquiry` VALUES ('28', '5de327f3bb74898f379e794fc6c41d47', '47cqvsg0u7k0', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-19 17:07:01', null);
INSERT INTO `inquiry` VALUES ('29', '2b0dc3b0081a291b8457c9aa1e76df63', '47dba1q81z40', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 09:05:56', null);
INSERT INTO `inquiry` VALUES ('30', '8e9f1955c98e7347f2f8f9cc57ea3f30', 'asii9990jja829', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-20 09:32:12', null);
INSERT INTO `inquiry` VALUES ('31', 'bcf8ce2715d05f408f71fd093f46709a', '47dbv2xmgl0k', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 09:33:24', null);
INSERT INTO `inquiry` VALUES ('32', '50fb60934bc93fd9d8f4e854c7b8fa37', '47dd0m0tz0cg', '10000', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 10:27:39', null);
INSERT INTO `inquiry` VALUES ('33', '02ee5379718cb4e2040867f9313d0660', '47dd3uj7lzuo', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 10:31:53', null);
INSERT INTO `inquiry` VALUES ('34', '58dbdc620e549324f4751fe38ef9afdf', '47dd4urcbnuo', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 10:33:11', null);
INSERT INTO `inquiry` VALUES ('35', 'fbf970776913d2dce66112222d3a79b7', '47dd6o0mujs4', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 10:35:33', null);
INSERT INTO `inquiry` VALUES ('36', '43df38e0a359b66abba7ec3b9e22ec41', '47dd6rjeqhog', '10000', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 10:35:41', null);
INSERT INTO `inquiry` VALUES ('37', '9371e3a8f794f62cf78808a8e088f89f', '47dd737k44w0', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 10:36:06', null);
INSERT INTO `inquiry` VALUES ('38', '17d14c43f75721771089eb74deb01a51', 'asii9990jja829', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-20 10:42:27', null);
INSERT INTO `inquiry` VALUES ('39', 'd858e6a3a112c947f4b6dab016600867', '47ddhjk70juo', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 10:49:46', null);
INSERT INTO `inquiry` VALUES ('40', '56cf9fe2b4775fcddbe46b8d720f6932', 'asii9990jja829', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-20 10:50:35', null);
INSERT INTO `inquiry` VALUES ('41', '3758b31b80bf308c931062905f995ddb', '47ddpge34hes', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:00:09', null);
INSERT INTO `inquiry` VALUES ('42', '41496694eb4bb0621a9f958eefe09777', '47ddt2jxbrok', '10000', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 11:04:49', null);
INSERT INTO `inquiry` VALUES ('43', 'afa13d9ce5c463098ab8532190b72fdf', '47de4hvp1zuo', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:19:44', null);
INSERT INTO `inquiry` VALUES ('44', '5dde21a777d12298dad52189739d2cf1', '47de5zutr3y8', '2000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:21:42', null);
INSERT INTO `inquiry` VALUES ('45', '80e1f79acbf533b497e2844419d4c58f', '47deih867p2c', '2500', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:38:00', null);
INSERT INTO `inquiry` VALUES ('46', 'a4a2b840260797537f7f244718f4676c', '47dejhrn9000', '2500', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:39:19', null);
INSERT INTO `inquiry` VALUES ('47', '9d29779f4d7caa46e6115d5b5cdbc380', '47dejjxjuwqo', '3000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 11:39:24', null);
INSERT INTO `inquiry` VALUES ('48', '2cea6944ba3fb7b6cd30a7f90bc8b3d1', '47df7beqs328', '10', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 12:10:26', null);
INSERT INTO `inquiry` VALUES ('49', '9c7ee1eff4051d1a6c1716448ed65b3a', '47df8dju8les', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 12:11:49', null);
INSERT INTO `inquiry` VALUES ('50', '70569df8430655201586f7215eeedb70', '47dg34f068sg', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 12:51:59', null);
INSERT INTO `inquiry` VALUES ('51', '9f091b7946068cc8f48f426bfdade600', '47dg4ldb09wk', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 12:53:53', null);
INSERT INTO `inquiry` VALUES ('52', '76dfab16ac96831a7d65b86c78aeb93b', '47dg4xlains4', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 12:54:21', null);
INSERT INTO `inquiry` VALUES ('53', 'd4f8a620b6504e89e5b988b0317e8783', '47dgbmtdowys', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 13:03:06', null);
INSERT INTO `inquiry` VALUES ('54', 'c0f4b5dc2263c3a8c4030344c7291f87', '47dgdzpr4fuo', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 13:06:11', null);
INSERT INTO `inquiry` VALUES ('55', 'edd3599a09471cb04a87d71295a24630', '47dge9pybrac', '1', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 13:06:32', null);
INSERT INTO `inquiry` VALUES ('56', '4b45933cc90fb2f61ba9087c171698e2', '47dj0bcpn0qo', '1', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:09:22', null);
INSERT INTO `inquiry` VALUES ('57', 'db0e8a5d77018b74647927843d30a60e', '47dj2oop6eck', '25000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:12:28', null);
INSERT INTO `inquiry` VALUES ('58', '3cfbcc3d4531df75458571919ef9775e', '47djdr2lxj0g', '1500', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:26:54', null);
INSERT INTO `inquiry` VALUES ('59', '330fe22c98595602668bb7843593e0bd', '47djef2uum2o', '1400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:27:47', null);
INSERT INTO `inquiry` VALUES ('60', '1b2b066dfa7e3bd20c9b0993c17d0948', '47djgsx2n7sw', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:30:54', null);
INSERT INTO `inquiry` VALUES ('61', '606d76909d78a5f4847ef52410866780', '47djrz9gs4cg', '2300', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-20 15:45:31', null);
INSERT INTO `inquiry` VALUES ('62', 'c51642cee4a2c96e28ced2597c77eda9', '47djtc5c00is', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:47:16', null);
INSERT INTO `inquiry` VALUES ('63', '256c7642f5ca526cab5a584f22ba3ad4', '47djtv15guw4', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:47:58', null);
INSERT INTO `inquiry` VALUES ('64', '13e8005e8242a2fc197cc3767698619f', '47djvgdcfvi8', '2400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-20 15:50:02', null);
INSERT INTO `inquiry` VALUES ('65', 'c9d4f2c0f97fc4d9b3c556fdec59a75b', 'asii9990jja829', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-20 17:13:47', null);
INSERT INTO `inquiry` VALUES ('66', '27d2fc44e9f0f334c6388bca5fe9b3d3', 'asii9990jja829', '300', 'AREMA', 'ABCD123', '08577544678', 'Anwar', '2016-12-20 17:16:11', null);
INSERT INTO `inquiry` VALUES ('67', '9770e594d209c9f3c0d2a134017355f1', '47dn5840ekkk', '12000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:23:51', null);
INSERT INTO `inquiry` VALUES ('68', 'aba73e23cc9e5e8a70b771b7c733cfc9', '47dn5kemu4mc', '12000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:24:17', null);
INSERT INTO `inquiry` VALUES ('69', 'd17d03857a565d861d5739869e49b29e', '47dn5p2m3ku8', '5000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:24:27', null);
INSERT INTO `inquiry` VALUES ('70', 'a0603e0c7aa7939a50879b0cd128a8a8', '47dn6n2l4wsg', '1000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:25:41', null);
INSERT INTO `inquiry` VALUES ('71', 'c4d691777a7e772a649fea356f051148', '47dn6v381hgk', '1000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:26:00', null);
INSERT INTO `inquiry` VALUES ('72', '5b4fcdccd053523774f6441bff5d214f', '47dn71shd6ww', '1000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:26:14', null);
INSERT INTO `inquiry` VALUES ('73', 'ef64dd7764df4c27359471ce1d6ea081', '47dn86l8m4qo', '2156', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:27:42', null);
INSERT INTO `inquiry` VALUES ('74', '3ed338b6aace0934bf4dcadd1f1a3ccb', '47dnczy4gcu8', '1257', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:34:01', null);
INSERT INTO `inquiry` VALUES ('75', '75307f5ee6e57948e4d1f4e6fc747abd', '47dnh7qdgpk4', '120', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:39:31', null);
INSERT INTO `inquiry` VALUES ('76', '7b9af6bd7f51fa758caf8a8ee8abdb46', '47dnhpzxqxq8', '120', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:40:11', null);
INSERT INTO `inquiry` VALUES ('77', '170b967e5bb9de0e5bcf90d850f2f4fb', '47dni2an8xc0', '120', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-20 18:40:37', null);
INSERT INTO `inquiry` VALUES ('78', '7a02bdf4e5493421a07486874ab71cd3', '47e6fxmi1y68', '1470', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', '2016-12-21 09:31:12', null);
INSERT INTO `inquiry` VALUES ('79', '16ad1071b7eff17d570c8a0ecd00a14d', 'asii9990jja829', '300', 'AREMA', 'ABCD123', '08577544678', 'Syabil', '2016-12-21 09:50:05', null);
INSERT INTO `inquiry` VALUES ('80', 'cf1122898e51c48ac84c1539036d8402', 'asii9990jja829', '300', 'AREMA', 'TM100', '08577544678', 'Syabil', '2016-12-21 09:51:40', null);
INSERT INTO `inquiry` VALUES ('81', '8ef42f7d06973b955e5613417e2ca015', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'Syabil Anwar', '2016-12-21 09:55:55', null);
INSERT INTO `inquiry` VALUES ('82', 'd87092d18bbb71e934921fe6861f7cb3', '47e75lqbx2yo', '1400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-21 10:04:44', null);
INSERT INTO `inquiry` VALUES ('83', '63203c0007fec5634819dfb19897ce9a', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'udin', '2016-12-21 10:04:57', null);
INSERT INTO `inquiry` VALUES ('84', '86b3a7ec7935d6ef89a51547d0b559ff', '47e76nfrfm68', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-21 10:06:15', '1');
INSERT INTO `inquiry` VALUES ('85', '3804ed0afafc5f45a5a6c33d5fc7c414', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'udin', '2016-12-21 10:06:27', '1');
INSERT INTO `inquiry` VALUES ('86', 'cb0d3f76bc1b455ee2eb57f9ff81246d', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'udin', '2016-12-21 10:16:56', '1');
INSERT INTO `inquiry` VALUES ('87', '051d2684e390ca2398c5b846b7caa52f', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'udin', '2016-12-21 10:28:41', '1');
INSERT INTO `inquiry` VALUES ('88', '30bafee7e40bdd48e9f3914c53533e15', '323sdd33dsf', '300', 'AREMA', 'TM100', '08577544678', 'udin', '2016-12-21 10:29:13', '0');
INSERT INTO `inquiry` VALUES ('89', 'bcdbeb7fbcee4c409ff2cc6c89bf8e68', '47e7xp2locao', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 10:41:33', '1');
INSERT INTO `inquiry` VALUES ('90', '3a43099dcc2f7f9376092fca47028bb2', '47e847qao0ow', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 10:49:56', '0');
INSERT INTO `inquiry` VALUES ('91', '754ecdd628e98ce7ceca6e7cf50dbf82', '47e84yct164g', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 10:50:53', '0');
INSERT INTO `inquiry` VALUES ('92', '2567d573ac16e4aacfe806f089d42717', '47e85dfilquc', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 10:51:25', '0');
INSERT INTO `inquiry` VALUES ('93', 'e3ae941d218ac0c485c2cd4014f63ba8', '47e865rr594w', '15000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 10:52:37', '1');
INSERT INTO `inquiry` VALUES ('94', 'f7dc9789c37cc488818d262607b0d359', '47e8dj80ok00', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:02:06', '0');
INSERT INTO `inquiry` VALUES ('95', '426cfd71d96288a133abffcb8c51838b', '47e8dx7ktk2s', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:02:35', '0');
INSERT INTO `inquiry` VALUES ('96', 'bef39b4e834e5dc0dd2270948144cf2b', '47e8f4079hwk', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:04:09', '0');
INSERT INTO `inquiry` VALUES ('97', '7c10dc1290df356620e4c247d487c7b4', '47e8np9woh0k', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:15:30', '1');
INSERT INTO `inquiry` VALUES ('98', '97b34cec652a8f07b1dba8b27145c576', '47e8og45ds6c', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:16:21', '0');
INSERT INTO `inquiry` VALUES ('99', '2761259de84dbf3fbab9761d2bd76560', '47e8oph5c82s', '2400', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:16:46', '1');
INSERT INTO `inquiry` VALUES ('100', '23c46ba400c9be9ad9e90f0474a0c3ad', '47e8ruc3ws6c', '2400', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:20:46', '0');
INSERT INTO `inquiry` VALUES ('101', '48b7c9dd8f2b12ff73ce6fabec3ee61d', '47e8s132c328', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:21:02', '0');
INSERT INTO `inquiry` VALUES ('102', 'ce7906fda0e77edd7cd9ae3736a2adcd', '47e8s858x90k', '2500', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:21:23', '1');
INSERT INTO `inquiry` VALUES ('103', 'd067a5c903cfadffa95c4437aad71c55', '47e8x8kl4dc0', '900', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:27:57', '1');
INSERT INTO `inquiry` VALUES ('104', '02f613fc4d2c8d3ea7a8e50efc188af3', '47e8xsyhz6ck', '5000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:28:39', '1');
INSERT INTO `inquiry` VALUES ('105', '73e7434be6cb28697ddeeaa5c47dd853', '47e92b07jt2c', '1000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 11:34:34', '1');
INSERT INTO `inquiry` VALUES ('106', 'c7a3ab17b8b7896536b567f53a1226fd', '323sdd33dsf', '600', 'AREMA', 'TM100', '08577544678', 'shinta', '2016-12-21 11:50:07', '1');
INSERT INTO `inquiry` VALUES ('107', '568925deec65addaca0f4bb5b83003bb', '47e9plbzjles', '60713', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-21 12:05:05', '1');
INSERT INTO `inquiry` VALUES ('108', '9f6fcb2e53954a04e5af110e51c4309f', '47e9ysflz0w0', '1000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 12:16:53', '0');
INSERT INTO `inquiry` VALUES ('109', 'd2683b0cc8c08e22a3ecfa33a2647c3f', '47e9z1rlbmec', '1500', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', '2016-12-21 12:17:18', '1');
INSERT INTO `inquiry` VALUES ('110', '406b66710efcffcfa9cbf9ad2967ed73', '47ecpvpjkyw4', '40000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-21 14:26:18', '0');
INSERT INTO `inquiry` VALUES ('111', '2731b8d0b4c03a8f883355880f868aa0', '47ecqk8gxdic', '40000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-21 14:27:10', '0');
INSERT INTO `inquiry` VALUES ('112', '154db2bd8a13006234901e3e70ff8a02', '47eekqhk1i2o', '501', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 15:53:46', '1');
INSERT INTO `inquiry` VALUES ('113', 'fbb1a8620c418e2a202517fadc7ca076', '47ef09eu0kis', '110', 'AREMA', 'TM100', '6285777544678', 'anwar', '2016-12-21 16:13:53', '0');
INSERT INTO `inquiry` VALUES ('114', '342587674d36760c81ddb53b74ac608e', '47ef1u6mggow', '110', 'AREMA', 'TM100', '6285777544678', 'anwar', '2016-12-21 16:15:58', '0');
INSERT INTO `inquiry` VALUES ('115', 'e49dfa74742cb277f0021f2a0d834053', '47ef28ns1aw4', '125', 'AREMA', 'TM100', '6285777544678', 'anwar', '2016-12-21 16:16:36', '1');
INSERT INTO `inquiry` VALUES ('116', '3d48c6c9461d2de05d442eef1fcb082a', '47ef7dmvwxc0', '212', 'AREMA', 'TM100', '6285777544678', 'anwar', '2016-12-21 16:23:17', '1');
INSERT INTO `inquiry` VALUES ('117', 'bc41317e218085f083b9f2b3c3a9dc9f', '47efrmk9j48w', '321', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-21 16:49:46', '1');
INSERT INTO `inquiry` VALUES ('118', '78953bb1a74a66e70bb799fc69f8cf14', '47efvmnvdlk4', '1459', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 16:55:00', '1');
INSERT INTO `inquiry` VALUES ('119', '78df90b54bbc6daf2ad9cea89688bf48', '47eg1ho5avk0', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-21 17:02:31', '0');
INSERT INTO `inquiry` VALUES ('120', '0bce41bc90880f0266abdf0504947cac', '47eg3ny0b7ac', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-21 17:05:20', '0');
INSERT INTO `inquiry` VALUES ('121', 'dfe2ef8cc50e42ad9525a699eaecba15', '47egca75a76s', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-21 17:16:37', '0');
INSERT INTO `inquiry` VALUES ('122', '0160bb99a8d45b72b7553c8aa9618f58', '47egd1b3v98g', '1000', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:17:36', '0');
INSERT INTO `inquiry` VALUES ('123', '063740ba6b557baeda437501a317443d', '47egdjhzzfcw', '1120', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:18:14', '0');
INSERT INTO `inquiry` VALUES ('124', 'e15a7f7e91578f6812892c239fc6ebbd', '47egdvkn3uw4', '1112', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:18:42', '0');
INSERT INTO `inquiry` VALUES ('125', 'b841e90cacbaac2f26f685977376a3ef', '47ege28ss6kg', '11', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:18:55', '0');
INSERT INTO `inquiry` VALUES ('126', '96a9b2408184baa92c5bddeeb3d8bd42', '47ege6mhu7eo', '1230', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:19:06', '0');
INSERT INTO `inquiry` VALUES ('127', '9d757a1277bcef15a53271a05d36f995', '47egfd7lzt0k', '2714', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:20:37', '0');
INSERT INTO `inquiry` VALUES ('128', '321bc6e7011f255b1d2fb5667d6fb5f2', '47egflol99og', '4200', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:20:57', '0');
INSERT INTO `inquiry` VALUES ('129', 'ca0f88b68e78606be9a7d11eedca94a8', '47eggfwgdv40', '5000', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:22:03', '0');
INSERT INTO `inquiry` VALUES ('130', '056beb4ab7dabe99e44d378e1a970579', '47eggneonwcg', '3037', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:22:19', '0');
INSERT INTO `inquiry` VALUES ('131', '8adcb5a2e5357a08b991c4f34cc90769', '47eggwsmivuo', '1500', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:22:38', '0');
INSERT INTO `inquiry` VALUES ('132', 'bafe336a2e054dc5f142ec66fb79ecc1', '47egh2nfid2c', '125', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:22:52', '0');
INSERT INTO `inquiry` VALUES ('133', '91dcd0fd4a1ea9966e18b7eeb396ba00', '47eghtkwsa0w', '3789', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:23:51', '0');
INSERT INTO `inquiry` VALUES ('134', '8e2f4f5b37de59fba583c5b6a5fe19b6', '47egi4ced3ac', '6527', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 17:24:23', '1');
INSERT INTO `inquiry` VALUES ('135', 'beb60bf06159a07bd20fd22cee9e6544', '47ehrabwhv28', '2500', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-21 18:23:14', '0');
INSERT INTO `inquiry` VALUES ('136', '3861807f331257c268f5a4b7033e52d0', '47f0p5q85qio', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-22 09:13:49', '0');
INSERT INTO `inquiry` VALUES ('137', 'ba54409bf83e2734ab3af31cbd58f488', '47f0pcj2hoao', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-22 09:14:22', '1');
INSERT INTO `inquiry` VALUES ('138', 'c7e3efd9a29aa32eed284b54cb846bb1', '47f1an2h8vms', '111', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-22 09:41:58', '1');
INSERT INTO `inquiry` VALUES ('139', 'a97364d53028a47befd23f253086d764', '47f1bldkkcw0', '111', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-22 09:43:06', '0');
INSERT INTO `inquiry` VALUES ('140', 'a41cedb468f12db89e015d321170406a', '47f3kgu4bf40', '2500', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', '2016-12-22 11:29:22', '1');
INSERT INTO `inquiry` VALUES ('141', '90d748216afa4f0d4c5dcf1fed87896e', '47f3llsd8uw4', '1500', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', '2016-12-22 11:30:23', '1');
INSERT INTO `inquiry` VALUES ('142', '3316991716bcef4813bdfd9519f1f270', '47f7a6syf5wk', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-22 14:23:30', '1');
INSERT INTO `inquiry` VALUES ('143', '6e5c1fed5649715ccee44c53ad31a109', '47f7avfx2bk0', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-22 14:24:25', '1');
INSERT INTO `inquiry` VALUES ('144', 'b9385779f0d9c3ff3d97c87072c15930', '47f8ie7399wk', '1450', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', '2016-12-22 15:21:16', '1');
INSERT INTO `inquiry` VALUES ('145', 'a95e6060e746a12b64c3496593e177a7', '47f8lp64df8k', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-22 15:25:47', '1');
INSERT INTO `inquiry` VALUES ('146', '2629e89f6d183050c1d8db1bd9a3a60d', '47f8pqo7dxk4', '1300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', '2016-12-22 15:31:00', '1');
INSERT INTO `inquiry` VALUES ('147', '1f4ae5657f93980c30775a4a352f9f3c', '47fb0w3ui3k0', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', '2016-12-22 17:19:27', '1');
INSERT INTO `inquiry` VALUES ('148', 'aaa49f0f730c73ad26c55b05bd46683b', '47fb900pe668', '3200', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-22 17:29:54', '0');
INSERT INTO `inquiry` VALUES ('149', '6d138af8e0f4090899f03454d54e3039', '47fcd5xvhuec', '12000', 'AREMA', 'ABCD123', '6281222900985', 'M Agung Satrio', '2016-12-22 18:22:21', '0');
INSERT INTO `inquiry` VALUES ('150', '6b937291cbfe022470193bf47510b450', '47fvxgahybuo', '2200', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', '2016-12-23 09:43:17', '1');
INSERT INTO `inquiry` VALUES ('151', '70a8b704957ecffdf5c8760257e70b26', '323sdd33dsf', '600', 'AREMA', 'SAM001', '08577544678', 'Syam', '2017-01-03 14:00:52', '0');

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `tipe` varchar(1) DEFAULT NULL,
  `header` int(11) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('1', 'Home', 'S', '0', 'home', 'fa fa-home', '1');
INSERT INTO `menu` VALUES ('2', 'User Management', 'H', '0', '', 'fa fa-cog', '2');
INSERT INTO `menu` VALUES ('3', 'Role', 'S', '2', 'user_management/role', 'fa fa-lock', '3');
INSERT INTO `menu` VALUES ('4', 'User', 'S', '2', 'user_management/user', 'fa fa-user', '4');
INSERT INTO `menu` VALUES ('5', 'Data Merchant', 'H', '0', '', 'fa fa-desktop', '5');
INSERT INTO `menu` VALUES ('6', 'Generate QR Code', 'S', '5', 'merchant/qrcode', 'fa fa-qrcode', '6');
INSERT INTO `menu` VALUES ('7', 'Merchant List', 'S', '5', 'merchant/merchant', 'fa fa-exchange', '7');
INSERT INTO `menu` VALUES ('8', 'Merchant Inquiry', 'S', '5', 'merchant/inquiry', 'fa  fa-exchange', '8');
INSERT INTO `menu` VALUES ('9', 'Merchant Transaksi', 'S', '5', 'merchant/transaksi', 'fa fa-barcode', '9');
INSERT INTO `menu` VALUES ('10', 'Audit', 'H', '0', '', 'fa fa-bar-chart-o', '10');
INSERT INTO `menu` VALUES ('11', 'Log Activity', 'S', '10', 'report/activity', 'fa fa-book', '11');

-- ----------------------------
-- Table structure for `merchant`
-- ----------------------------
DROP TABLE IF EXISTS `merchant`;
CREATE TABLE `merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_merchant` varchar(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(14) NOT NULL,
  `jenis_usaha` varchar(255) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `zipcode` varchar(10) DEFAULT NULL,
  `type_identitas` varchar(50) DEFAULT NULL,
  `no_identitas` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `id_bank` varchar(5) DEFAULT NULL,
  `bank` varchar(250) DEFAULT NULL,
  `rekening` varchar(50) DEFAULT NULL,
  `balance` bigint(20) DEFAULT NULL,
  `mdr` bigint(20) DEFAULT NULL,
  `fee` bigint(20) DEFAULT NULL,
  `withdraw` bigint(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ID_BANK` (`id_bank`),
  KEY `id_merchant` (`id_merchant`),
  CONSTRAINT `ID_BANK` FOREIGN KEY (`id_bank`) REFERENCES `bank` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of merchant
-- ----------------------------
INSERT INTO `merchant` VALUES ('4', 'ABCD123', 'PT ABC', '0821222324252', 'sirup', 'abc jalan jalan', '22122', 'KTP', '12345678901234', 'abc@gmail.com', '011', 'Bank Danamon', '1113334567', '164044', '20', '32809', '131235', 'ACTIVE');
INSERT INTO `merchant` VALUES ('6', 'MCTS', 'Merchant Test', '082123224455', 'Jus Buah', 'alamat palsu', '22334', 'KTP', '29388844499999', 'mercant@gmail.com', '002', 'Bank BRI', '20309994005999', '0', '3', '0', '0', 'ACTIVE');
INSERT INTO `merchant` VALUES ('7', 'SYA11', 'Idolmart', '087782257271', 'pernak pernik', 'jalan idola gang asik', '12310', 'KTP', '39773992090090', 'idolmart@gmail.com', '013', 'Permata Bank', '028982989999', null, '10', null, null, 'ACTIVE');
INSERT INTO `merchant` VALUES ('8', 'TM100', 'Jual Snack Murah ', '085730572300', 'Dagang Makanan', 'adasdsfsfsfsfdf', '12520', 'KTP', '09867892567', 'berta.shite@gmail.com', '009', 'Bank BNI', '737527829010', '19474', '3', '584', '18890', 'ACTIVE');
INSERT INTO `merchant` VALUES ('9', 'SAM001', 'Chiki', '087782257270', 'klontong', 'pondok pinang', '12310', 'KTP', '31740521038700', '', '002', 'Bank BRI', '1002221788', null, '3', null, null, 'ACTIVE');
INSERT INTO `merchant` VALUES ('10', 'TLTOM1', 'Telolet Om ', '897771333332', 'BIS', 'ophir 1 no 2 pakubuwono', '12120', 'KTP', '31729498948888', 'teloletom@gmail.com', '009', 'Bank BNI', '102929990000', '0', '3', '0', '0', 'ACTIVE');

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) NOT NULL DEFAULT '',
  `nama` varchar(200) DEFAULT NULL,
  `harga` varchar(20) DEFAULT NULL,
  `user_id` varchar(50) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'CURRENT_TIMESTAMP()',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('3', '111222333', 'handphone', '200000', '1', '2016-03-02 09:15:21');
INSERT INTO `product` VALUES ('4', '1234567890', 'rokok sin', '12500', '1', '2016-03-02 09:15:25');
INSERT INTO `product` VALUES ('5', '21212333', 'djisamsoe', '12345', '1', '2016-03-02 09:59:40');
INSERT INTO `product` VALUES ('8', '540545622618', 'erre', '211122', '1', '2016-03-02 09:15:36');
INSERT INTO `product` VALUES ('9', '8998989121163', 'rokok ', '10000', '1', '2016-03-02 09:15:39');
INSERT INTO `product` VALUES ('10', '8999909096004', 'mild', '18500', '1', '2016-03-02 09:15:44');
INSERT INTO `product` VALUES ('13', '0987654321', 'keripik singkong', '5000', '33', '2016-03-02 09:15:52');

-- ----------------------------
-- Table structure for `qr`
-- ----------------------------
DROP TABLE IF EXISTS `qr`;
CREATE TABLE `qr` (
  `id` bigint(20) NOT NULL,
  `id_merchant` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of qr
-- ----------------------------

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  `is_admin` varchar(1) DEFAULT 'N',
  `partner_id` int(11) DEFAULT NULL,
  `program_id` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `id` (`id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Super Admin', 'Y', '0', null);
INSERT INTO `role` VALUES ('30', 'Admin  Operation', 'N', '0', null);

-- ----------------------------
-- Table structure for `role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `role_menu`;
CREATE TABLE `role_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `create_` tinyint(1) DEFAULT NULL,
  `read_` tinyint(1) DEFAULT NULL,
  `update_` tinyint(1) DEFAULT NULL,
  `delete_` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=495 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role_menu
-- ----------------------------
INSERT INTO `role_menu` VALUES ('439', '30', '1', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('440', '30', '6', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('441', '30', '7', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('442', '30', '8', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('443', '30', '10', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('487', '1', '1', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('488', '1', '3', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('489', '1', '4', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('490', '1', '6', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('491', '1', '7', '1', '1', '1', '0');
INSERT INTO `role_menu` VALUES ('492', '1', '8', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('493', '1', '9', '1', '1', '1', '1');
INSERT INTO `role_menu` VALUES ('494', '1', '11', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `transaksi`
-- ----------------------------
DROP TABLE IF EXISTS `transaksi`;
CREATE TABLE `transaksi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `trxid` varchar(250) NOT NULL,
  `inquiryid` varchar(250) DEFAULT NULL,
  `requestid` varchar(250) DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `app_code` varchar(100) DEFAULT NULL,
  `id_merchant` varchar(100) DEFAULT '',
  `msisdn` varchar(14) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `amount_trx` bigint(20) DEFAULT NULL,
  `serial_number` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of transaksi
-- ----------------------------
INSERT INTO `transaksi` VALUES ('1', 'UPOC20161215163526COPU26062', 'EVGL20161215162004LGVE932617', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', '', '2016-12-15 16:35:26', null, null);
INSERT INTO `transaksi` VALUES ('2', 'YGTM20161215165708MTGY155576', 'CMGD20161215165647DGMC43091', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nNomor Hp / Kode Merchant tidak terdaftar <mCoin>', '2016-12-15 16:57:09', null, null);
INSERT INTO `transaksi` VALUES ('3', 'BXJA20161216090609AJXB811530', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nNomor Hp / Kode Merchant tidak terdaftar <mCoin>', '2016-12-16 09:06:10', null, null);
INSERT INTO `transaksi` VALUES ('4', 'RMAG20161216094404GAMR126772', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nNomor Hp / Kode Merchant tidak terdaftar <mCoin>', '2016-12-16 09:44:05', null, null);
INSERT INTO `transaksi` VALUES ('5', 'BOAH20161216105356HAOB164270', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.6289652787194.invalidPin-time-10:54:01:843>PIN yang anda masukkan salah <mCoin>', '2016-12-16 10:53:59', null, null);
INSERT INTO `transaksi` VALUES ('6', 'VDNI20161216105526INDV958745', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.6289652787194.invalidPin-time-10:55:31:562>PIN yang anda masukkan salah <mCoin>', '2016-12-16 10:55:27', null, null);
INSERT INTO `transaksi` VALUES ('7', 'WLGJ20161216110014JGLW16492', 'CRGQ20161216090521QGRC243960', '123456789', '1000', 'AREMA', 'MCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.6289652787194.invalidPin-time-11:00:19:765>PIN yang anda masukkan salah <mCoin>', '2016-12-16 11:00:15', null, null);
INSERT INTO `transaksi` VALUES ('8', 'IMLF20161216134532FLMI750155', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-13:45:37:328>PIN yang anda masukkan salah <mCoin>', '2016-12-16 13:45:33', null, null);
INSERT INTO `transaksi` VALUES ('9', 'YCKJ20161216143657JKCY678294', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-14:37:02:937>PIN yang anda masukkan salah <mCoin>', '2016-12-16 14:36:58', null, null);
INSERT INTO `transaksi` VALUES ('10', 'ODVY20161216144656YVDO944278', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-14:47:01:875>PIN yang anda masukkan salah <mCoin>', '2016-12-16 14:46:57', null, null);
INSERT INTO `transaksi` VALUES ('11', 'FGHL20161216144855LHGF349928', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-14:49:01:562>PIN yang anda masukkan salah <mCoin>', '2016-12-16 14:48:59', null, null);
INSERT INTO `transaksi` VALUES ('12', 'ZUMX20161216145711XMUZ917451', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-14:57:16:828>PIN yang anda masukkan salah <mCoin>', '2016-12-16 14:57:12', null, null);
INSERT INTO `transaksi` VALUES ('13', 'ZEPX20161216150352XPEZ947354', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.invalidPin-time-15:03:57:921>PIN yang anda masukkan salah <mCoin>', '2016-12-16 15:03:53', null, null);
INSERT INTO `transaksi` VALUES ('14', 'BHWO20161216154017OWHB241671', 'BPQG20161216134512GQPB503596', '123456789', '1000', 'AREMA', 'ABCD123', '087782257270', 'syamsul', 'FAILED', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.000\nBERHASIL\nSaldo saat ini: 476.854\r\n <mCoin>', '2016-12-16 15:40:20', null, null);
INSERT INTO `transaksi` VALUES ('15', 'REUS20161216155348SUER81479', 'IJZY20161216155305YZJI589186', '123456789', '1500', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'FAILED', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.500\nBERHASIL\nSaldo saat ini: 475.354\r\n <mCoin>', '2016-12-16 15:53:50', null, null);
INSERT INTO `transaksi` VALUES ('16', 'EXTY20161216155429YTXE30519', 'IJZY20161216155305YZJI589186', '123456789', '1500', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'FAILED', 'TEST MERCHANT\nTRF.1500.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-16 15:54:30', null, null);
INSERT INTO `transaksi` VALUES ('17', 'WEMT20161216155547TMEW386570', 'ZJUQ20161216155525QUJZ92265', '123456789', '100', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 100\nBERHASIL\nSaldo saat ini: 475.254\r\n <mCoin>', '2016-12-16 15:55:50', null, null);
INSERT INTO `transaksi` VALUES ('18', 'EYZL20161216160315LZYE72890', 'WOJQ20161216160307QJOW144632', '123456789', '100', 'AREMA', 'MCTS', '08577544678', 'Anwar', 'FAILED', 'TEST MERCHANT\nNomor Hp / Kode Merchant tidak terdaftar <mCoin>', '2016-12-16 16:03:16', null, null);
INSERT INTO `transaksi` VALUES ('19', 'HPBL20161216161437LBPH443719', 'FRBZ20161216161354ZBRF542995', '123456789', '100', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'FAILED', 'TEST MERCHANT\nTRF.100.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-16 16:14:39', null, null);
INSERT INTO `transaksi` VALUES ('20', 'LWEF20161216161537FEWL947691', 'ZADP20161216161527PDAZ949894', '123456789', '500', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 500\nBERHASIL\nSaldo saat ini: 474.754\r\n <mCoin>', '2016-12-16 16:15:39', null, null);
INSERT INTO `transaksi` VALUES ('21', 'IWYH20161216161818HYWI886554', 'ZIOD20161216161753DOIZ795537', '123456789', '600', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 600\nBERHASIL\nSaldo saat ini: 474.154\r\n <mCoin>', '2016-12-16 16:18:20', null, null);
INSERT INTO `transaksi` VALUES ('22', '7b1b2d714f9de66945333d925747cce2', 'dc852bc1aa3d57940dcfa3065b7ef536', 'sfsdre4434drgf456', '1400', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.400\nBERHASIL\nSaldo saat ini: 472.754\r\n <mCoin>', '2016-12-16 16:22:52', null, null);
INSERT INTO `transaksi` VALUES ('23', '4665dd91186d85c88663aaa0b49be3ad', '5419b55714c7d25d5759aac2e4ac6b1c', 'ewf3245fdsde323fsd', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.700\nBERHASIL\nSaldo saat ini: 471.054\r\n <mCoin>', '2016-12-16 16:33:21', null, null);
INSERT INTO `transaksi` VALUES ('24', '30966e2c9fd8a79c7293acad8f864fe8', '8e9f1955c98e7347f2f8f9cc57ea3f30', 'ewf3245fdsde323fsd', '1700', 'AREMA', 'ABCD123', '08577544678', 'Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.700\nBERHASIL\nSaldo saat ini: 469.354\r\n <mCoin>', '2016-12-20 09:32:28', null, null);
INSERT INTO `transaksi` VALUES ('25', '143db83312c34121bef5a7d6636dbb90', '3758b31b80bf308c931062905f995ddb', '47de3a2i5mkg', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.000\nBERHASIL\nSaldo saat ini: 468.354\r\n <mCoin>', '2016-12-20 11:18:13', null, null);
INSERT INTO `transaksi` VALUES ('26', '4a5b35172a21a8116765b144c7dbd9f2', 'afa13d9ce5c463098ab8532190b72fdf', '47de4khr8jac', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 11:19:52', null, null);
INSERT INTO `transaksi` VALUES ('27', '8d5d5e4ad29eb0d613449fc7ba2cd3d3', 'afa13d9ce5c463098ab8532190b72fdf', '47de4wamlko4', '1000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 11:20:18', null, null);
INSERT INTO `transaksi` VALUES ('28', 'beb00ae3474a9889f571d1b1674871ce', '5dde21a777d12298dad52189739d2cf1', '47de62luylgk', '2000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.000\nBERHASIL\nSaldo saat ini: 466.354\r\n <mCoin>', '2016-12-20 11:21:51', null, null);
INSERT INTO `transaksi` VALUES ('29', '87bdab330b9656d3560b133015481178', '80e1f79acbf533b497e2844419d4c58f', '47deilokq680', '2500', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.500\nBERHASIL\nSaldo saat ini: 463.854\r\n <mCoin>', '2016-12-20 11:38:13', null, null);
INSERT INTO `transaksi` VALUES ('30', '9499ffdc239267711d829256599e6aa6', '9d29779f4d7caa46e6115d5b5cdbc380', '47dejnpbj1k4', '3000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 3.000\nBERHASIL\nSaldo saat ini: 460.854\r\n <mCoin>', '2016-12-20 11:39:35', null, null);
INSERT INTO `transaksi` VALUES ('31', '62f6edb73035ddec003eae83aebe57dd', '4b45933cc90fb2f61ba9087c171698e2', '47dj0kngxzok', '25000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 25.000\nBERHASIL\nSaldo saat ini: 425.854\r\n <mCoin>', '2016-12-20 15:09:45', null, null);
INSERT INTO `transaksi` VALUES ('32', 'c789eec2b9a83dcaccceaa1898b02b5c', '4b45933cc90fb2f61ba9087c171698e2', '47dj13k1uko4', '25000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.25000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:10:26', null, null);
INSERT INTO `transaksi` VALUES ('33', 'a16c2e4e44c148f82702377886d2fa0f', 'db0e8a5d77018b74647927843d30a60e', '47djdd2tomo0', '25000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.25000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:26:27', null, null);
INSERT INTO `transaksi` VALUES ('34', '79c15f5e558347e6f2967a333d1a78e4', '3cfbcc3d4531df75458571919ef9775e', '47djdzwkzdq8', '3000', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.3000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:27:16', null, null);
INSERT INTO `transaksi` VALUES ('35', '43b33318871c755fbbe8bbfe7bcd3ff5', '330fe22c98595602668bb7843593e0bd', '47djeqqripq8', '1400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.400\nBERHASIL\nSaldo saat ini: 424.454\r\n <mCoin>', '2016-12-20 15:28:15', null, null);
INSERT INTO `transaksi` VALUES ('36', 'ab97397f2fb8cbbec9339e710fed8bfd', '1b2b066dfa7e3bd20c9b0993c17d0948', '47djhbyusi80', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.300\nBERHASIL\nSaldo saat ini: 422.154\r\n <mCoin>', '2016-12-20 15:31:39', null, null);
INSERT INTO `transaksi` VALUES ('37', 'bf1a71c031c5283657af7eae451b7bbf', 'c51642cee4a2c96e28ced2597c77eda9', '47djtfrtgxyc', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:47:27', null, null);
INSERT INTO `transaksi` VALUES ('38', '55b0cb8662092a7e6cece49ecd424b9a', '256c7642f5ca526cab5a584f22ba3ad4', '47dju1k70cw0', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:48:14', null, null);
INSERT INTO `transaksi` VALUES ('39', 'afd60011a369994a99c84ee780d101b4', '256c7642f5ca526cab5a584f22ba3ad4', '47djujitusis', '2300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 15:48:53', null, null);
INSERT INTO `transaksi` VALUES ('40', 'e85ac5e5f216066a6b9eebfc92c823f4', '13e8005e8242a2fc197cc3767698619f', '47djvktebzeo', '2400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.400\nBERHASIL\nSaldo saat ini: 397.754\r\n <mCoin>', '2016-12-20 15:50:14', null, null);
INSERT INTO `transaksi` VALUES ('41', '1f2c511281c88db8c94cebdb7c94688a', 'c9d4f2c0f97fc4d9b3c556fdec59a75b', 'ewf3245fdsde323fsd', '1700', 'AREMA', 'ABCD123', '08577544678', 'Syamsul Anwar', 'FAILED', 'TEST MERCHANT\nTRF.1700.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 17:14:06', null, null);
INSERT INTO `transaksi` VALUES ('42', '4234a4a4b8123136505ceec084da85a6', '27d2fc44e9f0f334c6388bca5fe9b3d3', 'ewf3245fdsde323fsd', '300', 'AREMA', 'ABCD123', '08577544678', 'Syamsul Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 300\nBERHASIL\nSaldo saat ini: 397.454\r\n <mCoin>', '2016-12-20 17:16:39', null, null);
INSERT INTO `transaksi` VALUES ('43', '1201c9927e7a5e926f740ebaf282451b', '9770e594d209c9f3c0d2a134017355f1', '47dn5fq37zs4', '12000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.12000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 18:24:10', null, null);
INSERT INTO `transaksi` VALUES ('44', 'dce877beee85bac52c2381219f221cf4', 'd17d03857a565d861d5739869e49b29e', '47dn5tn6fhgk', '5000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 5.000\nBERHASIL\nSaldo saat ini: 392.454\r\n <mCoin>', '2016-12-20 18:24:41', null, null);
INSERT INTO `transaksi` VALUES ('45', '2e364508143e2200e7b0f62ef2b536ab', 'a0603e0c7aa7939a50879b0cd128a8a8', '47dn6ppusi04', '1000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 18:25:50', null, null);
INSERT INTO `transaksi` VALUES ('46', '3996db44e55c6326c5be9ac7e22dea75', '5b4fcdccd053523774f6441bff5d214f', '47dn77tna10k', '1000', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-20 18:26:29', null, null);
INSERT INTO `transaksi` VALUES ('47', 'd385d52e9cccf1c174eb3cdd211e014f', 'ef64dd7764df4c27359471ce1d6ea081', '47dn89jrcx4w', '2156', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.156\nBERHASIL\nSaldo saat ini: 390.298\r\n <mCoin>', '2016-12-20 18:27:52', null, null);
INSERT INTO `transaksi` VALUES ('48', '51f1b356f6bfda80576a57fcf26810f9', '3ed338b6aace0934bf4dcadd1f1a3ccb', '47dnd4jo8hes', '1257', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.257\nBERHASIL\nSaldo saat ini: 389.041\r\n <mCoin>', '2016-12-20 18:34:14', null, null);
INSERT INTO `transaksi` VALUES ('49', '44e44b11c83486f62991dc6384913ede', '170b967e5bb9de0e5bcf90d850f2f4fb', '47dni4pnkmio', '120', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 120\nBERHASIL\nSaldo saat ini: 388.921\r\n <mCoin>', '2016-12-20 18:40:46', null, null);
INSERT INTO `transaksi` VALUES ('50', 'a6d394140f4963a10c5809d6ac7a702e', '7a02bdf4e5493421a07486874ab71cd3', '47e6g0xcoeww', '1470', 'AREMA', 'ABCD123', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.470\nBERHASIL\nSaldo saat ini: 387.451\r\n <mCoin>', '2016-12-21 09:31:22', null, null);
INSERT INTO `transaksi` VALUES ('51', 'c4c6dc9a6e787ae8a530bed2656c584f', 'cf1122898e51c48ac84c1539036d8402', 'ewf3245fdsde323fsd', '300', 'AREMA', 'TM100', '08577544678', 'Syabil', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 301\nBERHASIL\nSaldo saat ini: 387.150\r\n <mCoin>', '2016-12-21 09:52:28', '301', null);
INSERT INTO `transaksi` VALUES ('52', 'd9c0945f81ec1d1f9cabe576fe1f1b8e', '8ef42f7d06973b955e5613417e2ca015', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'Syabil Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 302\nBERHASIL\nSaldo saat ini: 386.848\r\n <mCoin>', '2016-12-21 09:56:09', '302', null);
INSERT INTO `transaksi` VALUES ('53', '57572ca93a2837e46c8f8e8bf3946532', '8ef42f7d06973b955e5613417e2ca015', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'Syabil Anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 303\nBERHASIL\nSaldo saat ini: 386.545\r\n <mCoin>', '2016-12-21 09:56:31', '303', null);
INSERT INTO `transaksi` VALUES ('54', '00103eae1a09f3a558ff9f4c8d54917b', 'd87092d18bbb71e934921fe6861f7cb3', '47e75v74p76s', '1400', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.400\nBERHASIL\nSaldo saat ini: 385.145\r\n <mCoin>', '2016-12-21 10:05:09', '1400', null);
INSERT INTO `transaksi` VALUES ('55', 'c9605cd5c2dca2590850be09bd69cb8a', '63203c0007fec5634819dfb19897ce9a', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 304\nBERHASIL\nSaldo saat ini: 384.841\r\n <mCoin>', '2016-12-21 10:05:20', '304', null);
INSERT INTO `transaksi` VALUES ('56', '16c1a970540f9b35759663857afaf8a8', '63203c0007fec5634819dfb19897ce9a', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 305\nBERHASIL\nSaldo saat ini: 384.536\r\n <mCoin>', '2016-12-21 10:05:28', '305', null);
INSERT INTO `transaksi` VALUES ('57', '48e1eb4045243bdda292624c69bb164e', '86b3a7ec7935d6ef89a51547d0b559ff', '47e76q260usk', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.200\nBERHASIL\nSaldo saat ini: 383.336\r\n <mCoin>', '2016-12-21 10:06:15', '1200', null);
INSERT INTO `transaksi` VALUES ('58', 'da359dfd76bbea58eeee8fcb388db446', '3804ed0afafc5f45a5a6c33d5fc7c414', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 306\nBERHASIL\nSaldo saat ini: 383.030\r\n <mCoin>', '2016-12-21 10:06:27', '306', null);
INSERT INTO `transaksi` VALUES ('59', '5619861cf36043fc2875615a767d1cc4', 'cb0d3f76bc1b455ee2eb57f9ff81246d', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 307\nBERHASIL\nSaldo saat ini: 382.723\r\n <mCoin>', '2016-12-21 10:16:56', '307', 'APSF-693303');
INSERT INTO `transaksi` VALUES ('60', '190d439525e10764960bcd832ecbe719', '051d2684e390ca2398c5b846b7caa52f', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 300\nBERHASIL\nSaldo saat ini: 382.423\r\n <mCoin>', '2016-12-21 10:28:41', '300', 'FPWD-246583');
INSERT INTO `transaksi` VALUES ('61', '3ce289fdb82c374455d7d6f7d4db654c', '30bafee7e40bdd48e9f3914c53533e15', '3423432fdsfsd44343', '300', 'AREMA', 'TM100', '08577544678', 'udin', 'FAILED', 'TEST MERCHANT\nTRF.300.6285730572300.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 10:29:26', '300', null);
INSERT INTO `transaksi` VALUES ('62', 'e9f549f0665def04330da997fd4fe6c4', 'bcdbeb7fbcee4c409ff2cc6c89bf8e68', '47e7xr14w204', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 10.000\nBERHASIL\nSaldo saat ini: 372.423\r\n <mCoin>', '2016-12-21 10:41:32', '10000', 'SFEJ-365991');
INSERT INTO `transaksi` VALUES ('63', '10a5e155e1e33b79f02452c5465c7293', '3a43099dcc2f7f9376092fca47028bb2', '47e849d6a8ow', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 10:50:01', '10000', null);
INSERT INTO `transaksi` VALUES ('64', 'b88c9171e98cc81c4ae30126f8eaf108', '3a43099dcc2f7f9376092fca47028bb2', '47e84dn59hes', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 10:50:10', '10000', null);
INSERT INTO `transaksi` VALUES ('65', 'a2e274369ee5e4d544b9bf4518a2c6ae', '754ecdd628e98ce7ceca6e7cf50dbf82', '47e85032i1a8', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 10:50:58', '10000', null);
INSERT INTO `transaksi` VALUES ('66', '75bb3548902a56e7def8db84d21b3daf', '2567d573ac16e4aacfe806f089d42717', '47e85elfyq68', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 10:51:31', '10000', null);
INSERT INTO `transaksi` VALUES ('67', 'bcc5c249bf6b7fc85b5882e499eeefdf', 'e3ae941d218ac0c485c2cd4014f63ba8', '47e868lbzxkw', '15000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 15.000\nBERHASIL\nSaldo saat ini: 357.423\r\n <mCoin>', '2016-12-21 10:52:36', '15000', 'VXFZ-147435');
INSERT INTO `transaksi` VALUES ('68', 'e4cd1ef7eda981828948bddb4fc9f9ac', 'f7dc9789c37cc488818d262607b0d359', '47e8dlrz0lyc', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:02:13', '10000', null);
INSERT INTO `transaksi` VALUES ('69', '9e94e3fb581e1e6359117e36a1b4b96a', 'f7dc9789c37cc488818d262607b0d359', '47e8dpnbh0u8', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:02:22', '10000', null);
INSERT INTO `transaksi` VALUES ('70', '1a1344bf5a850eb9e054acc96f2b55ba', 'f7dc9789c37cc488818d262607b0d359', '47e8dsm3lvs4', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:02:27', '10000', null);
INSERT INTO `transaksi` VALUES ('71', 'ac391388a35e2f05c6da0503aca58a7c', '426cfd71d96288a133abffcb8c51838b', '47e8dzg58kg0', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:02:42', '10000', null);
INSERT INTO `transaksi` VALUES ('72', '93f9e248e288905a9aa4b71213cbc792', '426cfd71d96288a133abffcb8c51838b', '47e8e5ujodgk', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:02:57', '10000', null);
INSERT INTO `transaksi` VALUES ('73', '8abf842bd12bc0eef11d33a9b8d7e815', '426cfd71d96288a133abffcb8c51838b', '47e8e9ulln40', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:03:06', '10000', null);
INSERT INTO `transaksi` VALUES ('74', 'c9bcb72756568307caf9faa87a552ff3', '426cfd71d96288a133abffcb8c51838b', '47e8ekkjfbc4', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:03:28', '10000', null);
INSERT INTO `transaksi` VALUES ('75', '603f79f5f55cfa430baa75949326bd1a', '426cfd71d96288a133abffcb8c51838b', '47e8emsscnsw', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:03:34', '10000', null);
INSERT INTO `transaksi` VALUES ('76', '4046e31830429104b7c6d3bf3987a466', 'bef39b4e834e5dc0dd2270948144cf2b', '47e8fddqfmqs', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:04:32', '10000', null);
INSERT INTO `transaksi` VALUES ('77', '9a8b8ef54ae5565f300356a29df73786', 'bef39b4e834e5dc0dd2270948144cf2b', '47e8fpcr4xs0', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:04:58', '10000', null);
INSERT INTO `transaksi` VALUES ('78', '4ca6b35a2f764e11209928c146b7b848', 'bef39b4e834e5dc0dd2270948144cf2b', '47e8fukn3h44', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:05:09', '10000', null);
INSERT INTO `transaksi` VALUES ('79', '45abb9bc4d53f1bec04f87d9a91b6eb7', 'bef39b4e834e5dc0dd2270948144cf2b', '47e8fwua6rsw', '10000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.10000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:05:13', '10000', null);
INSERT INTO `transaksi` VALUES ('80', '45be33a2f8021b0781d012926dfa90ec', '7c10dc1290df356620e4c247d487c7b4', '47e8nr37or8k', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.300\nBERHASIL\nSaldo saat ini: 355.123\r\n <mCoin>', '2016-12-21 11:15:30', '2300', 'NDEA-490782');
INSERT INTO `transaksi` VALUES ('81', '071f432a10a66747296e527dd58afcbb', '97b34cec652a8f07b1dba8b27145c576', '47e8ohxujzok', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:16:28', '2300', null);
INSERT INTO `transaksi` VALUES ('82', '5455875f4aae32367acc7225e8ee7222', '97b34cec652a8f07b1dba8b27145c576', '47e8ol3s8zgg', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:16:34', '2300', null);
INSERT INTO `transaksi` VALUES ('83', 'faef703d324c7eb8644502731e3ab695', '2761259de84dbf3fbab9761d2bd76560', '47e8oqkvmvwg', '2400', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.400\nBERHASIL\nSaldo saat ini: 352.723\r\n <mCoin>', '2016-12-21 11:16:46', '2400', 'VXNA-619497');
INSERT INTO `transaksi` VALUES ('84', 'a17c2cd57c696fb76b2d9867f4dff214', '23c46ba400c9be9ad9e90f0474a0c3ad', '47e8rw2cb6kg', '2400', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.2400.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:20:53', '2400', null);
INSERT INTO `transaksi` VALUES ('85', '07c99df1f7157f770b1f9f69960508b8', '48b7c9dd8f2b12ff73ce6fabec3ee61d', '47e8s2zf8fuo', '2300', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.2300.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 11:21:08', '2300', null);
INSERT INTO `transaksi` VALUES ('86', 'bd90a44f5d881831c5c272afe7bf1f0a', 'ce7906fda0e77edd7cd9ae3736a2adcd', '47e8sa4qjauc', '2500', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 2.500\nBERHASIL\nSaldo saat ini: 350.223\r\n <mCoin>', '2016-12-21 11:21:23', '2500', 'VDUJ-255215');
INSERT INTO `transaksi` VALUES ('87', '44cfb1768664d730a507d3b9a7158f73', 'd067a5c903cfadffa95c4437aad71c55', '47e8xafpg62o', '900', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 900\nBERHASIL\nSaldo saat ini: 349.323\r\n <mCoin>', '2016-12-21 11:27:57', '900', 'ZYPN-961041');
INSERT INTO `transaksi` VALUES ('88', 'aec05a65f3394ebc287bb88d65bf8d06', '02f613fc4d2c8d3ea7a8e50efc188af3', '47e8xu09wq0w', '5000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 5.000\nBERHASIL\nSaldo saat ini: 344.323\r\n <mCoin>', '2016-12-21 11:28:39', '5000', 'UCXK-506385');
INSERT INTO `transaksi` VALUES ('89', '4ccf9d4c126dff8b7cf5f835cb83f134', '73e7434be6cb28697ddeeaa5c47dd853', '47e92dgonzwg', '1000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.000\nBERHASIL\nSaldo saat ini: 343.323\r\n <mCoin>', '2016-12-21 11:34:34', '1000', 'WNRU-311817');
INSERT INTO `transaksi` VALUES ('90', '8300576d1245e96911fec4728c575857', 'c7a3ab17b8b7896536b567f53a1226fd', '3423432fdsfsd44343', '600', 'AREMA', 'TM100', '08577544678', 'shinta', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 600\nBERHASIL\nSaldo saat ini: 342.723\r\n <mCoin>', '2016-12-21 11:50:07', '600', 'NHDV-808970');
INSERT INTO `transaksi` VALUES ('91', '42602725f7670bc9edf4578ef089760e', '568925deec65addaca0f4bb5b83003bb', '47e9ppcrnrcw', '60713', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 60.713\nBERHASIL\nSaldo saat ini: 281.010\r\n <mCoin>', '2016-12-21 12:05:04', '60713', 'EOZF-830505');
INSERT INTO `transaksi` VALUES ('92', '57a28663483c7cd19fef3506985a1e8f', '9f6fcb2e53954a04e5af110e51c4309f', '47e9yuivhu04', '1000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 12:16:58', '1000', null);
INSERT INTO `transaksi` VALUES ('93', 'd33c2eaa91174d9b98b799bdcf5633a7', '9f6fcb2e53954a04e5af110e51c4309f', '47e9ywonw18g', '1000', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'FAILED', 'TEST MERCHANT\nTRF.1000.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 12:17:04', '1000', null);
INSERT INTO `transaksi` VALUES ('94', 'd67723545f5f06cf8eaea04f10d81d70', 'd2683b0cc8c08e22a3ecfa33a2647c3f', '47e9z35souas', '1500', 'AREMA', 'ABCD123', '628997136396', 'Cosmos', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.500\nBERHASIL\nSaldo saat ini: 279.510\r\n <mCoin>', '2016-12-21 12:17:18', '1500', 'CBOP-712598');
INSERT INTO `transaksi` VALUES ('95', '19ee5ea39cedfa2bb3d9644dc0a2fe4a', '154db2bd8a13006234901e3e70ff8a02', '47eekt9g5jgg', '501', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 501\nBERHASIL\nSaldo saat ini: 279.009\r\n <mCoin>', '2016-12-21 15:53:46', '501', 'MWZB-999219');
INSERT INTO `transaksi` VALUES ('96', '9292709480561a81328b97fb6d0012c4', 'fbb1a8620c418e2a202517fadc7ca076', '47ef0atlejy8', '110', 'AREMA', 'TM100', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.110.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 16:14:01', '110', null);
INSERT INTO `transaksi` VALUES ('97', '8b9e5130b816a5a07bf4f0ad1c0256d6', '342587674d36760c81ddb53b74ac608e', '47ef1wbk0g6c', '110', 'AREMA', 'TM100', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nTRF.110.6285730572300.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-21 16:16:05', '110', null);
INSERT INTO `transaksi` VALUES ('98', '79f1cf7f4ea07c0a008130bf7e086b5c', 'e49dfa74742cb277f0021f2a0d834053', '47ef2avyyry8', '125', 'AREMA', 'TM100', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 125\nBERHASIL\nSaldo saat ini: 278.774\r\n <mCoin>', '2016-12-21 16:16:35', '125', 'IPVJ-628851');
INSERT INTO `transaksi` VALUES ('99', '8cd1125e42e66469abe1bfde6e452b7b', '3d48c6c9461d2de05d442eef1fcb082a', '47ef7fi45mg4', '212', 'AREMA', 'TM100', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 212\nBERHASIL\nSaldo saat ini: 278.562\r\n <mCoin>', '2016-12-21 16:23:17', '212', 'JSRM-663133');
INSERT INTO `transaksi` VALUES ('100', '62facee271798aca927546d201ba2cf3', 'bc41317e218085f083b9f2b3c3a9dc9f', '47efroyxo92c', '321', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 321\nBERHASIL\nSaldo saat ini: 278.241\r\n <mCoin>', '2016-12-21 16:49:45', '321', 'XBJY-2195');
INSERT INTO `transaksi` VALUES ('101', '5010c17103fb8ddf57ca1e18408f3a7a', '78953bb1a74a66e70bb799fc69f8cf14', '47efvpqkqeck', '1459', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 1.459\nBERHASIL\nSaldo saat ini: 276.782\r\n <mCoin>', '2016-12-21 16:54:59', '1459', 'IEBU-317189');
INSERT INTO `transaksi` VALUES ('102', 'd067d3a10dce293dd5b19ddc08dc7c24', '78df90b54bbc6daf2ad9cea89688bf48', '47eg1k25tdog', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.811.62821222324252.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:02:37', '811', null);
INSERT INTO `transaksi` VALUES ('103', '6c78cc55d0c574c5b7625720621ae2cd', '0bce41bc90880f0266abdf0504947cac', '47eg3pl1k9s0', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.811.62821222324252.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:05:27', '811', null);
INSERT INTO `transaksi` VALUES ('104', 'fb29e5c24fa61cc6d7a67aa704537619', '0bce41bc90880f0266abdf0504947cac', '47eg4hzfdckk', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.811.62821222324252.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:06:29', '811', null);
INSERT INTO `transaksi` VALUES ('105', '7a263cafda3417885cd3a47fdc38643a', '0bce41bc90880f0266abdf0504947cac', '47eg50zyuv40', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.811.62821222324252.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:07:09', '811', null);
INSERT INTO `transaksi` VALUES ('106', '333024e5dc671f4c47d063ede9a80c4d', '0bce41bc90880f0266abdf0504947cac', '47eg7bkeqjs4', '811', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.1234>SEMENTARA TIDAK DAPAT DIPROSES <mCoin>', '2016-12-21 17:10:10', '811', null);
INSERT INTO `transaksi` VALUES ('107', 'aa32313291c457038e0e25a0e8016d8d', 'dfe2ef8cc50e42ad9525a699eaecba15', '47egcc6lk12c', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.1234>SEMENTARA TIDAK DAPAT DIPROSES <mCoin>', '2016-12-21 17:16:43', '232', null);
INSERT INTO `transaksi` VALUES ('108', '736b1719f9d5c3920576f08e557bb4b9', '0160bb99a8d45b72b7553c8aa9618f58', '47egdailj22o', '1000', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nBAYAR.1234>SEMENTARA TIDAK DAPAT DIPROSES <mCoin>', '2016-12-21 17:17:58', '1000', null);
INSERT INTO `transaksi` VALUES ('109', '99bead8237b5e7dd2c8b3eff5f7e428e', '063740ba6b557baeda437501a317443d', '47egdrudl18g', '1120', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1120.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:18:34', '1120', null);
INSERT INTO `transaksi` VALUES ('110', 'f4486c750d80351cef1093fb9f8a49cb', 'e15a7f7e91578f6812892c239fc6ebbd', '47egdxrd4js4', '1112', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1112.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:18:47', '1112', null);
INSERT INTO `transaksi` VALUES ('111', '1584282e2089a0050a4ce3656865019b', '96a9b2408184baa92c5bddeeb3d8bd42', '47ege91iz000', '1230', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1230.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:19:13', '1230', null);
INSERT INTO `transaksi` VALUES ('112', '65eb0af2f45c3353df8c4d200a6f5821', '9d757a1277bcef15a53271a05d36f995', '47egfgiut14w', '2714', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.2714.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:20:48', '2714', null);
INSERT INTO `transaksi` VALUES ('113', 'c9a9634c043b9eead2831388f7c7e363', '321bc6e7011f255b1d2fb5667d6fb5f2', '47egfnfjyyqs', '4200', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.4200.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:21:03', '4200', null);
INSERT INTO `transaksi` VALUES ('114', 'ef98e7d83b0fde698769dfa4d951bd96', 'ca0f88b68e78606be9a7d11eedca94a8', '47eggi4nmm68', '5000', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.5000.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:22:08', '5000', null);
INSERT INTO `transaksi` VALUES ('115', '0355b1c00e9778c2084fac56bf819b8b', '056beb4ab7dabe99e44d378e1a970579', '47eggr4gy7ok', '3037', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.3037.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:22:29', '3037', null);
INSERT INTO `transaksi` VALUES ('116', 'bcc7c45770c9e79845c10dd1ccec62c8', '8adcb5a2e5357a08b991c4f34cc90769', '47eggxzejbac', '1500', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.1500.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:22:44', '1500', null);
INSERT INTO `transaksi` VALUES ('117', '1ecb24353d4f4a5a9cf29cccecdb6484', 'bafe336a2e054dc5f142ec66fb79ecc1', '47egh54vzomc', '125', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.125.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:22:58', '125', null);
INSERT INTO `transaksi` VALUES ('118', '26a35106d3e81903881c154054c82d7b', '91dcd0fd4a1ea9966e18b7eeb396ba00', '47eghw4p8quc', '3789', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'FAILED', 'TEST MERCHANT\nTRF.3789.6285730572300.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-21 17:23:58', '3789', null);
INSERT INTO `transaksi` VALUES ('119', '3a29b7334ff510259070a591ce70b23d', '8e2f4f5b37de59fba583c5b6a5fe19b6', '47egi81lbp0k', '6527', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 6.527\nBERHASIL\nSaldo saat ini: 270.255\r\n <mCoin>', '2016-12-21 17:24:23', '6527', 'DPGU-900835');
INSERT INTO `transaksi` VALUES ('120', 'de1b30b9f6fe95e88d56a68335768949', 'ba54409bf83e2734ab3af31cbd58f488', '47f0pjzovfgg', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 1.200\nBERHASIL\nSaldo saat ini: 269.055\r\n <mCoin>', '2016-12-22 09:14:21', '1200', 'GCZJ-879270');
INSERT INTO `transaksi` VALUES ('121', '369e129f04a47d9fbfd237a9646d319c', 'c7e3efd9a29aa32eed284b54cb846bb1', '47f1ap5jzrac', '111', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 62821222324252 \nNama: PT ABC\nNominal: 111\nBERHASIL\nSaldo saat ini: 268.944\r\n <mCoin>', '2016-12-22 09:41:58', '111', 'VLNH-955584');
INSERT INTO `transaksi` VALUES ('122', 'c8a29781f6ac07da80bb47f6b9f27d24', 'a97364d53028a47befd23f253086d764', '47f1brqmsdc0', '111', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nTRF.111.62821222324252.1234>Pesan Transaksi yang sama sudah pernah dikirim. STATUS: DALAM PROSES. <mCoin>', '2016-12-22 09:43:22', '111', null);
INSERT INTO `transaksi` VALUES ('123', '601ca4defb9099ccef38ea6b246f9a4f', 'a97364d53028a47befd23f253086d764', '47f1cwvc5jsw', '111', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'FAILED', 'TEST MERCHANT\nBAYAR.111.62821222324252.1234>PIN yang anda masukkan salah <mCoin>', '2016-12-22 09:44:50', '111', null);
INSERT INTO `transaksi` VALUES ('124', '4edf68ccd02db49b1ef9bcd495a89850', 'a41cedb468f12db89e015d321170406a', '47f3kxnk36yo', '2500', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 2.500\nBERHASIL\nSaldo saat ini: 266.444\r\n <mCoin>', '2016-12-22 11:29:22', '2500', 'CEIK-334419');
INSERT INTO `transaksi` VALUES ('125', '38eac60e899175bde2facb2a05fb55a0', '90d748216afa4f0d4c5dcf1fed87896e', '47f3lp9otxa8', '1500', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nTRANSFER SALDO \nKe No HP: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 1.500\nBERHASIL\nSaldo saat ini: 264.944\r\n <mCoin>', '2016-12-22 11:30:23', '1500', 'IJUQ-387202');
INSERT INTO `transaksi` VALUES ('126', '1b5e023f60be363df107005322aa4070', '3316991716bcef4813bdfd9519f1f270', '47f7a8pfy30g', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 62821222324252 \nNama: PT ABC\nNominal: 232\nBERHASIL\nSaldo saat ini: 264.712 <mCoin>', '2016-12-22 14:23:30', '232', 'SXOT-724111');
INSERT INTO `transaksi` VALUES ('127', '42e0f9bb73cd9fa821e99c327e769f87', '6e5c1fed5649715ccee44c53ad31a109', '47f7ayor57i8', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 62821222324252 \nNama: PT ABC\nNominal: 232\nBERHASIL\nSaldo saat ini: 264.480 <mCoin>', '2016-12-22 14:24:25', '232', 'QVJI-320615');
INSERT INTO `transaksi` VALUES ('128', '4acea7e723b8b5f45d6dde3797170b48', 'b9385779f0d9c3ff3d97c87072c15930', '47f8ih5szh0k', '1450', 'AREMA', 'TM100', '6285730572300', 'Berta Sihite', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 1.450\nBERHASIL\nSaldo saat ini: 263.030 <mCoin>', '2016-12-22 15:21:16', '1450', 'NGXM-916281');
INSERT INTO `transaksi` VALUES ('129', '174a1f2bd6a380a25d12e0277a229b81', 'a95e6060e746a12b64c3496593e177a7', '47f8lxla68g0', '1200', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 62821222324252 \nNama: PT ABC\nNominal: 1.200\nBERHASIL\nSaldo saat ini: 261.830 <mCoin>', '2016-12-22 15:25:47', '1200', 'ZNJO-246235');
INSERT INTO `transaksi` VALUES ('130', '2dff40444b571d4647ee32f54d9d2dde', '2629e89f6d183050c1d8db1bd9a3a60d', 'KKUF-241830', '1300', 'AREMA', 'ABCD123', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 62821222324252 \nNama: PT ABC\nNominal: 1.300\nBERHASIL\nSaldo saat ini: 260.530 <mCoin>', '2016-12-22 15:31:00', '1300', 'XEGW-94318');
INSERT INTO `transaksi` VALUES ('131', 'db97553e6660c8afc90d469013cfc7d1', '1f4ae5657f93980c30775a4a352f9f3c', '47fb0yzdrckk', '232', 'AREMA', 'ABCD123', '6285777544678', 'anwar', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 62821222324252 \nNama: PT ABC\nNominal: 232\nBERHASIL\nSaldo saat ini: 260.298 <mCoin>', '2016-12-22 17:19:27', '232', 'DPMZ-650622');
INSERT INTO `transaksi` VALUES ('132', 'be7e42c66ecb30e71d75647e0482398a', '6b937291cbfe022470193bf47510b450', '47fvy8jrrykg', '2200', 'AREMA', 'TM100', '6285777925653', 'Ahmad Faisal', 'SUCCESS', 'TEST MERCHANT\nPEMBAYARAN KE MERCHANT: 6285730572300 \nNama: JUAL SNACK MURAH \nNominal: 2.200\nBERHASIL\nSaldo saat ini: 258.098 <mCoin>', '2016-12-23 09:43:17', '2200', 'YZJK-616803');

-- ----------------------------
-- Table structure for `t_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_log`;
CREATE TABLE `t_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `action` varchar(20) DEFAULT NULL,
  `modul` varchar(255) DEFAULT NULL,
  `data` longtext,
  `ip` varchar(15) DEFAULT NULL,
  `ua` varchar(255) DEFAULT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `operator` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=621 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_log
-- ----------------------------
INSERT INTO `t_log` VALUES ('1', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 16:55:01', '');
INSERT INTO `t_log` VALUES ('2', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 16:55:15', '');
INSERT INTO `t_log` VALUES ('3', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 16:55:30', '');
INSERT INTO `t_log` VALUES ('4', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 17:04:21', '');
INSERT INTO `t_log` VALUES ('5', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 17:04:26', '');
INSERT INTO `t_log` VALUES ('6', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 17:11:42', '');
INSERT INTO `t_log` VALUES ('7', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 18:51:53', '');
INSERT INTO `t_log` VALUES ('8', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:36', null);
INSERT INTO `t_log` VALUES ('9', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:41', null);
INSERT INTO `t_log` VALUES ('10', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:46', null);
INSERT INTO `t_log` VALUES ('11', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:49', null);
INSERT INTO `t_log` VALUES ('12', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:52', null);
INSERT INTO `t_log` VALUES ('13', '1', 'View', 'user_management/user_edit/33', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:07:55', null);
INSERT INTO `t_log` VALUES ('14', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-08 19:08:30', null);
INSERT INTO `t_log` VALUES ('15', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 10:50:14', null);
INSERT INTO `t_log` VALUES ('16', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:12:51', null);
INSERT INTO `t_log` VALUES ('17', '1', 'View', 'user_management/role_edit/1', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:12:54', null);
INSERT INTO `t_log` VALUES ('18', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":10,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":11,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:13:00', null);
INSERT INTO `t_log` VALUES ('19', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:13:02', null);
INSERT INTO `t_log` VALUES ('20', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:16:09', null);
INSERT INTO `t_log` VALUES ('21', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:16:13', null);
INSERT INTO `t_log` VALUES ('22', '1', 'View', 'user_management/role_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:17:42', null);
INSERT INTO `t_log` VALUES ('23', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:17:55', null);
INSERT INTO `t_log` VALUES ('24', '1', 'View', 'user_management/user_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 11:17:58', null);
INSERT INTO `t_log` VALUES ('25', '1', 'Login', 'Login', '{\"userid_Kasir\":\"1\",\"username_Kasir\":\"admin\",\"name_Kasir\":\"Administrator\",\"role_Kasir\":\"1\",\"isadmin_Kasir\":\"Y\",\"email_user_Kasir\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 14:36:52', null);
INSERT INTO `t_log` VALUES ('26', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:04:35', null);
INSERT INTO `t_log` VALUES ('27', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:04:39', null);
INSERT INTO `t_log` VALUES ('28', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:06:51', null);
INSERT INTO `t_log` VALUES ('29', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:16', null);
INSERT INTO `t_log` VALUES ('30', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:26', null);
INSERT INTO `t_log` VALUES ('31', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:32', null);
INSERT INTO `t_log` VALUES ('32', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:39', null);
INSERT INTO `t_log` VALUES ('33', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:48', null);
INSERT INTO `t_log` VALUES ('34', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:53', null);
INSERT INTO `t_log` VALUES ('35', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:10:56', null);
INSERT INTO `t_log` VALUES ('36', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:11:00', null);
INSERT INTO `t_log` VALUES ('37', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:13:08', null);
INSERT INTO `t_log` VALUES ('38', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:13:11', null);
INSERT INTO `t_log` VALUES ('39', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:13:53', null);
INSERT INTO `t_log` VALUES ('40', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:13:58', null);
INSERT INTO `t_log` VALUES ('41', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:14:02', null);
INSERT INTO `t_log` VALUES ('42', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:17:50', null);
INSERT INTO `t_log` VALUES ('43', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:18:06', null);
INSERT INTO `t_log` VALUES ('44', '1', 'Update', 'user_management/profile/', '{\"username\":\"admin\",\"email\":\"admin@gmail.com\",\"name\":\"Administrator\",\"phone\":\"087875852955\",\"address1\":\"pondok pinang\",\"no_identitas\":\"1234563434\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:18:37', null);
INSERT INTO `t_log` VALUES ('45', '1', 'Update', 'user_management/profile/', '{\"username\":\"admin\",\"email\":\"admin@gmail.com\",\"name\":\"Administrator\",\"phone\":\"087875852955\",\"address1\":\"pondok pinang\",\"no_identitas\":\"1234563434\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:18:52', null);
INSERT INTO `t_log` VALUES ('46', '1', 'Update', 'user_management/profile/', '{\"username\":\"admin\",\"email\":\"admin@gmail.com\",\"name\":\"Admin\",\"phone\":\"087875852955\",\"address1\":\"pondok pinang\",\"no_identitas\":\"1234563434\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:08', null);
INSERT INTO `t_log` VALUES ('47', '1', 'Logout', 'Logout', '{\"userid_Kasir\":\"\",\"username_Kasir\":\"\",\"name_Kasir\":\"\",\"role_Kasir\":\"\",\"isadmin_Kasir\":\"\",\"partner_Kasir\":\"\",\"program_Kasir\":\"\",\"email_user_Kasir\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:21', null);
INSERT INTO `t_log` VALUES ('48', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Admin\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:27', null);
INSERT INTO `t_log` VALUES ('49', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Admin\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:34', null);
INSERT INTO `t_log` VALUES ('50', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Admin\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:45', null);
INSERT INTO `t_log` VALUES ('51', '32', 'Login', 'Login', '{\"userid_Merchant\":\"32\",\"username_Merchant\":\"acunk\",\"name_Merchant\":\"acunk aja\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"N\",\"email_user_Merchant\":\"acunk21@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:19:50', null);
INSERT INTO `t_log` VALUES ('52', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:20:35', null);
INSERT INTO `t_log` VALUES ('53', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:21:46', null);
INSERT INTO `t_log` VALUES ('54', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:23:01', null);
INSERT INTO `t_log` VALUES ('55', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:31:18', null);
INSERT INTO `t_log` VALUES ('56', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:31:18', null);
INSERT INTO `t_log` VALUES ('57', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:41:14', null);
INSERT INTO `t_log` VALUES ('58', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:42:53', null);
INSERT INTO `t_log` VALUES ('59', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:46:03', null);
INSERT INTO `t_log` VALUES ('60', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:48:51', null);
INSERT INTO `t_log` VALUES ('61', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:51:06', null);
INSERT INTO `t_log` VALUES ('62', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:51:53', null);
INSERT INTO `t_log` VALUES ('63', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:53:25', null);
INSERT INTO `t_log` VALUES ('64', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:53:39', null);
INSERT INTO `t_log` VALUES ('65', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-09 15:56:26', null);
INSERT INTO `t_log` VALUES ('66', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 09:46:53', null);
INSERT INTO `t_log` VALUES ('67', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 09:47:06', null);
INSERT INTO `t_log` VALUES ('68', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 09:47:20', null);
INSERT INTO `t_log` VALUES ('69', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:17:35', null);
INSERT INTO `t_log` VALUES ('70', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:17:38', null);
INSERT INTO `t_log` VALUES ('71', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:19:03', null);
INSERT INTO `t_log` VALUES ('72', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:19:24', null);
INSERT INTO `t_log` VALUES ('73', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:19:40', null);
INSERT INTO `t_log` VALUES ('74', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:19:48', null);
INSERT INTO `t_log` VALUES ('75', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:20:22', null);
INSERT INTO `t_log` VALUES ('76', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:20:26', null);
INSERT INTO `t_log` VALUES ('77', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:20:48', null);
INSERT INTO `t_log` VALUES ('78', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:21:32', null);
INSERT INTO `t_log` VALUES ('79', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:28:53', null);
INSERT INTO `t_log` VALUES ('80', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:29:00', null);
INSERT INTO `t_log` VALUES ('81', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:29:30', null);
INSERT INTO `t_log` VALUES ('82', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:29:52', null);
INSERT INTO `t_log` VALUES ('83', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:30:38', null);
INSERT INTO `t_log` VALUES ('84', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:30:45', null);
INSERT INTO `t_log` VALUES ('85', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:32:28', null);
INSERT INTO `t_log` VALUES ('86', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:32:37', null);
INSERT INTO `t_log` VALUES ('87', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:33:31', null);
INSERT INTO `t_log` VALUES ('88', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:35:07', null);
INSERT INTO `t_log` VALUES ('89', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:35:11', null);
INSERT INTO `t_log` VALUES ('90', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:37:44', null);
INSERT INTO `t_log` VALUES ('91', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:37:47', null);
INSERT INTO `t_log` VALUES ('92', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:39:54', null);
INSERT INTO `t_log` VALUES ('93', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:39:57', null);
INSERT INTO `t_log` VALUES ('94', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:34', null);
INSERT INTO `t_log` VALUES ('95', '1', 'View', 'user_management/role_edit/1', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:39', null);
INSERT INTO `t_log` VALUES ('96', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":10,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"0\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:46', null);
INSERT INTO `t_log` VALUES ('97', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:48', null);
INSERT INTO `t_log` VALUES ('98', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:52', null);
INSERT INTO `t_log` VALUES ('99', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:54', null);
INSERT INTO `t_log` VALUES ('100', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:40:59', null);
INSERT INTO `t_log` VALUES ('101', '1', 'View', 'user_management/role_edit/1', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:04', null);
INSERT INTO `t_log` VALUES ('102', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":10,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:09', null);
INSERT INTO `t_log` VALUES ('103', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:11', null);
INSERT INTO `t_log` VALUES ('104', '1', 'View', 'user_management/role_edit/1', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:17', null);
INSERT INTO `t_log` VALUES ('105', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":10,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"0\",\"read_\":\"1\",\"update_\":\"0\",\"delete_\":\"0\"}]]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:24', null);
INSERT INTO `t_log` VALUES ('106', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:26', null);
INSERT INTO `t_log` VALUES ('107', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:28', null);
INSERT INTO `t_log` VALUES ('108', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:41:31', null);
INSERT INTO `t_log` VALUES ('109', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:44:16', null);
INSERT INTO `t_log` VALUES ('110', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:44:44', null);
INSERT INTO `t_log` VALUES ('111', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:47:33', null);
INSERT INTO `t_log` VALUES ('112', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:48:14', null);
INSERT INTO `t_log` VALUES ('113', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:48:58', null);
INSERT INTO `t_log` VALUES ('114', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:49:28', null);
INSERT INTO `t_log` VALUES ('115', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:53:40', null);
INSERT INTO `t_log` VALUES ('116', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:54:25', null);
INSERT INTO `t_log` VALUES ('117', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:55:02', null);
INSERT INTO `t_log` VALUES ('118', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:58:21', null);
INSERT INTO `t_log` VALUES ('119', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:58:23', null);
INSERT INTO `t_log` VALUES ('120', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:58:44', null);
INSERT INTO `t_log` VALUES ('121', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:59:19', null);
INSERT INTO `t_log` VALUES ('122', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 10:59:41', null);
INSERT INTO `t_log` VALUES ('123', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:00:32', null);
INSERT INTO `t_log` VALUES ('124', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:01:10', null);
INSERT INTO `t_log` VALUES ('125', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:02:13', null);
INSERT INTO `t_log` VALUES ('126', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:03:33', null);
INSERT INTO `t_log` VALUES ('127', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:05:01', null);
INSERT INTO `t_log` VALUES ('128', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:06:17', null);
INSERT INTO `t_log` VALUES ('129', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:06:42', null);
INSERT INTO `t_log` VALUES ('130', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:09:55', null);
INSERT INTO `t_log` VALUES ('131', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:11:30', null);
INSERT INTO `t_log` VALUES ('132', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:12:44', null);
INSERT INTO `t_log` VALUES ('133', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:12:46', null);
INSERT INTO `t_log` VALUES ('134', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:14:34', null);
INSERT INTO `t_log` VALUES ('135', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:15:03', null);
INSERT INTO `t_log` VALUES ('136', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:15:28', null);
INSERT INTO `t_log` VALUES ('137', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:15:43', null);
INSERT INTO `t_log` VALUES ('138', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:20:03', null);
INSERT INTO `t_log` VALUES ('139', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:20:49', null);
INSERT INTO `t_log` VALUES ('140', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:21:06', null);
INSERT INTO `t_log` VALUES ('141', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:21:17', null);
INSERT INTO `t_log` VALUES ('142', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:22:47', null);
INSERT INTO `t_log` VALUES ('143', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:24:50', null);
INSERT INTO `t_log` VALUES ('144', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:25:00', null);
INSERT INTO `t_log` VALUES ('145', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:25:16', null);
INSERT INTO `t_log` VALUES ('146', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:26:26', null);
INSERT INTO `t_log` VALUES ('147', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:26:42', null);
INSERT INTO `t_log` VALUES ('148', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:28:24', null);
INSERT INTO `t_log` VALUES ('149', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:30:13', null);
INSERT INTO `t_log` VALUES ('150', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:30:33', null);
INSERT INTO `t_log` VALUES ('151', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:31:27', null);
INSERT INTO `t_log` VALUES ('152', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:34:44', null);
INSERT INTO `t_log` VALUES ('153', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:38:05', null);
INSERT INTO `t_log` VALUES ('154', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:39:01', null);
INSERT INTO `t_log` VALUES ('155', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:42:09', null);
INSERT INTO `t_log` VALUES ('156', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:42:09', null);
INSERT INTO `t_log` VALUES ('157', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:45:10', null);
INSERT INTO `t_log` VALUES ('158', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:45:12', null);
INSERT INTO `t_log` VALUES ('159', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 11:45:49', null);
INSERT INTO `t_log` VALUES ('160', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:14:05', null);
INSERT INTO `t_log` VALUES ('161', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:14:12', null);
INSERT INTO `t_log` VALUES ('162', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:14:17', null);
INSERT INTO `t_log` VALUES ('163', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:14:40', null);
INSERT INTO `t_log` VALUES ('164', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:14:48', null);
INSERT INTO `t_log` VALUES ('165', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:20:04', null);
INSERT INTO `t_log` VALUES ('166', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:22:30', null);
INSERT INTO `t_log` VALUES ('167', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:24:01', null);
INSERT INTO `t_log` VALUES ('168', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:24:22', null);
INSERT INTO `t_log` VALUES ('169', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:26:34', null);
INSERT INTO `t_log` VALUES ('170', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:27:48', null);
INSERT INTO `t_log` VALUES ('171', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:53:40', null);
INSERT INTO `t_log` VALUES ('172', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:54:24', null);
INSERT INTO `t_log` VALUES ('173', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 13:55:14', null);
INSERT INTO `t_log` VALUES ('174', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:00:58', null);
INSERT INTO `t_log` VALUES ('175', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:01:51', null);
INSERT INTO `t_log` VALUES ('176', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:03:04', null);
INSERT INTO `t_log` VALUES ('177', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:03:40', null);
INSERT INTO `t_log` VALUES ('178', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:03:56', null);
INSERT INTO `t_log` VALUES ('179', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:04:16', null);
INSERT INTO `t_log` VALUES ('180', '1', 'View Forbidden', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 14:27:05', null);
INSERT INTO `t_log` VALUES ('181', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:33:14', null);
INSERT INTO `t_log` VALUES ('182', '1', 'View', 'user_management/role_edit/1', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:33:17', null);
INSERT INTO `t_log` VALUES ('183', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":10,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:33:25', null);
INSERT INTO `t_log` VALUES ('184', '1', 'View', 'user_management/role', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:33:26', null);
INSERT INTO `t_log` VALUES ('185', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:52:36', null);
INSERT INTO `t_log` VALUES ('186', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:55:59', null);
INSERT INTO `t_log` VALUES ('187', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:56:29', null);
INSERT INTO `t_log` VALUES ('188', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:58:44', null);
INSERT INTO `t_log` VALUES ('189', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 15:59:05', null);
INSERT INTO `t_log` VALUES ('190', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"q\",\"name\":\"w\",\"phone\":\"e\",\"jenis_usaha\":\"\",\"zipcode\":\"\",\"type_identitas\":\"KTP\",\"no_identitas\":\"h\",\"id_bank\":\"014\",\"bank\":\"BANK BCA\",\"rekening\":\"5\",\"mdr\":\"r\",\"status\":\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:01:03', null);
INSERT INTO `t_log` VALUES ('191', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:01:13', null);
INSERT INTO `t_log` VALUES ('192', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:03:53', null);
INSERT INTO `t_log` VALUES ('193', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:04:36', null);
INSERT INTO `t_log` VALUES ('194', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:05:05', null);
INSERT INTO `t_log` VALUES ('195', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:07:04', null);
INSERT INTO `t_log` VALUES ('196', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:08:21', null);
INSERT INTO `t_log` VALUES ('197', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:09:53', null);
INSERT INTO `t_log` VALUES ('198', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:10:14', null);
INSERT INTO `t_log` VALUES ('199', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:11:44', null);
INSERT INTO `t_log` VALUES ('200', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:12:14', null);
INSERT INTO `t_log` VALUES ('201', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:12:43', null);
INSERT INTO `t_log` VALUES ('202', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:13:00', null);
INSERT INTO `t_log` VALUES ('203', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:15:45', null);
INSERT INTO `t_log` VALUES ('204', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:17:34', null);
INSERT INTO `t_log` VALUES ('205', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"sdd\",\"name\":\"sddds\",\"phone\":\"3223\",\"jenis_usaha\":\"\",\"zipcode\":\"3324\",\"type_identitas\":\"KTP\",\"no_identitas\":\"333\",\"id_bank\":\"014\",\"bank\":\"BANK BCA\",\"rekening\":\"333\",\"mdr\":\"33\",\"status\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:18:08', null);
INSERT INTO `t_log` VALUES ('206', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"sdddd\",\"name\":\"sddds\",\"phone\":\"3223\",\"jenis_usaha\":\"sdsd\",\"zipcode\":\"3324\",\"type_identitas\":\"KTP\",\"no_identitas\":\"333\",\"id_bank\":\"014\",\"bank\":\"BANK BCA\",\"rekening\":\"333\",\"mdr\":\"33\",\"status\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:20:00', null);
INSERT INTO `t_log` VALUES ('207', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:20:31', null);
INSERT INTO `t_log` VALUES ('208', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"ffffg\",\"name\":\"gfdfd\",\"phone\":\"434\",\"jenis_usaha\":\"dfdfgd\",\"zipcode\":\"43434\",\"type_identitas\":\"KTP\",\"no_identitas\":\"434343\",\"id_bank\":\"030\",\"bank\":\"AMERICAN EXPRESS BANK LTD\",\"rekening\":\"44343\",\"mdr\":\"43\",\"status\":\"1\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:20:53', null);
INSERT INTO `t_log` VALUES ('209', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:20:55', null);
INSERT INTO `t_log` VALUES ('210', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:26:51', null);
INSERT INTO `t_log` VALUES ('211', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:31:00', null);
INSERT INTO `t_log` VALUES ('212', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:35:29', null);
INSERT INTO `t_log` VALUES ('213', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:35:36', null);
INSERT INTO `t_log` VALUES ('214', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:35:58', null);
INSERT INTO `t_log` VALUES ('215', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:36:16', null);
INSERT INTO `t_log` VALUES ('216', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:36:33', null);
INSERT INTO `t_log` VALUES ('217', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:12', null);
INSERT INTO `t_log` VALUES ('218', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:16', null);
INSERT INTO `t_log` VALUES ('219', '1', 'View', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:19', null);
INSERT INTO `t_log` VALUES ('220', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:36', null);
INSERT INTO `t_log` VALUES ('221', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:44', null);
INSERT INTO `t_log` VALUES ('222', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:38:49', null);
INSERT INTO `t_log` VALUES ('223', '1', 'View', 'user_management/user', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:58:10', null);
INSERT INTO `t_log` VALUES ('224', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 16:58:14', null);
INSERT INTO `t_log` VALUES ('225', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:02:48', null);
INSERT INTO `t_log` VALUES ('226', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:03:04', null);
INSERT INTO `t_log` VALUES ('227', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:03:04', null);
INSERT INTO `t_log` VALUES ('228', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:07:05', null);
INSERT INTO `t_log` VALUES ('229', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:26:30', null);
INSERT INTO `t_log` VALUES ('230', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:27:01', null);
INSERT INTO `t_log` VALUES ('231', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:30:55', null);
INSERT INTO `t_log` VALUES ('232', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:32:22', null);
INSERT INTO `t_log` VALUES ('233', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:33:19', null);
INSERT INTO `t_log` VALUES ('234', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:34:47', null);
INSERT INTO `t_log` VALUES ('235', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:35:30', null);
INSERT INTO `t_log` VALUES ('236', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:36:42', null);
INSERT INTO `t_log` VALUES ('237', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:39:28', null);
INSERT INTO `t_log` VALUES ('238', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:40:28', null);
INSERT INTO `t_log` VALUES ('239', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:42:01', null);
INSERT INTO `t_log` VALUES ('240', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:43:52', null);
INSERT INTO `t_log` VALUES ('241', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:50:17', null);
INSERT INTO `t_log` VALUES ('242', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:50:56', null);
INSERT INTO `t_log` VALUES ('243', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:52:09', null);
INSERT INTO `t_log` VALUES ('244', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:52:53', null);
INSERT INTO `t_log` VALUES ('245', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:53:22', null);
INSERT INTO `t_log` VALUES ('246', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:53:44', null);
INSERT INTO `t_log` VALUES ('247', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:54:22', null);
INSERT INTO `t_log` VALUES ('248', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:56:20', null);
INSERT INTO `t_log` VALUES ('249', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 17:57:36', null);
INSERT INTO `t_log` VALUES ('250', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:00:46', null);
INSERT INTO `t_log` VALUES ('251', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:06:49', null);
INSERT INTO `t_log` VALUES ('252', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:07:08', null);
INSERT INTO `t_log` VALUES ('253', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:09:28', null);
INSERT INTO `t_log` VALUES ('254', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:11:08', null);
INSERT INTO `t_log` VALUES ('255', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:11:43', null);
INSERT INTO `t_log` VALUES ('256', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:12:56', null);
INSERT INTO `t_log` VALUES ('257', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:19:23', null);
INSERT INTO `t_log` VALUES ('258', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:20:28', null);
INSERT INTO `t_log` VALUES ('259', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:21:38', null);
INSERT INTO `t_log` VALUES ('260', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:25:21', null);
INSERT INTO `t_log` VALUES ('261', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:27:08', null);
INSERT INTO `t_log` VALUES ('262', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:29:15', null);
INSERT INTO `t_log` VALUES ('263', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:31:09', null);
INSERT INTO `t_log` VALUES ('264', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:31:39', null);
INSERT INTO `t_log` VALUES ('265', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:32:30', null);
INSERT INTO `t_log` VALUES ('266', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:33:00', null);
INSERT INTO `t_log` VALUES ('267', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:33:53', null);
INSERT INTO `t_log` VALUES ('268', '1', 'delete', 'merchant/merchant_delete', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:33:59', null);
INSERT INTO `t_log` VALUES ('269', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:34:00', null);
INSERT INTO `t_log` VALUES ('270', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:34:25', null);
INSERT INTO `t_log` VALUES ('271', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:39:21', null);
INSERT INTO `t_log` VALUES ('272', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:40:27', null);
INSERT INTO `t_log` VALUES ('273', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:41:10', null);
INSERT INTO `t_log` VALUES ('274', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:42:24', null);
INSERT INTO `t_log` VALUES ('275', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:43:25', null);
INSERT INTO `t_log` VALUES ('276', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-13 18:43:30', null);
INSERT INTO `t_log` VALUES ('277', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 09:35:59', null);
INSERT INTO `t_log` VALUES ('278', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 09:36:02', null);
INSERT INTO `t_log` VALUES ('279', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 09:55:38', null);
INSERT INTO `t_log` VALUES ('280', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 09:57:23', null);
INSERT INTO `t_log` VALUES ('281', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 09:57:36', null);
INSERT INTO `t_log` VALUES ('282', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:04:17', null);
INSERT INTO `t_log` VALUES ('283', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:04:44', null);
INSERT INTO `t_log` VALUES ('284', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:04:46', null);
INSERT INTO `t_log` VALUES ('285', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:05:59', null);
INSERT INTO `t_log` VALUES ('286', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:06:17', null);
INSERT INTO `t_log` VALUES ('287', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:07:20', null);
INSERT INTO `t_log` VALUES ('288', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:07:56', null);
INSERT INTO `t_log` VALUES ('289', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:10:27', null);
INSERT INTO `t_log` VALUES ('290', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:10:30', null);
INSERT INTO `t_log` VALUES ('291', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:10:48', null);
INSERT INTO `t_log` VALUES ('292', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:11:53', null);
INSERT INTO `t_log` VALUES ('293', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:12:30', null);
INSERT INTO `t_log` VALUES ('294', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:13:13', null);
INSERT INTO `t_log` VALUES ('295', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:13:59', null);
INSERT INTO `t_log` VALUES ('296', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:14:39', null);
INSERT INTO `t_log` VALUES ('297', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:15:03', null);
INSERT INTO `t_log` VALUES ('298', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:17:07', null);
INSERT INTO `t_log` VALUES ('299', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:17:21', null);
INSERT INTO `t_log` VALUES ('300', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:18:17', null);
INSERT INTO `t_log` VALUES ('301', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:18:36', null);
INSERT INTO `t_log` VALUES ('302', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:19:35', null);
INSERT INTO `t_log` VALUES ('303', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:19:59', null);
INSERT INTO `t_log` VALUES ('304', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 10:20:28', null);
INSERT INTO `t_log` VALUES ('305', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:40:22', null);
INSERT INTO `t_log` VALUES ('306', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:40:25', null);
INSERT INTO `t_log` VALUES ('307', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:43:10', null);
INSERT INTO `t_log` VALUES ('308', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:48:10', null);
INSERT INTO `t_log` VALUES ('309', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:50:29', null);
INSERT INTO `t_log` VALUES ('310', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:54:23', null);
INSERT INTO `t_log` VALUES ('311', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:54:34', null);
INSERT INTO `t_log` VALUES ('312', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:54:44', null);
INSERT INTO `t_log` VALUES ('313', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:54:52', null);
INSERT INTO `t_log` VALUES ('314', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:55:21', null);
INSERT INTO `t_log` VALUES ('315', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:55:25', null);
INSERT INTO `t_log` VALUES ('316', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:57:16', null);
INSERT INTO `t_log` VALUES ('317', '1', 'View', 'user_management/user', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:57:56', null);
INSERT INTO `t_log` VALUES ('318', '1', 'Delete', 'user_management/user_delete/36', '{success:true, Msg:\'Berhasil hapus user\'}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:58:02', null);
INSERT INTO `t_log` VALUES ('319', '1', 'View', 'user_management/user', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:58:04', null);
INSERT INTO `t_log` VALUES ('320', '1', 'Delete', 'user_management/user_delete/35', '{success:true, Msg:\'Berhasil hapus user\'}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:58:08', null);
INSERT INTO `t_log` VALUES ('321', '1', 'View', 'user_management/user', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:58:10', null);
INSERT INTO `t_log` VALUES ('322', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 16:58:13', null);
INSERT INTO `t_log` VALUES ('323', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 17:02:06', null);
INSERT INTO `t_log` VALUES ('324', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"ABCD123\",\"name\":\"PT ABC\",\"phone\":\"0821222324252\",\"jenis_usaha\":\"sirup\",\"zipcode\":\"22122\",\"type_identitas\":\"KTP\",\"alamat\":\"abc jalan jalan\",\"no_identitas\":\"12345678901234\",\"id_bank\":\"011\",\"bank\":\"Bank Danamon\",\"rekening\":\"1113334567\",\"mdr\":\"4\",\"email\":\"abc@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 17:04:12', null);
INSERT INTO `t_log` VALUES ('325', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-15 17:04:24', null);
INSERT INTO `t_log` VALUES ('326', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:06:43', null);
INSERT INTO `t_log` VALUES ('327', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:06:51', null);
INSERT INTO `t_log` VALUES ('328', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:08:04', null);
INSERT INTO `t_log` VALUES ('329', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:24:28', null);
INSERT INTO `t_log` VALUES ('330', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:24:31', null);
INSERT INTO `t_log` VALUES ('331', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"MCA12345\",\"name\":\"Mobile Apps\",\"phone\":\"082123889999\",\"jenis_usaha\":\"mobile\",\"zipcode\":\"12310\",\"type_identitas\":\"KTP\",\"alamat\":\"gandaria city\",\"no_identitas\":\"091828389495888\",\"id_bank\":\"022\",\"bank\":\"Bank Niaga\",\"rekening\":\"10022003300\",\"mdr\":\"5\",\"email\":\"mobile@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:27:03', null);
INSERT INTO `t_log` VALUES ('332', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:27:14', null);
INSERT INTO `t_log` VALUES ('333', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:27:20', null);
INSERT INTO `t_log` VALUES ('334', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"TEST\",\"name\":\"test\",\"phone\":\"0876767676767\",\"jenis_usaha\":\"test\",\"zipcode\":\"12341\",\"type_identitas\":\"KTP\",\"alamat\":\"test\",\"no_identitas\":\"12345609871209\",\"id_bank\":\"111\",\"bank\":\"Bank Dki\",\"rekening\":\"22929292030303\",\"mdr\":\"8\",\"email\":\"testing@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:28:50', null);
INSERT INTO `t_log` VALUES ('335', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:29:27', null);
INSERT INTO `t_log` VALUES ('336', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:29:30', null);
INSERT INTO `t_log` VALUES ('337', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"TESTING2\",\"name\":\"testing\",\"phone\":\"089799009900\",\"jenis_usaha\":\"test\",\"zipcode\":\"12310\",\"type_identitas\":\"KTP\",\"alamat\":\"test\",\"no_identitas\":\"3174052505901234\",\"id_bank\":\"008\",\"bank\":\"Bank Mandiri\",\"rekening\":\"9008765423\",\"mdr\":\"5\",\"email\":\"testingaja@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:30:59', null);
INSERT INTO `t_log` VALUES ('338', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:31:51', null);
INSERT INTO `t_log` VALUES ('339', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:37:01', null);
INSERT INTO `t_log` VALUES ('340', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:40:39', null);
INSERT INTO `t_log` VALUES ('341', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:41:05', null);
INSERT INTO `t_log` VALUES ('342', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:41:08', null);
INSERT INTO `t_log` VALUES ('343', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:41:23', null);
INSERT INTO `t_log` VALUES ('344', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:42:01', null);
INSERT INTO `t_log` VALUES ('345', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:42:22', null);
INSERT INTO `t_log` VALUES ('346', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 09:42:56', null);
INSERT INTO `t_log` VALUES ('347', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 13:44:01', null);
INSERT INTO `t_log` VALUES ('348', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 13:53:28', null);
INSERT INTO `t_log` VALUES ('349', '1', 'delete', 'merchant/merchant_delete', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 13:56:18', null);
INSERT INTO `t_log` VALUES ('350', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 13:56:19', null);
INSERT INTO `t_log` VALUES ('351', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 13:59:56', null);
INSERT INTO `t_log` VALUES ('352', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:03:51', null);
INSERT INTO `t_log` VALUES ('353', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"MCTS\",\"name\":\"Merchant Test\",\"phone\":\"082123224455\",\"jenis_usaha\":\"Jus Buah\",\"zipcode\":\"22334\",\"type_identitas\":\"KTP\",\"alamat\":\"alamat palsu\",\"no_identitas\":\"29388844499999\",\"id_bank\":\"002\",\"bank\":\"Bank BRI\",\"rekening\":\"20309994005999\",\"mdr\":\"10\",\"email\":\"mercant@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:05:28', null);
INSERT INTO `t_log` VALUES ('354', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:11:13', null);
INSERT INTO `t_log` VALUES ('355', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:18:28', null);
INSERT INTO `t_log` VALUES ('356', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"SSSS\",\"name\":\"sss\",\"phone\":\"22332\",\"jenis_usaha\":\"ssds\",\"zipcode\":\"1111\",\"type_identitas\":\"KTP\",\"alamat\":\"oapaoao . no pepko\",\"no_identitas\":\"21111111111\",\"id_bank\":\"032\",\"bank\":\"Jp. Morgan Chase Bank, N.A.\",\"rekening\":\"212\",\"mdr\":\"2\",\"email\":\"33@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:19:49', null);
INSERT INTO `t_log` VALUES ('357', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:24:01', null);
INSERT INTO `t_log` VALUES ('358', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:32:35', null);
INSERT INTO `t_log` VALUES ('359', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:32:46', null);
INSERT INTO `t_log` VALUES ('360', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:40:14', null);
INSERT INTO `t_log` VALUES ('361', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 14:56:52', null);
INSERT INTO `t_log` VALUES ('362', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 15:09:30', null);
INSERT INTO `t_log` VALUES ('363', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 15:43:42', null);
INSERT INTO `t_log` VALUES ('364', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 15:57:09', null);
INSERT INTO `t_log` VALUES ('365', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 15:58:28', null);
INSERT INTO `t_log` VALUES ('366', '1', 'delete', 'merchant/merchant_delete', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:01:36', null);
INSERT INTO `t_log` VALUES ('367', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:01:38', null);
INSERT INTO `t_log` VALUES ('368', '1', 'delete', 'merchant/merchant_delete', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:01:42', null);
INSERT INTO `t_log` VALUES ('369', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:01:43', null);
INSERT INTO `t_log` VALUES ('370', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:28:14', null);
INSERT INTO `t_log` VALUES ('371', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:29:00', null);
INSERT INTO `t_log` VALUES ('372', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:29:19', null);
INSERT INTO `t_log` VALUES ('373', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:33:37', null);
INSERT INTO `t_log` VALUES ('374', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:33:40', null);
INSERT INTO `t_log` VALUES ('375', '1', 'delete', 'merchant/merchant_delete', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:33:46', null);
INSERT INTO `t_log` VALUES ('376', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:33:47', null);
INSERT INTO `t_log` VALUES ('377', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:35:47', null);
INSERT INTO `t_log` VALUES ('378', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:36:44', null);
INSERT INTO `t_log` VALUES ('379', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"SYA11\",\"name\":\"Idolmart\",\"phone\":\"087782257271\",\"jenis_usaha\":\"pernak pernik\",\"zipcode\":\"12310\",\"type_identitas\":\"KTP\",\"alamat\":\"jalan idola gang asik\",\"no_identitas\":\"39773992090090\",\"id_bank\":\"013\",\"bank\":\"Permata Bank\",\"rekening\":\"028982989999\",\"mdr\":\"10\",\"email\":\"idolmart@gmail.com\",\"status\":\"1\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:38:22', null);
INSERT INTO `t_log` VALUES ('380', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:42:30', null);
INSERT INTO `t_log` VALUES ('381', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:45:55', null);
INSERT INTO `t_log` VALUES ('382', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:46:02', null);
INSERT INTO `t_log` VALUES ('383', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:47:03', null);
INSERT INTO `t_log` VALUES ('384', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:47:07', null);
INSERT INTO `t_log` VALUES ('385', '1', 'View', 'merchant/inquiry', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:47:16', null);
INSERT INTO `t_log` VALUES ('386', '1', 'View', 'merchant/transaksi', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-16 16:47:25', null);
INSERT INTO `t_log` VALUES ('387', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-16 17:03:58', null);
INSERT INTO `t_log` VALUES ('388', '1', 'View', 'merchant/merchant', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-16 17:04:10', null);
INSERT INTO `t_log` VALUES ('389', '1', 'View', 'user_management/role', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-16 17:31:49', null);
INSERT INTO `t_log` VALUES ('390', '1', 'View', 'report/activity', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-16 17:32:10', null);
INSERT INTO `t_log` VALUES ('391', '1', 'View', 'merchant/merchant', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-16 17:32:45', null);
INSERT INTO `t_log` VALUES ('392', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 13:47:49', null);
INSERT INTO `t_log` VALUES ('393', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 13:50:29', null);
INSERT INTO `t_log` VALUES ('394', '1', 'View', 'user_management/role', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 13:53:31', null);
INSERT INTO `t_log` VALUES ('395', '1', 'View', 'user_management/role_edit/30', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 13:53:45', null);
INSERT INTO `t_log` VALUES ('396', '1', 'View', 'user_management/role', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 14:07:59', null);
INSERT INTO `t_log` VALUES ('397', '1', 'View', 'user_management/user', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 14:08:05', null);
INSERT INTO `t_log` VALUES ('398', '1', 'View', 'merchant/inquiry', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 15:08:40', null);
INSERT INTO `t_log` VALUES ('399', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:29:36', null);
INSERT INTO `t_log` VALUES ('400', '1', 'View', 'merchant/inquiry', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:31:46', null);
INSERT INTO `t_log` VALUES ('401', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:39:06', null);
INSERT INTO `t_log` VALUES ('402', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:39:41', null);
INSERT INTO `t_log` VALUES ('403', '1', 'View', 'merchant/inquiry', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:40:09', null);
INSERT INTO `t_log` VALUES ('404', '1', 'View', 'merchant/transaksi', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 17:40:35', null);
INSERT INTO `t_log` VALUES ('405', '1', 'View', 'report/activity', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-19 18:00:14', null);
INSERT INTO `t_log` VALUES ('406', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 09:34:01', null);
INSERT INTO `t_log` VALUES ('407', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 09:34:08', null);
INSERT INTO `t_log` VALUES ('408', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 09:47:11', null);
INSERT INTO `t_log` VALUES ('409', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 10:54:44', null);
INSERT INTO `t_log` VALUES ('410', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:01:34', null);
INSERT INTO `t_log` VALUES ('411', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:03:01', null);
INSERT INTO `t_log` VALUES ('412', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:32', null);
INSERT INTO `t_log` VALUES ('413', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:40', null);
INSERT INTO `t_log` VALUES ('414', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:56', null);
INSERT INTO `t_log` VALUES ('415', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:56', null);
INSERT INTO `t_log` VALUES ('416', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:56', null);
INSERT INTO `t_log` VALUES ('417', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:56', null);
INSERT INTO `t_log` VALUES ('418', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:05:56', null);
INSERT INTO `t_log` VALUES ('419', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:06:01', null);
INSERT INTO `t_log` VALUES ('420', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:21:26', null);
INSERT INTO `t_log` VALUES ('421', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:24:11', null);
INSERT INTO `t_log` VALUES ('422', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:24:53', null);
INSERT INTO `t_log` VALUES ('423', '1', 'View', 'user_management/user', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:26:27', null);
INSERT INTO `t_log` VALUES ('424', '1', 'View', 'user_management/user_add', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:26:35', null);
INSERT INTO `t_log` VALUES ('425', '1', 'View', 'merchant/merchant', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:26:50', null);
INSERT INTO `t_log` VALUES ('426', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:26:55', null);
INSERT INTO `t_log` VALUES ('427', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-20 11:28:11', null);
INSERT INTO `t_log` VALUES ('428', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:34:04', null);
INSERT INTO `t_log` VALUES ('429', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 11:39:43', null);
INSERT INTO `t_log` VALUES ('430', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 13:12:57', null);
INSERT INTO `t_log` VALUES ('431', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:14:24', null);
INSERT INTO `t_log` VALUES ('432', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:14:28', null);
INSERT INTO `t_log` VALUES ('433', '1', 'View', 'user_management/role', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:19:20', null);
INSERT INTO `t_log` VALUES ('434', '1', 'View', 'user_management/role_edit/1', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:19:24', null);
INSERT INTO `t_log` VALUES ('435', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":9,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":11,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:19:30', null);
INSERT INTO `t_log` VALUES ('436', '1', 'View', 'user_management/role', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:19:32', null);
INSERT INTO `t_log` VALUES ('437', '1', 'View', 'user_management/role', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:19:44', null);
INSERT INTO `t_log` VALUES ('438', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:30:55', null);
INSERT INTO `t_log` VALUES ('439', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:31:05', null);
INSERT INTO `t_log` VALUES ('440', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:38:59', null);
INSERT INTO `t_log` VALUES ('441', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:39:15', null);
INSERT INTO `t_log` VALUES ('442', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:39:23', null);
INSERT INTO `t_log` VALUES ('443', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:39:37', null);
INSERT INTO `t_log` VALUES ('444', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:39:56', null);
INSERT INTO `t_log` VALUES ('445', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:40:02', null);
INSERT INTO `t_log` VALUES ('446', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"TM100\",\"name\":\"Jual Snack Murah \",\"phone\":\"085730572300\",\"jenis_usaha\":\"Dagang Makanan\",\"zipcode\":\"12520\",\"type_identitas\":\"KTP\",\"alamat\":\"adasdsfsfsfsfdf\",\"no_identitas\":\"09867892567\",\"id_bank\":\"009\",\"bank\":\"Bank BNI\",\"rekening\":\"737527829010\",\"mdr\":\"3\",\"email\":\"berta.shite@gmail.com\",\"status\":\"ACTIVE\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 17:43:04', null);
INSERT INTO `t_log` VALUES ('447', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:29:46', null);
INSERT INTO `t_log` VALUES ('448', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:34:25', null);
INSERT INTO `t_log` VALUES ('449', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:38:37', null);
INSERT INTO `t_log` VALUES ('450', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:50:36', null);
INSERT INTO `t_log` VALUES ('451', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:50:36', null);
INSERT INTO `t_log` VALUES ('452', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-20 18:50:36', null);
INSERT INTO `t_log` VALUES ('453', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:28:02', null);
INSERT INTO `t_log` VALUES ('454', '1', 'View', 'merchant/inquiry', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:39:38', null);
INSERT INTO `t_log` VALUES ('455', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:39:53', null);
INSERT INTO `t_log` VALUES ('456', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:40:00', null);
INSERT INTO `t_log` VALUES ('457', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:50:41', null);
INSERT INTO `t_log` VALUES ('458', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 09:52:34', null);
INSERT INTO `t_log` VALUES ('459', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:29:00', null);
INSERT INTO `t_log` VALUES ('460', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:29:30', null);
INSERT INTO `t_log` VALUES ('461', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:31:44', null);
INSERT INTO `t_log` VALUES ('462', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:31:57', null);
INSERT INTO `t_log` VALUES ('463', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:34:21', null);
INSERT INTO `t_log` VALUES ('464', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:34:30', null);
INSERT INTO `t_log` VALUES ('465', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:37:04', null);
INSERT INTO `t_log` VALUES ('466', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:37:15', null);
INSERT INTO `t_log` VALUES ('467', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 10:59:47', null);
INSERT INTO `t_log` VALUES ('468', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 11:04:05', null);
INSERT INTO `t_log` VALUES ('469', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 11:10:33', null);
INSERT INTO `t_log` VALUES ('470', '1', 'View', 'merchant/transaksi', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 11:10:41', null);
INSERT INTO `t_log` VALUES ('471', '1', 'View', 'merchant/transaksi', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 11:11:04', null);
INSERT INTO `t_log` VALUES ('472', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 11:38:08', null);
INSERT INTO `t_log` VALUES ('473', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 15:51:59', null);
INSERT INTO `t_log` VALUES ('474', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 15:52:04', null);
INSERT INTO `t_log` VALUES ('475', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 15:56:29', null);
INSERT INTO `t_log` VALUES ('476', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 16:13:25', null);
INSERT INTO `t_log` VALUES ('477', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 16:46:11', null);
INSERT INTO `t_log` VALUES ('478', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 16:47:51', null);
INSERT INTO `t_log` VALUES ('479', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 16:49:58', null);
INSERT INTO `t_log` VALUES ('480', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 17:01:31', null);
INSERT INTO `t_log` VALUES ('481', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 17:19:44', null);
INSERT INTO `t_log` VALUES ('482', '1', 'View', 'merchant/transaksi', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 17:21:31', null);
INSERT INTO `t_log` VALUES ('483', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-21 17:23:28', null);
INSERT INTO `t_log` VALUES ('484', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:33:48', null);
INSERT INTO `t_log` VALUES ('485', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:33:54', null);
INSERT INTO `t_log` VALUES ('486', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:33:56', null);
INSERT INTO `t_log` VALUES ('487', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"SAM001\",\"name\":\"Chiki\",\"phone\":\"087782257270\",\"jenis_usaha\":\"klontong\",\"zipcode\":\"12310\",\"type_identitas\":\"KTP\",\"alamat\":\"pondok pinang\",\"no_identitas\":\"31740521038700\",\"id_bank\":\"002\",\"bank\":\"Bank BRI\",\"rekening\":\"1002221788\",\"mdr\":\"3\",\"email\":\"\",\"status\":\"ACTIVE\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:35:07', null);
INSERT INTO `t_log` VALUES ('488', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:35:21', null);
INSERT INTO `t_log` VALUES ('489', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 09:38:18', null);
INSERT INTO `t_log` VALUES ('490', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 10:03:43', null);
INSERT INTO `t_log` VALUES ('491', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:04:26', null);
INSERT INTO `t_log` VALUES ('492', '1', 'View', 'user_management/role', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:04:48', null);
INSERT INTO `t_log` VALUES ('493', '1', 'View', 'user_management/role_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:04:53', null);
INSERT INTO `t_log` VALUES ('494', '1', 'View', 'user_management/role_edit/1', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:05:18', null);
INSERT INTO `t_log` VALUES ('495', '1', 'Update', 'user_management/role_edit/1', '[{\"name\":\"Super Admin\",\"is_admin\":\"Y\"},[{\"role_id\":\"1\",\"menu_id\":1,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":3,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":4,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":6,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":7,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"0\"},{\"role_id\":\"1\",\"menu_id\":8,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":9,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"},{\"role_id\":\"1\",\"menu_id\":11,\"create_\":\"1\",\"read_\":\"1\",\"update_\":\"1\",\"delete_\":\"1\"}]]', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:05:29', null);
INSERT INTO `t_log` VALUES ('496', '1', 'View', 'user_management/role', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:05:31', null);
INSERT INTO `t_log` VALUES ('497', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:05:37', null);
INSERT INTO `t_log` VALUES ('498', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:06:36', null);
INSERT INTO `t_log` VALUES ('499', '1', 'Add', 'data_master/komunitas_add', '{\"id_merchant\":\"TLTOM1\",\"name\":\"Telolet Om \",\"phone\":\"897771333332\",\"jenis_usaha\":\"BIS\",\"zipcode\":\"12120\",\"type_identitas\":\"KTP\",\"alamat\":\"ophir 1 no 2\",\"no_identitas\":\"31729498948888\",\"id_bank\":\"009\",\"bank\":\"Bank BNI\",\"rekening\":\"102929990000\",\"mdr\":\"3\",\"email\":\"teloletom@gmail.com\",\"status\":\"ACTIVE\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:19:36', null);
INSERT INTO `t_log` VALUES ('500', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:19:48', null);
INSERT INTO `t_log` VALUES ('501', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:20:30', null);
INSERT INTO `t_log` VALUES ('502', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:26:49', null);
INSERT INTO `t_log` VALUES ('503', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:31:56', null);
INSERT INTO `t_log` VALUES ('504', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:36:46', null);
INSERT INTO `t_log` VALUES ('505', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:38:06', null);
INSERT INTO `t_log` VALUES ('506', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:51:35', null);
INSERT INTO `t_log` VALUES ('507', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 11:52:30', null);
INSERT INTO `t_log` VALUES ('508', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:02:40', null);
INSERT INTO `t_log` VALUES ('509', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:02:56', null);
INSERT INTO `t_log` VALUES ('510', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:04:12', null);
INSERT INTO `t_log` VALUES ('511', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:04:20', null);
INSERT INTO `t_log` VALUES ('512', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:04:31', null);
INSERT INTO `t_log` VALUES ('513', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:05:34', null);
INSERT INTO `t_log` VALUES ('514', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:05:41', null);
INSERT INTO `t_log` VALUES ('515', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:07:04', null);
INSERT INTO `t_log` VALUES ('516', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:08:47', null);
INSERT INTO `t_log` VALUES ('517', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:09:03', null);
INSERT INTO `t_log` VALUES ('518', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:09:10', null);
INSERT INTO `t_log` VALUES ('519', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:09:28', null);
INSERT INTO `t_log` VALUES ('520', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:24:34', null);
INSERT INTO `t_log` VALUES ('521', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:24:39', null);
INSERT INTO `t_log` VALUES ('522', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:52:55', null);
INSERT INTO `t_log` VALUES ('523', '1', 'View', 'merchant/merchant', '', '10.10.12.87', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 14:53:05', null);
INSERT INTO `t_log` VALUES ('524', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 14:59:36', null);
INSERT INTO `t_log` VALUES ('525', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 14:59:45', null);
INSERT INTO `t_log` VALUES ('526', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 15:00:19', null);
INSERT INTO `t_log` VALUES ('527', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 15:09:00', null);
INSERT INTO `t_log` VALUES ('528', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 15:32:40', null);
INSERT INTO `t_log` VALUES ('529', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 15:58:58', null);
INSERT INTO `t_log` VALUES ('530', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:26:10', null);
INSERT INTO `t_log` VALUES ('531', '1', 'View', 'merchant/merchant', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:26:47', null);
INSERT INTO `t_log` VALUES ('532', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:26:51', null);
INSERT INTO `t_log` VALUES ('533', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.101', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 16:27:44', null);
INSERT INTO `t_log` VALUES ('534', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 16:50:11', null);
INSERT INTO `t_log` VALUES ('535', '1', 'View', 'report/activity', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 16:50:21', null);
INSERT INTO `t_log` VALUES ('536', '1', 'View', 'report/activity', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:53:47', null);
INSERT INTO `t_log` VALUES ('537', '1', 'View', 'user_management/user', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:54:36', null);
INSERT INTO `t_log` VALUES ('538', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.12', 'Mozilla/5.0 (Linux; Android 4.4.2; ASUS_T00G Build/KVT49L) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.91 Mobile Safari/537.36', '2016-12-22 16:54:40', null);
INSERT INTO `t_log` VALUES ('539', '1', 'View', 'merchant/merchant', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:54:43', null);
INSERT INTO `t_log` VALUES ('540', '1', 'View', 'user_management/role', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:55:12', null);
INSERT INTO `t_log` VALUES ('541', '1', 'View', 'merchant/merchant', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:55:18', null);
INSERT INTO `t_log` VALUES ('542', '1', 'View', 'user_management/role', '', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 16:55:37', null);
INSERT INTO `t_log` VALUES ('543', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.61', 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:00:36', null);
INSERT INTO `t_log` VALUES ('544', '1', 'View', 'merchant/merchant', '', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:01:55', null);
INSERT INTO `t_log` VALUES ('545', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:04:44', null);
INSERT INTO `t_log` VALUES ('546', '1', 'View', 'merchant/merchant', '', '10.10.12.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:05:24', null);
INSERT INTO `t_log` VALUES ('547', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:08:36', null);
INSERT INTO `t_log` VALUES ('548', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:18:37', null);
INSERT INTO `t_log` VALUES ('549', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:18:54', null);
INSERT INTO `t_log` VALUES ('550', '1', 'View', 'merchant/transaksi', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:19:42', null);
INSERT INTO `t_log` VALUES ('551', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:22:15', null);
INSERT INTO `t_log` VALUES ('552', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:27:17', null);
INSERT INTO `t_log` VALUES ('553', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.113', 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:29:18', null);
INSERT INTO `t_log` VALUES ('554', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:31:26', null);
INSERT INTO `t_log` VALUES ('555', '1', 'Add', 'merchant/merchant_add', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:31:40', null);
INSERT INTO `t_log` VALUES ('556', '1', 'View', 'merchant/merchant', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:31:52', null);
INSERT INTO `t_log` VALUES ('557', '1', 'View', 'report/activity', '', '10.10.12.48', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-22 17:32:08', null);
INSERT INTO `t_log` VALUES ('558', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.73', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.95 Safari/537.36', '2016-12-22 17:35:42', null);
INSERT INTO `t_log` VALUES ('559', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:37:58', null);
INSERT INTO `t_log` VALUES ('560', '1', 'View', 'report/activity', '', '10.10.12.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:38:45', null);
INSERT INTO `t_log` VALUES ('561', '1', 'View', 'merchant/transaksi', '', '10.10.12.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:38:57', null);
INSERT INTO `t_log` VALUES ('562', '1', 'Logout', 'Logout', '{\"userid_Merchant\":\"\",\"username_Merchant\":\"\",\"name_Merchant\":\"\",\"role_Merchant\":\"\",\"isadmin_Merchant\":\"\",\"email_user_Merchant\":\"\"}', '10.10.12.153', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-22 17:39:30', null);
INSERT INTO `t_log` VALUES ('563', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '10.10.12.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-23 09:41:38', null);
INSERT INTO `t_log` VALUES ('564', '1', 'View', 'merchant/merchant', '', '10.10.12.154', 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36', '2016-12-23 09:41:47', null);
INSERT INTO `t_log` VALUES ('565', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-23 09:46:56', null);
INSERT INTO `t_log` VALUES ('566', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-23 09:47:04', null);
INSERT INTO `t_log` VALUES ('567', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-23 09:47:08', null);
INSERT INTO `t_log` VALUES ('568', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-23 09:52:10', null);
INSERT INTO `t_log` VALUES ('569', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-23 09:52:12', null);
INSERT INTO `t_log` VALUES ('570', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-26 10:47:05', null);
INSERT INTO `t_log` VALUES ('571', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-26 10:58:14', null);
INSERT INTO `t_log` VALUES ('572', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-26 11:00:50', null);
INSERT INTO `t_log` VALUES ('573', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 09:39:57', null);
INSERT INTO `t_log` VALUES ('574', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 09:40:22', null);
INSERT INTO `t_log` VALUES ('575', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 09:40:24', null);
INSERT INTO `t_log` VALUES ('576', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 09:40:50', null);
INSERT INTO `t_log` VALUES ('577', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 12:21:46', null);
INSERT INTO `t_log` VALUES ('578', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 14:24:38', null);
INSERT INTO `t_log` VALUES ('579', '1', 'Add', 'merchant/merchant_add', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 14:43:51', null);
INSERT INTO `t_log` VALUES ('580', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 14:54:48', null);
INSERT INTO `t_log` VALUES ('581', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-28 14:57:13', null);
INSERT INTO `t_log` VALUES ('582', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:26:48', null);
INSERT INTO `t_log` VALUES ('583', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:27:58', null);
INSERT INTO `t_log` VALUES ('584', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:28:07', null);
INSERT INTO `t_log` VALUES ('585', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:36:04', null);
INSERT INTO `t_log` VALUES ('586', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:36:16', null);
INSERT INTO `t_log` VALUES ('587', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:36:24', null);
INSERT INTO `t_log` VALUES ('588', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:37:10', null);
INSERT INTO `t_log` VALUES ('589', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:37:23', null);
INSERT INTO `t_log` VALUES ('590', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:37:32', null);
INSERT INTO `t_log` VALUES ('591', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:37:48', null);
INSERT INTO `t_log` VALUES ('592', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:38:00', null);
INSERT INTO `t_log` VALUES ('593', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:38:15', null);
INSERT INTO `t_log` VALUES ('594', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:38:29', null);
INSERT INTO `t_log` VALUES ('595', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:40:35', null);
INSERT INTO `t_log` VALUES ('596', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:42:19', null);
INSERT INTO `t_log` VALUES ('597', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:46:16', null);
INSERT INTO `t_log` VALUES ('598', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:46:40', null);
INSERT INTO `t_log` VALUES ('599', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:51:21', null);
INSERT INTO `t_log` VALUES ('600', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:51:39', null);
INSERT INTO `t_log` VALUES ('601', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:52:13', null);
INSERT INTO `t_log` VALUES ('602', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:56:45', null);
INSERT INTO `t_log` VALUES ('603', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 09:57:30', null);
INSERT INTO `t_log` VALUES ('604', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:01:30', null);
INSERT INTO `t_log` VALUES ('605', '1', 'View', 'merchant/transaksi', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:02:35', null);
INSERT INTO `t_log` VALUES ('606', '1', 'View', 'merchant/inquiry', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:10:33', null);
INSERT INTO `t_log` VALUES ('607', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:21:59', null);
INSERT INTO `t_log` VALUES ('608', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:22:04', null);
INSERT INTO `t_log` VALUES ('609', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:22:05', null);
INSERT INTO `t_log` VALUES ('610', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:22:08', null);
INSERT INTO `t_log` VALUES ('611', '1', 'View', 'report/activity', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-29 10:22:10', null);
INSERT INTO `t_log` VALUES ('612', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2016-12-30 10:14:31', null);
INSERT INTO `t_log` VALUES ('613', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-03 12:58:19', null);
INSERT INTO `t_log` VALUES ('614', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-03 12:58:31', null);
INSERT INTO `t_log` VALUES ('615', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-03 12:58:32', null);
INSERT INTO `t_log` VALUES ('616', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-05 09:26:00', null);
INSERT INTO `t_log` VALUES ('617', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-19 10:15:44', null);
INSERT INTO `t_log` VALUES ('618', '1', 'View', 'merchant/merchant', '', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-19 10:15:49', null);
INSERT INTO `t_log` VALUES ('619', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-19 14:16:10', null);
INSERT INTO `t_log` VALUES ('620', '1', 'Login', 'Login', '{\"userid_Merchant\":\"1\",\"username_Merchant\":\"admin\",\"name_Merchant\":\"Administrator\",\"role_Merchant\":\"1\",\"isadmin_Merchant\":\"Y\",\"email_user_Merchant\":\"admin@gmail.com\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 6.1; rv:50.0) Gecko/20100101 Firefox/50.0', '2017-01-31 09:47:12', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(40) NOT NULL,
  `password` varchar(40) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `status` enum('Active','Inactive') DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `no_identitas` varchar(20) DEFAULT NULL,
  `address1` varchar(40) DEFAULT NULL,
  `address2` varchar(40) DEFAULT NULL,
  `zip` varchar(10) DEFAULT NULL,
  `city` varchar(40) DEFAULT NULL,
  `country` varchar(40) DEFAULT NULL,
  `description` varchar(80) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `is_admin` varchar(1) DEFAULT 'N',
  `verified` varchar(1) DEFAULT 'N',
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `last_login` datetime DEFAULT NULL,
  `online` tinyint(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`username`),
  UNIQUE KEY `id` (`id`) USING BTREE,
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com', 'Active', 'Administrator', '087875852955', '1234563434', 'pondok pinang', 'admin', '123123456', 'jakarta selatan', 'indonesia Raya', null, '1', 'Y', 'Y', '2015-05-18 09:17:22', '2017-01-31 09:47:12', '1');
INSERT INTO `user` VALUES ('32', 'acunk', '0bb2d65484c83d519c747dd9ab5317bf', 'acunk21@gmail.com', 'Active', 'acunk aja', '56456', '345346364', 'pinang city', 'jakarta', '123123', 'Jakarta Selatan', 'Indonesia', null, '1', 'N', 'Y', '2015-09-16 09:24:11', '2016-12-09 15:19:50', '1');
INSERT INTO `user` VALUES ('33', 'ahmad', 'd41d8cd98f00b204e9800998ecf8427e', 'ahmad@gmail.com', 'Active', 'ahmad', '211122', '1234567', 'gandaria', 'jakarta', '12310', 'jakarta', 'Indonesia', null, '30', 'N', 'Y', '2015-12-22 13:55:35', '2016-03-21 17:08:36', '1');
